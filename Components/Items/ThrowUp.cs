﻿using UnityEngine;
using System.Collections;

public class ThrowUp : MonoBehaviour
{
	Rigidbody2D ctRigidbody2D;
	[HideInInspector]
	ItemModel oItem;
	BoxCollider2D ctBoxCollider2D;
	bool shouldThrow = false;
	// Use this for initialization
	void Awake ()
	{
		ctRigidbody2D = GetComponent<Rigidbody2D> ();
		oItem = GetComponent<ItemContainer> ().Item;
		ctBoxCollider2D = GetComponent<BoxCollider2D> ();
	}

	void OnEnable ()
	{
		ctRigidbody2D.gravityScale = 0;
		oItem.ThrowPower = 4100;
		StartCoroutine (StartThrow ());
	}

	IEnumerator StartThrow ()
	{
		ctBoxCollider2D.enabled = false;
		shouldThrow = true;
		ctRigidbody2D.gravityScale = 0.7f;
		ctRigidbody2D.AddForce (new Vector2 (-oItem.ThrowDirection.X * oItem.ThrowPower * Time.deltaTime, -oItem.ThrowDirection.Y * oItem.ThrowPower * Time.deltaTime));
		yield return new WaitForSeconds (0.2f);
		oItem.ThrowPower = 0;
		shouldThrow = false;
		ctRigidbody2D.gravityScale = 0;
		ctRigidbody2D.velocity = Vector2.zero;
		ctBoxCollider2D.enabled = true;
	}

}
