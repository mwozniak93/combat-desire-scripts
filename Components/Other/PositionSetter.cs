﻿using UnityEngine;
using System.Collections;

public class PositionSetter : MonoBehaviour
{
	[SerializeField]
	Transform target;

	void Update ()
	{
		transform.position = target.position;
	}

}
