﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour
{
	CharacterModel player;

	void Start ()
	{
		player = PlayerSingleton.instance.GetComponent<CharacterContainer> ().oCharacter;
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.CompareTag ("Player")) {
			if (DataManager.instance.oPlayer.CurrentStage.MissionWaves.IsCompleted ()) {
//				GameStats.instance.Mission.Complete();
				DataManager.instance.oPlayer.CurrentStage.OnStageCompleted ();
			} else {
				player.OnDialogShow ("I must finish my mision first...");
			}
		}
	}
}
