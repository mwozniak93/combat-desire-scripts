﻿using UnityEngine;
using System.Collections;

public class BulletSortOrder : MonoBehaviour
{

	ParticleSystemRenderer ctParticleSystemRenderer;
	GameObject oBulletOwnerGO;
	float ownerDistance;

	// Use this for initialization
	void Start ()
	{
		ctParticleSystemRenderer = GetComponent<ParticleSystemRenderer> ();
		oBulletOwnerGO = GetComponentInParent<Bullet> ().oBulletOnwerGO;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		ownerDistance = Vector2.Distance (oBulletOwnerGO.transform.position, transform.position);
//		//Debug.Log("ownerDistance  : " +ownerDistance );
		if (ownerDistance <= 0.4f) {
			ctParticleSystemRenderer.sortingOrder = 0;
		} else {
			ctParticleSystemRenderer.sortingOrder = 100;
		}
	}
}
