﻿using UnityEngine;
using System.Collections;

public class Deactivate : MonoBehaviour
{
	[SerializeField]
	public float time;
	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (deactivate ());
	}

	IEnumerator deactivate ()
	{
		yield return new WaitForSeconds (time);
//		//Debug.Log("Dewactivating... " ) ;
		gameObject.SetActive (false);
	}

}
