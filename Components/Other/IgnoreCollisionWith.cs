﻿using UnityEngine;
using System.Collections;

public class IgnoreCollisionWith : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		Physics2D.IgnoreCollision (GetComponentInParent<BoxCollider2D> (), PlayerSingleton.instance.GetComponent<BoxCollider2D> ());
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
