﻿using UnityEngine;
using System.Collections;

public class SetParticlesSortLayer : MonoBehaviour
{
	[SerializeField]
	public string sortingLayer;
	// Use this for initialization
	void Awake ()
	{
		foreach (var item in GetComponents<ParticleSystemRenderer>()) {
			item.sortingLayerName = sortingLayer;
		}
		foreach (var item in GetComponentsInChildren<ParticleSystemRenderer>()) {
//			//Debug.Log ("Item  : " + item.name);
			item.sortingLayerName = sortingLayer;
		}
	}

}
