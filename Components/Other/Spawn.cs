﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour
{

	GameObject ObjectToSpawn;
	CharacterModel oCharacter;
	MapMarker ctMapMarker;
	bool loaded = false;
	// Use this for initialization
	public void Init ()
	{



	}

	void Start ()
	{
		if (!loaded) {
			GetComponent<CircleCollider2D> ().radius = 2.4f;
			oCharacter = transform.parent.GetComponentInChildren<CharacterContainer> ().oCharacter;
			ctMapMarker = transform.parent.GetComponentInChildren<MapMarker> ();
			ObjectToSpawn = transform.parent.Find (transform.parent.name).gameObject;
			loaded = true;
		}
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.CompareTag ("Player") && !oCharacter.IsDead) {
			ObjectToSpawn.SetActive (true);
		}

	}

	void OnTriggerExit2D (Collider2D coll)
	{
		if (coll.CompareTag ("Player") && !oCharacter.IsDead) {
			StartCoroutine (Deacvtivate ());
		}

	}

	IEnumerator Deacvtivate ()
	{
		yield return new WaitForEndOfFrame ();
		ObjectToSpawn.SetActive (false);
	}

	void Update ()
	{
		if (ObjectToSpawn) {
			transform.position = ObjectToSpawn.transform.position;
		}
	}
}
