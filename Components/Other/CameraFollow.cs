﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

	public Transform target;
	float distance = 10f;
	public float smooth;

	void Start ()
	{
		target = PlayerSingleton.instance.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (target) {
			transform.position = Vector3.Lerp (transform.position, new Vector3 (target.position.x, target.position.y, target.position.z - distance), Time.deltaTime * 1.8f);
		}
		
	}
}
