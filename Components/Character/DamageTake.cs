﻿using UnityEngine;
using System.Collections;

public class DamageTake : MonoBehaviour
{
	bool isHit = false;
	CharacterModel oCharacterModel;
	bool isContinous;
	public VectorXY pushDirection;
	SpriteRenderer ctSpriteRenderer;
	float waveTimer = 0;
	//	CharacterModel oEnemyModel;
	States previousStates;
	Color startColor;
	Powerup ctPowerupController;
	CharacterModel oEnemyModel;
	Animator ctAnimator;
	bool isWaveHit;

	void Start ()
	{
		pushDirection = new VectorXY ();
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		ctPowerupController = GetComponent<Powerup> ();
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		ctAnimator = GetComponent<Animator> ();
		startColor = ctSpriteRenderer.color;
		oCharacterModel.eDie += Die;
//		oCharacterModel.ChargeKi(1);
	}

	void OnDestroy ()
	{
		//Debug.Log ("On destroy for    : " + name + " oCharacterModel : " + oCharacterModel);
		oCharacterModel.eDie -= Die;
		
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.CompareTag ("Bullet")) {

			TakeDamage (coll.gameObject);
			oCharacterModel.ReduceKiPoints (5);

		}
		if (coll.CompareTag ("BulletWave")) {
			waveTimer = 1;
		}

	}

	void OnTriggerStay2D (Collider2D coll)
	{
		if (coll.CompareTag ("BulletWave")) {
			isWaveHit = true;
			if (waveTimer >= 0.35f) {
				TakeDamage (coll.gameObject);
				waveTimer = 0;
			}
		}
	}

	void OnTriggerExit2D (Collider2D coll)
	{
		if (coll.CompareTag ("BulletWave")) {
			waveTimer = 0;
		}
	}

	public void TakeDamage (GameObject goEnemy)
	{
		previousStates = oCharacterModel.States;	
		if (oCharacterModel.States != States.Hurting) {
			float damage = 0;
			Bullet bullet = goEnemy.GetComponent<Bullet> ();
			oEnemyModel = bullet.oBulletOwner;
			pushDirection.X = bullet.Direction.X;
			pushDirection.Y = bullet.Direction.Y;
			if (oCharacterModel.States != States.Guarding) {
				damage = bullet.damage;
			} else if (oCharacterModel.States == States.Guarding) {
				damage = bullet.damage / 5;
			}
			if (oCharacterModel.States != States.Guarding) { //|| (pushDirection.X==oCharacterModel.Direction.X)
				oCharacterModel.States = States.Hurting;	
			
			}
//			//Debug.Log ("STATE : " + oCharacterModel.States);
			oCharacterModel.Direction.X = Mathf.Round (-bullet.Direction.X);
			oCharacterModel.Direction.Y = Mathf.Round (-bullet.Direction.Y);
			oCharacterModel.OnDamageStart ();
			ctSpriteRenderer.color = Color.red;
			ctAnimator.SetBool ("IsHurting", true);
			StartCoroutine (UnHit ());
			oCharacterModel.TakeDamage (damage);

			if (oEnemyModel != null) {
				//			oEnemyModel.On(false);
				oCharacterModel.OnDead (oEnemyModel);
			}
		}

	}

	IEnumerable ContinousDamage ()
	{
		yield return new WaitForSeconds (0.5f);

	}

	IEnumerator UnHit ()
	{
		yield return new WaitForSeconds (0.09f);
		ctSpriteRenderer.color = startColor;
		yield return new WaitForSeconds (0.13f);
		ctAnimator.SetBool ("IsHurting", false);
		if (oCharacterModel.States == States.Hurting) {
			oCharacterModel.OnDamageEnd ();
			if (oCharacterModel.States != States.Guarding) {
				oCharacterModel.States = States.Idle;
			}
		}

	}

	void Die ()
	{

		GameObject dieEffect = EffectsPoolManager.instance.GetExplosion (ExplosionType.SmokeBig);
		if (dieEffect) {
			dieEffect.transform.position = transform.position;
			dieEffect.SetActive (true);
		}
		StartCoroutine (DeactivateCharacter ());
//		Deactivate

	}

	IEnumerator DeactivateCharacter ()
	{
		yield return new WaitForEndOfFrame ();
		if (oCharacterModel.CharacterType == CharacterType.Player) {
			yield return new WaitForSeconds (1f);
		}
		gameObject.SetActive (false);
	}

	void Update ()
	{
		if (isWaveHit) {
			waveTimer += Time.deltaTime;
		}

	}
	

}
