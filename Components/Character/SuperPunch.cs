﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Punch))]
public class SuperPunch : MonoBehaviour
{

	CharacterModel oCharacterModel;
	Punch ctBasicAttack;
	// Use this for initialization
	//	void Start () {
	//		oCharacterModel = GetComponent<CharacterContainer>().oCharacter;
	//		ctBasicAttack =GetComponent<BasicAttack>();
	//	}
	//
	//	BoxCollider2D Hit;
	Animator ctAnimator;
	Vector2 startHitPos;
	Vector2 StandingPosition;
	float dirX, dirY;
	States previousState;
	Bullet ctBulletUp, ctBulletDown, ctBulletLeft, ctBulletRight;
	SpriteRenderer ctSpriteRenderer;
	BoxCollider2D HitBoxUp, HitBoxDown, HitBoxLeft, HitBoxRight;
	string randomizedAttack;
	//	EffectsPoolManager SnEffectsPoolManager;
	GameObject effect;
	//	bool isAboutToAttack=false;
	//	Animator ctAnimator;
	//	BoxCollider2D HitBoxCollider;
	void Start ()
	{
		ctAnimator = GetComponent<Animator> ();
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		HitBoxUp = transform.Find ("HitBoxUp").GetComponent<BoxCollider2D> ();
		HitBoxUp.enabled = false;
		ctBulletUp = HitBoxUp.GetComponent<Bullet> ();

		HitBoxDown = transform.Find ("HitBoxDown").GetComponent<BoxCollider2D> ();
		HitBoxDown.enabled = false;
		ctBulletDown = HitBoxDown.GetComponent<Bullet> ();

		HitBoxLeft = transform.Find ("HitBoxLeft").GetComponent<BoxCollider2D> ();
		HitBoxLeft.enabled = false;
		ctBulletLeft = HitBoxLeft.GetComponent<Bullet> ();

		HitBoxRight = transform.Find ("HitBoxRight").GetComponent<BoxCollider2D> ();
		HitBoxRight.enabled = false;
		ctBulletRight = HitBoxRight.GetComponent<Bullet> ();
		oCharacterModel.eDamageTaken += CancelSuperPunch;
//		SnEffectsPoolManager = EffectsPoolManager.instance;
		//		ctMovementController =GetComponent<MovementController>();
	}

	void OnDestroy ()
	{
		oCharacterModel.eDamageTaken -= CancelSuperPunch;

	}

	//X: 0.06416014, 0.1211056
	//Y : 0.16, 0.09

	public void SpeedAttackOnDown ()
	{
		if (oCharacterModel.States == States.Idle && !oCharacterModel.IsDead) {
			oCharacterModel.PunchBtnPressTime = 0;
			StartCoroutine (SetPrepereAttackAnim ());
		}

	}

	IEnumerator SetPrepereAttackAnim ()
	{
		yield return new WaitForSeconds (0.1f);
		if (oCharacterModel.States == States.Idle) {
			oCharacterModel.States = States.StandingSteel;
		}
		yield return new WaitForSeconds (0.65f);
		if (oCharacterModel.PunchBtnPressTime > 0.75f && oCharacterModel.States == States.StandingSteel) {
			oCharacterModel.SetPowerUpAura (true);
			oCharacterModel.States = States.SuperPunchPrepere;
			oCharacterModel.LastState = States.SuperPunchPrepere;
			ctAnimator.SetBool ("AttackPrepere", true);
		}
//		}
	}

	public void SpeedAttackOnUp ()
	{
		float speedBackup = oCharacterModel.Speed;
		float randomize = Random.Range (1, oCharacterModel.AttackAnimationsCount);
		//		////Debug.Log("randomzied :" + randomize);
		randomizedAttack = "IsAttacking" + randomize;//
		ctAnimator.SetBool ("AttackPrepere", false);

		if (oCharacterModel.PunchBtnPressTime > 0.75f && oCharacterModel.States == States.SuperPunchPrepere) {
			effect = EffectsPoolManager.instance.GetEffect (EffectType.SuperPunch);
			if (effect) {
				effect.transform.position = transform.position;
				effect.SetActive (true);
				effect.transform.SetParent (transform);
			}

			Bullet ctBullet;
			BoxCollider2D Hit;
			Hit = HitBoxRight;
			ctBullet = ctBulletRight;
			dirX = oCharacterModel.Direction.X;
			dirY = oCharacterModel.Direction.Y;

			if (dirX == -1) {
				Hit = HitBoxLeft;
				ctBullet = ctBulletLeft;
			}
			if (dirX == 1) {
				Hit = HitBoxRight;
				ctBullet = ctBulletRight;
			}

			if (dirY == 1) {
				Hit = HitBoxUp;
				ctBullet = ctBulletUp;
			}

			if (dirY == -1) {
				Hit = HitBoxDown;
				ctBullet = ctBulletDown;
			}
			ctBullet.damage = oCharacterModel.KiLevel;
			ctBullet.oBulletOwner = oCharacterModel;
			ctBullet.Direction = oCharacterModel.Direction;
			ctBullet.explosionType = ExplosionType.SmokeSmall;

			if (oCharacterModel.States != States.SpeedAttack && oCharacterModel.States != States.Attacking && oCharacterModel.States != States.Charging && oCharacterModel.States != States.Hurting) {
//				////Debug.Log("(oCharacterModel.PunchBtnPressTime ; 3' " +oCharacterModel.PunchBtnPressTime + " Styate : " + oCharacterModel.States);
				//		float randomizeAttack = Random.Range(1,2);
				previousState = oCharacterModel.States;
				Hit.enabled = true;
				ctAnimator.SetBool (randomizedAttack, true);
				oCharacterModel.States = States.SpeedAttack;
				oCharacterModel.LastState = States.SpeedAttack;
				oCharacterModel.Speed = 2.7f;
				StartCoroutine (StopPunching (speedBackup, Hit));
			}

		}

	}

	void CancelSuperPunch (float dmgVal)
	{

		if (oCharacterModel.LastState == States.SuperPunchPrepere) {

			oCharacterModel.States = States.Idle;
			oCharacterModel.LastState = States.Idle;
			oCharacterModel.PunchBtnPressTime = 0;
			oCharacterModel.SetPowerUpAura (false);
			ctAnimator.SetBool ("AttackPrepere", false);
			ctAnimator.SetBool (randomizedAttack, false);
			if (effect) {
				effect.SetActive (false);
			}
			HitBoxUp.enabled = false;
			HitBoxRight.enabled = false;
			HitBoxDown.enabled = false;
			HitBoxLeft.enabled = false;

		}
	}

	IEnumerator StopPunching (float speedBackup, BoxCollider2D Hit)
	{
		yield return new WaitForSeconds (0.24f);
		oCharacterModel.Speed = speedBackup;
		ctAnimator.SetBool (randomizedAttack, false);
		Hit.enabled = false;
		yield return new WaitForSeconds (0.05f);
//		////Debug.Log("Super Punch" ) ;
		oCharacterModel.States = States.Idle;
		oCharacterModel.PunchBtnPressTime = 0;
		oCharacterModel.SetPowerUpAura (false);
		if (effect) {
			effect.SetActive (false);
		}

	}

	void Update ()
	{

		oCharacterModel.PunchBtnPressTime += Time.deltaTime;
	}
}
