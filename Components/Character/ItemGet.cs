﻿using UnityEngine;
using System.Collections;

//TODO: dac ten component przy zaladowaniu  LVL?
public class ItemGet : MonoBehaviour
{
	CharacterModel oCharacter;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
	}

	void OnDestroy ()
	{
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionComplete -= AddBonus;	
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		if (coll.CompareTag ("Item")) {
			ItemModel oItem = coll.GetComponent<ItemContainer> ().Item;

			switch (oItem.ItemBoostType) {
			case ItemBoostType.Health:
				oCharacter.AddHp ((oItem.BoostValue / 100) * oCharacter.MaxKiLevel);
				break;
			case ItemBoostType.Coins:
				DataManager.instance.oPlayer.CurrentStage.Coins += (oItem.BoostValue * DataManager.instance.oPlayer.CurrentStage.CoinsMultipler);
				DataManager.instance.oPlayer.Coins += (oItem.BoostValue * DataManager.instance.oPlayer.CurrentStage.CoinsMultipler);
				break;

			case ItemBoostType.KiLevel:
				oCharacter.KiLevel += (float)oItem.BoostValue;
				break;

			case ItemBoostType.Mission:
				DataManager.instance.oPlayer.CollectShard (oItem.ItemType);
				GameObject effect = EffectsPoolManager.instance.GetEffect (EffectType.KiLevelGain);
				if (effect) {
					effect.transform.position = coll.transform.position;
					effect.SetActive (true);
				}

				break;
			}
			coll.gameObject.SetActive (false);
		}
	}

	void AddBonus (Mission mission)
	{
		////Debug.Log("BONUSSS! : " );
		DataManager.instance.oPlayer.CurrentStage.Coins += DataManager.instance.oPlayer.CurrentStage.MissionWaves.BonusCoins;
		DataManager.instance.oPlayer.Coins += DataManager.instance.oPlayer.CurrentStage.MissionWaves.BonusCoins;
		oCharacter.MaxKiLevelBase += (float)DataManager.instance.oPlayer.CurrentStage.MissionWaves.BonusKiLevel;
	}

}
