﻿using UnityEngine;
using System.Collections;

public class Vanish : MonoBehaviour
{
	CharacterModel oCharacterModel;
	CharacterModel oEnemyModel;
	SpriteRenderer ctEnemySprite;
	bool isFighting = false;
	float timer = 0;
	RaycastHit2D hit;
	bool isClicked = false;
	GameObject vanishEffect;
	SpriteRenderer ctSpriteRenderer;
	BoxCollider2D ctBoxCollider2D;
	float VanishKiPercReq = 8;
	float teleportRate = 0.81f;
	[SerializeField]
	LayerMask vanishPreventLayer;
	//	GameObject startGo, endG0;
	//	Collider2D hit;
	void Start ()
	{
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		ctBoxCollider2D = GetComponent<BoxCollider2D> ();
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
	}

	public void VanishOn ()
	{
		StartCoroutine (VanishOnCor ());
	}

	IEnumerator VanishOnCor ()
	{

	
		bool isEnoughKi = oCharacterModel.IsEnoughKi (VanishKiPercReq);
		
		if (isClicked && oCharacterModel.DefendBtnPressTime < 0.25f && (oCharacterModel.States == States.Idle || oCharacterModel.States == States.SuperPunchPrepere) && isEnoughKi) {
//			if(oCharacterModel.KiLvlPercentage>0.2f){

			States StatesBackup;
//			if(oCharacterModel.States == States.SuperPunchPrepere){
			StatesBackup = oCharacterModel.States;
//			}
			AudioManager.Instance.Play (AudioType.Teleport);
			hit = Physics2D.Linecast (transform.position, new Vector2 (transform.position.x + teleportRate * oCharacterModel.Direction.X, transform.position.y + teleportRate * oCharacterModel.Direction.Y), vanishPreventLayer);
			vanishEffect = EffectsPoolManager.instance.GetEffect (EffectType.Vanish);
			if (vanishEffect) {
				vanishEffect.transform.position = transform.position;
				vanishEffect.SetActive (true);
			}
			if (hit.collider == null) {
//				//Debug.Log("HIT ? :"  +hit.transform.name);
//			if( hit.collider.gameObject.layer!=LayerMask.NameToLayer("Ground")){

							
				ctSpriteRenderer.enabled = false;
				ctBoxCollider2D.enabled = false;
				oCharacterModel.States = States.Vanishing;

				yield return new WaitForSeconds (0.18f);
				oCharacterModel.States = States.Idle;

				if (oCharacterModel.States == States.Idle || oCharacterModel.States == States.Walking) {
					oCharacterModel.ReduceKiPoints (VanishKiPercReq);
					if (oCharacterModel.Direction.X != 0) {

						transform.position = new Vector2 (transform.position.x + teleportRate * (oCharacterModel.Direction.X), transform.position.y);
					} else if (oCharacterModel.Direction.Y != 0) {
	
						transform.position = new Vector2 (transform.position.x, transform.position.y + teleportRate * (oCharacterModel.Direction.Y));
//				oCharacterModel.Direction.Y *=-1;
					}
					if (StatesBackup == States.SuperPunchPrepere) {
						oCharacterModel.States = States.SuperPunchPrepere;
					}

				}

				vanishEffect = EffectsPoolManager.instance.GetEffect (EffectType.Vanish);
				if (vanishEffect) {
					vanishEffect.transform.position = transform.position;
					vanishEffect.SetActive (true);
				}
			
			
				yield return new WaitForSeconds (0.1f);
				ctBoxCollider2D.enabled = true;
				ctSpriteRenderer.enabled = true;
			}
//		}

		}
		if (!isClicked) {
			oCharacterModel.DefendBtnPressTime = 0;
			isClicked = true;
		}
//		}
//		else{
//			//Debug.Log("Not Enough Ki");
//
//		}
	}

	void Update ()
	{
		oCharacterModel.DefendBtnPressTime += Time.deltaTime;
		if (oCharacterModel.DefendBtnPressTime >= 0.25f) {
			isClicked = false;
		}

	}

	void OnTriggerExit ()
	{
		isFighting = false;
	}
}
