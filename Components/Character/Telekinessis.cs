﻿using UnityEngine;
using System.Collections;

public class Telekinessis : MonoBehaviour
{
	CharacterModel oCharacter;
	[HideInInspector]
	public GameObject TkTargetGO;
	Animator ctAnimator;
	GameObject BoxTkEffect;
	Vector2 newPos;
	bool isUsingTk;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		ctAnimator = GetComponent<Animator> ();
		oCharacter.eTkOff += CancelTelekinsesis;
		newPos = transform.position;
	
	}

	void OnDestroy ()
	{
		oCharacter.eTkOff -= CancelTelekinsesis;
	}

	void CancelTelekinsesis ()
	{
		isUsingTk = false;
		if (oCharacter.LastState == States.TKFocus || oCharacter.LastState == States.TKShoot) {

//			if(TkTargetGO.name=="Box"){
			TkTargetGO.GetComponent<BoxCollider2D> ().isTrigger = false;
			TkTargetGO.gameObject.layer = LayerMask.NameToLayer ("Ground");
//			}
//			if(TkTargetGO.CompareTag("Bullet")){
//				TkTargetGO.gameObject.layer = LayerMask.NameToLayer("Ground");
//			}
			oCharacter.States = States.Idle;
			ctAnimator.SetBool ("PrepereEnergy", false);
			ctAnimator.SetBool ("EnergyAttack1", false);
			if (BoxTkEffect) {
				BoxTkEffect.SetActive (false);
				BoxTkEffect.transform.parent = null;
			}
			if (TkTargetGO) {
				if (TkTargetGO.GetComponent<Bullet> ()) {
					Destroy (TkTargetGO.gameObject.GetComponent<Bullet> ());
				}
			}
			TkTargetGO = null;
			oCharacter.OnTkObjectMet (false);
			oCharacter.LastState = States.Idle;
		}
	}
	//GameObject go
	public void TelekinesisOnDown ()
	{
		if (oCharacter.States == States.Idle) {
			isUsingTk = true;
			if (oCharacter.Direction.Y != 0 && oCharacter.Direction.X == 0) {
				newPos = new Vector2 (transform.position.x, (transform.position.y) + (0.3f * oCharacter.Direction.Y));
			} else if (oCharacter.Direction.X != 0 && oCharacter.Direction.Y == 0) {
				newPos = new Vector2 ((transform.position.x) + (oCharacter.Direction.X * 0.3f), transform.position.y);
			}
			oCharacter.States = States.TKFocus;
			oCharacter.LastState = oCharacter.States;
			oCharacter.SetPowerUpAura (true);
			ctAnimator.SetBool ("PrepereEnergy", true);
			if (TkTargetGO) {
				TkTargetGO.GetComponent<BoxCollider2D> ().isTrigger = true;
				if (TkTargetGO.CompareTag ("Box")) {
//				TkTargetGO.GetComponent<BoxCollider2D>().isTrigger=false;
				}
		
				BoxTkEffect = EffectsPoolManager.instance.GetEffect (EffectType.BoxTk);
				if (BoxTkEffect) {
					BoxTkEffect.transform.position = TkTargetGO.transform.position;
					BoxTkEffect.transform.SetParent (TkTargetGO.transform);
					BoxTkEffect.SetActive (true);
		
				}
			}
		}
	}

	public IEnumerator TelekinesisOnUp ()
	{
		if (oCharacter.States == States.TKFocus) {
			TkTargetGO.gameObject.layer = LayerMask.NameToLayer ("PlayerAttack");
			if (TkTargetGO && oCharacter.States == States.TKFocus) {
				if (!TkTargetGO.GetComponent<Bullet> ()) {
					TkTargetGO.gameObject.AddComponent<Bullet> ();
				}
				Bullet TkTargetBullet = TkTargetGO.GetComponent<Bullet> ();
				if (TkTargetBullet) {
//				TkTargetGO.GetComponent<BoxCollider2D>().isTrigger=true;
					TkTargetBullet.ctRigidbody2D.gravityScale = 0;
					TkTargetBullet.damage = oCharacter.KiLevel;
					TkTargetBullet.oBulletOwner = oCharacter;
					TkTargetBullet.Direction = oCharacter.Direction;
					TkTargetBullet.explosionType = ExplosionType.SmokeBig;
					ctAnimator.SetBool ("PrepereEnergy", false);
					ctAnimator.SetBool ("EnergyAttack1", true);
					oCharacter.States = States.TKShoot;
					oCharacter.LastState = oCharacter.States;
//				TkTargetGO.GetComponent<BoxCollider2D>().isTrigger=tr;
					TkTargetBullet.ctRigidbody2D.velocity = new Vector2 (2f * oCharacter.Direction.X, 2f * oCharacter.Direction.Y);

					TkTargetGO.tag = "Bullet";
		
					yield return new WaitForSeconds (0.3f);
					ctAnimator.SetBool ("EnergyAttack1", false);
					if (isUsingTk) {
						oCharacter.SetPowerUpAura (false);
						oCharacter.OnTkObjectMet (false);
						oCharacter.States = States.Idle;
					}
					yield return new WaitForSeconds (0.3f);
					if (TkTargetGO && isUsingTk) {
//			TkTargetGO.GetComponent<BoxCollider2D>().isTrigger=false;
						TkTargetBullet.ctRigidbody2D.velocity = Vector2.zero;
//		yield return new WaitForSeconds(3f);
						if (BoxTkEffect) {
							BoxTkEffect.transform.parent = null;
							BoxTkEffect.SetActive (false);
						}
						GameObject explosionEffect = EffectsPoolManager.instance.GetExplosion (ExplosionType.SmokeBig);
						explosionEffect.transform.position = TkTargetGO.transform.position;
						explosionEffect.SetActive (true);
						CharacterModel oBoxModel = TkTargetGO.GetComponent<CharacterContainer> ().oCharacter;

						TkTargetGO.tag = "Enemy";
						TkTargetGO.layer = LayerMask.NameToLayer ("Ground");
						if (TkTargetBullet) {
							TkTargetGO.GetComponent<BoxCollider2D> ().isTrigger = false;
							TkTargetBullet.ctRigidbody2D.gravityScale = 0;

							Destroy (TkTargetBullet);
							Destroy (TkTargetGO.GetComponent<Rigidbody2D> ());
						}

						TkTargetGO = null;
					}
				}
			}
		} else {
			CancelTelekinsesis ();
		}
	}

	void Update ()
	{
//		//Debug.Log("State : " + oCharacter.States);
		if (TkTargetGO && oCharacter.States == States.TKFocus) {
			TkTargetGO.transform.position = Vector2.MoveTowards (TkTargetGO.transform.position, newPos, 0.74f * Time.deltaTime);
		}
	
	}
}
