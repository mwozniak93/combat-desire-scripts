﻿using UnityEngine;
using System.Collections;

public class SoundListener : MonoBehaviour
{
	public CharacterModel oCharacter;
	// Use this for initialization
	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		oCharacter.ePunch += PlayHitSound;
	}

	void PlayHitSound ()
	{
		AudioManager.Instance.Play (AudioType.Hit);

	}
	//

}

