﻿using UnityEngine;
using System.Collections;

public class GhostMode : MonoBehaviour
{
	public float ghostModeChangeRateTime;
	SpriteRenderer ctSpriteRenderer;
	BoxCollider2D ctBoxCollider2D;
	// Use this for initialization
	void Awake ()
	{
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		ctBoxCollider2D = GetComponent<BoxCollider2D> ();
	}

	void OnEnable ()
	{
		StartCoroutine (ChangeMode ());
	}

	IEnumerator ChangeMode ()
	{
		yield return new WaitForSeconds (ghostModeChangeRateTime);

		ctBoxCollider2D.enabled = !ctBoxCollider2D.enabled;
		if (!ctBoxCollider2D.enabled) {
			ctSpriteRenderer.color = new Color (ctSpriteRenderer.color.r, ctSpriteRenderer.color.g, ctSpriteRenderer.color.b, 0.5f);
		} else {
			ctSpriteRenderer.color = new Color (ctSpriteRenderer.color.r, ctSpriteRenderer.color.g, ctSpriteRenderer.color.b, 1f);
		}
		StartCoroutine (ChangeMode ());
	}
}
