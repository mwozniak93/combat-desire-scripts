﻿using UnityEngine;
using System.Collections;

//[RequireComponent(typeof(Bullet))]
public class Punch : MonoBehaviour
{
	//delegate ()

	
	public delegate void Attack (float val);
	//	public  event Attack eAttack;
	//	public static BasicAttack instance;

	CharacterModel oCharacterModel;
	Animator ctAnimator;
	//	MovementController ctMovementController;
	Vector2 startHitPos;
	Vector2 StandingPosition;
	float dirX, dirY;
	States previousState;
	Bullet ctBulletUp, ctBulletDown, ctBulletLeft, ctBulletRight;
	SpriteRenderer ctSpriteRenderer;
	BoxCollider2D HitBoxUp, HitBoxDown, HitBoxLeft, HitBoxRight;
	bool isCooldown;
	bool isPunching = false;
	//	BoxCollider2D HitBoxCollider;
	void Start ()
	{
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		HitBoxUp = transform.Find ("HitBoxUp").GetComponent<BoxCollider2D> ();
		HitBoxUp.enabled = false;
		ctBulletUp = HitBoxUp.GetComponent<Bullet> ();

		HitBoxDown = transform.Find ("HitBoxDown").GetComponent<BoxCollider2D> ();
		HitBoxDown.enabled = false;
		ctBulletDown = HitBoxDown.GetComponent<Bullet> ();

		HitBoxLeft = transform.Find ("HitBoxLeft").GetComponent<BoxCollider2D> ();
		HitBoxLeft.enabled = false;
		ctBulletLeft = HitBoxLeft.GetComponent<Bullet> ();

		HitBoxRight = transform.Find ("HitBoxRight").GetComponent<BoxCollider2D> ();
		HitBoxRight.enabled = false;
		ctBulletRight = HitBoxRight.GetComponent<Bullet> ();
//		startHitPos = Hit.transform.position;
		ctAnimator = GetComponent<Animator> ();
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;

//		ctMovementController =GetComponent<MovementController>();
	}

	void OnEnable ()
	{
		if (HitBoxUp) {
			HitBoxUp.enabled = false;
		}
		if (HitBoxDown) {
			HitBoxDown.enabled = false;
		}
		if (HitBoxLeft) {
			HitBoxLeft.enabled = false;
		}
		if (HitBoxRight) {
			HitBoxRight.enabled = false;
		}

	}
	//X: 0.06416014, 0.1211056
	//Y : 0.16, 0.09
	public void PunchOn ()
	{
		if (oCharacterModel.PunchBtnPressTime <= 0.75f) {
			if (IsRequiredState () && !isPunching) {
				isPunching = true;
				Bullet ctBullet;
				BoxCollider2D Hit;
				Hit = HitBoxRight;
				ctBullet = ctBulletRight;
				dirX = oCharacterModel.Direction.X;
				dirY = oCharacterModel.Direction.Y;

				if (dirX == -1) {
					Hit = HitBoxLeft;
					ctBullet = ctBulletLeft;
				}
				if (dirX == 1) {
					Hit = HitBoxRight;
					ctBullet = ctBulletRight;
				}

				if (dirY == 1) {
					Hit = HitBoxUp;
					ctBullet = ctBulletUp;
				}

				if (dirY == -1) {
					Hit = HitBoxDown;
					ctBullet = ctBulletDown;
				}
				ctBullet.damage = oCharacterModel.KiLevel;
				ctBullet.oBulletOwner = oCharacterModel;
				ctBullet.Direction = oCharacterModel.Direction;
				ctBullet.explosionType = ExplosionType.SmokeSmall;

				float randomize = Random.Range (1, oCharacterModel.AttackAnimationsCount);
				string randomizedAttack = "IsAttacking" + randomize;
				previousState = oCharacterModel.States;
				oCharacterModel.OnPunch ();
				ctAnimator.SetBool (randomizedAttack, true);
				oCharacterModel.States = States.Attacking;
				StartCoroutine (StopPunching (randomizedAttack, Hit));

			}
		}
	}

	private bool IsRequiredState ()
	{
		if (oCharacterModel.States != States.Attacking && oCharacterModel.States != States.Charging && oCharacterModel.States != States.Guarding && oCharacterModel.States != States.SpeedAttack && oCharacterModel.States != States.SuperPunchPrepere && oCharacterModel.States != States.TKFocus) {
			return true;
		} else {
			return false;
		}
	}

	IEnumerator StopPunching (string randomizedAttack, BoxCollider2D Hit)
	{
		if (!isPunching) {
			yield break;
		}
		yield return new WaitForSeconds (0.12f);
		if (oCharacterModel.States == States.Attacking) {
			Hit.enabled = true;
			isCooldown = true;
		}
		yield return new WaitForSeconds (0.04f);
		Hit.enabled = false;
		yield return new WaitForSeconds (0.30f);
		ctAnimator.SetBool (randomizedAttack, false);
		if (oCharacterModel.States != States.SuperPunchPrepere) {
			oCharacterModel.States = States.Idle;
			isPunching = false;
		}
	}

}
