﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
	[HideInInspector]
	public float damage;
	[HideInInspector]
	public Rigidbody2D ctRigidbody2D;
	[HideInInspector]
	public VectorXY Direction;
	[SerializeField]
	bool shouldDeactivate = true;
	[HideInInspector]
	public ExplosionType explosionType;

	public CharacterModel oBulletOwner;
	public GameObject oBulletOnwerGO;
	// Use this for initialization
	void Awake ()
	{
		ctRigidbody2D = GetComponent<Rigidbody2D> ();

	}

	void OnEnable ()
	{
		//		oBulletOwner.eDie += DisableBullet;

	}

	void OnDisable ()
	{
//		oBulletOwner.eDie -= DisableBullet;

	}

	private void DisableBullet ()
	{
		InitExpplosion ();
		gameObject.SetActive (false);

	}

	void OnCollisionEnter2D (Collision2D coll)
	{
		if (explosionType != null) {
			GameObject explosion = EffectsPoolManager.instance.GetExplosion (explosionType);
			if (explosion) {
				explosion.transform.position = transform.position;
				explosion.SetActive (true);
			}
		}
		gameObject.SetActive (false);
//		}
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		
		if (coll.CompareTag ("Enemy") || coll.CompareTag ("Player") || coll.CompareTag ("Bullet") || coll.CompareTag ("BulletWave")) {
			InitExpplosion ();
			if (shouldDeactivate) {
				if (coll.gameObject.name != "Hit") {
					gameObject.SetActive (false);
				}
			}
		}

		if (gameObject.name == "Hit" && coll.name == "Hit") {
			StartCoroutine (PunchColl (coll.transform.position, coll.GetComponentInParent<CharacterContainer> ().oCharacter));
		}
	}

	void InitExpplosion ()
	{
		if (explosionType != null) {
			GameObject explosion = EffectsPoolManager.instance.GetExplosion (explosionType);
			if (explosion) {
				explosion.transform.position = transform.position;
				explosion.SetActive (true);
			}
		}
	}


	IEnumerator PunchColl (Vector2 pos, CharacterModel oEnemyModel)
	{
		float random = 1;
		if (random == 1) {
			GameObject hitExplosion = EffectsPoolManager.instance.GetExplosion (ExplosionType.Hit);
			if (hitExplosion) {
				hitExplosion.transform.position = pos;
				hitExplosion.SetActive (true);
				oEnemyModel.PushRate = 1.7f;
				yield return new WaitForSeconds (0.25f);
				oEnemyModel.PushRate = 1;
			}
		}
	}




}
