﻿using UnityEngine;
using System.Collections;

public class ItemDrop : MonoBehaviour
{
	CharacterModel oCharacter;
	Rigidbody2D itemRigidbody2D;
	float itemThowPower = 0;
	// Use this for initialization
	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		oCharacter.eDie += DropItem;
	}

	void OnEnable ()
	{
		if (oCharacter != null) {
			oCharacter.eDie += DropItem;
		}
	}

	void OnDisable ()
	{
		if (oCharacter != null) {
			oCharacter.eDie -= DropItem;
		}
	}

	void OnDestroy ()
	{
		oCharacter.eDie -= DropItem;
	}

	public void DropItem ()
	{
		if (oCharacter.Items.Count > 0) {
			int randomItemNo = Random.Range (0, oCharacter.Items.Count);
			ItemModel randomizedItem = oCharacter.Items [randomItemNo];

			ItemContainer item = EffectsPoolManager.instance.GetItem (randomizedItem.ItemType);
//			//Debug.Log ("randomized item : " + item.name);
			if (item == null) {
				return;	
			}
			if (item) {
				item.Item.ThrowDirection = oCharacter.Direction;
				item.transform.position = transform.position;
				item.gameObject.SetActive (true);
			}
		}
	}

}
