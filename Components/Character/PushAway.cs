﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(DamageTake))]
public class PushAway : MonoBehaviour
{
	CharacterModel oCharacterModel;
	DamageTake ctDamageTake;
	//	[SerializeField]
	//	float pushForce = 1.5f;
	// Use this for initialization
	Rigidbody2D ctRigidbody2D;

	void Start ()
	{
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		ctDamageTake = GetComponent<DamageTake> ();
		ctRigidbody2D = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate ()
	{
		push ();
	}

	void push ()
	{
		if (oCharacterModel.Oponent == CharacterType.Player) {
		}

		if (oCharacterModel.States == States.Hurting) {
			if (oCharacterModel.CharacterType == CharacterType.Player) {	
			}
			ctRigidbody2D.AddForce (new Vector2 (ctDamageTake.pushDirection.X * 40 * Time.deltaTime * oCharacterModel.PushForce * oCharacterModel.PushRate, ctDamageTake.pushDirection.Y * 40 * Time.deltaTime * oCharacterModel.PushForce * oCharacterModel.PushRate));
		}
	}
}
