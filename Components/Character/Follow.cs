﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Movement))]
public class Follow : MonoBehaviour
{
	Movement ctMovement;
	[HideInInspector]
	public Transform target;
	float DistanceToTarget;
	CharacterModel oCharacter;
	//	CharacterModel oCharacter;
	Vector2 dir;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter; 
		ctMovement = GetComponent<Movement> ();
	}

	public void FollowTarget ()
	{
//		//Debug.Log("Follow STATE :"  +target.position);
		CalcltateDistanceToTarget ();
		if (oCharacter.States == States.Following) {
			oCharacter.SpeedX = dir.x;
			oCharacter.SpeedY = dir.y;
		}
		if (oCharacter.States == States.StandingNear) {
			oCharacter.SpeedX = 0;
			oCharacter.SpeedY = 0;

		}
	}

	void CalcltateDistanceToTarget ()
	{
		if (target) {
			dir = target.position - transform.position;
			dir.Normalize ();
			DistanceToTarget = Vector2.Distance (target.position, transform.position);
			oCharacter.TargetDirection = new VectorXY (){ X = dir.x, Y = dir.y };
			oCharacter.DistanceToPlayer = DistanceToTarget;
		}
	}
		

}
