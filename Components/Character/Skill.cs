﻿using UnityEngine;
using System.Collections;
using EZCameraShake;

public class Skill : MonoBehaviour
{

	CharacterModel oCharacterModel;
	Animator ctAnimator;
	//	EffectsPoolManager snEffectsPoolManager;
	bool isPreperingEnergyWave = false;
	SpriteRenderer ctSpriteRenderer;
	GameObject effect;
	GameObject effectAuraPowerBeyond;
	GameObject effectExplosionPowerBeyond;
	float timer = 0;
	Bullet longWaveBullet;
	bool isShootingLongWave = false;
	[SerializeField]
	Texture2D transformationSprite, normalSprite;
	private Sprite[] transformationSpritesheet, normalSpritesheet;
	private Transform HitBoxUp, HitBoxDown, HitBoxLeft, HitBoxRight;
	SpriteSkin ctSpriteSkin;
	float energyUseWhenTransformed;
	float updateTimer = 0;
	bool isShootingWave = false;

	// Use this for initialization
	void Start ()
	{


		HitBoxUp = transform.Find ("HitBoxUp");
		HitBoxDown = transform.Find ("HitBoxDown");
		HitBoxLeft = transform.Find ("HitBoxLeft");
		HitBoxRight = transform.Find ("HitBoxRight");

		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		ctAnimator = GetComponent<Animator> ();
		ctSpriteSkin = GetComponent<SpriteSkin> ();
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		if (transformationSprite != null) {
			transformationSpritesheet = Resources.LoadAll<Sprite> (ctSpriteSkin.folderPath + transformationSprite.name);
		}
		if (normalSprite != null) {
			normalSpritesheet = Resources.LoadAll<Sprite> (ctSpriteSkin.folderPath + normalSprite.name);
		}

		oCharacterModel.eEnergyGatherOff += CancelWave;
		oCharacterModel.eCancelTransformation += CancelTransformation;
		oCharacterModel.eDie += TransformationOff;
		oCharacterModel.eLongWaveOff += StopLongWave;
		oCharacterModel.eTransformationCancel += TransformationOff;

		if (oCharacterModel.oCurrentSkill == null) {
			oCharacterModel.oCurrentSkill = oCharacterModel.PlayerSkillsList [0];	 
		}

//		snEffectsPoolManager = EffectsPoolManager.instance;
	
	}

	//	void OnEnable(){
	//
	//	}
	void OnDestroy ()
	{
		oCharacterModel.eEnergyGatherOff -= CancelWave;
		oCharacterModel.eCancelTransformation -= CancelTransformation;
		oCharacterModel.eDie -= TransformationOff;
		oCharacterModel.eTransformationCancel -= TransformationOff;
	
	}


	public IEnumerator ShootLongWave (float[] arr)
	{
//		ctAnimator.SetBool("PrepereEnergy",false);
		if (effect) {
			effect.SetActive (false);
			effect.transform.parent = null;
		}

		if ((oCharacterModel.States == States.Idle || oCharacterModel.States == States.StandingSteel) && !oCharacterModel.oCurrentSkill.IsCooldown && oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
			oCharacterModel.oCurrentSkill.LastCooldownVal = 0;
			isShootingLongWave = true;
			oCharacterModel.States = States.Attacking;
			oCharacterModel.LastState = oCharacterModel.States;
			ctAnimator.SetBool ("PrepereEnergy", true);
			foreach (float item in arr) {

				longWaveBullet = EffectsPoolManager.instance.GetBullet (oCharacterModel.oCurrentSkill.BulletType);
				yield return new WaitForSeconds (0.2f);
				Debug.Log ("isShootingLongWave=true; ; " + isShootingLongWave);
				ctAnimator.SetBool ("PrepereEnergy", false);
				if (isShootingLongWave) {
					ctAnimator.SetBool ("EnergyAttack2", true);
				}
				if (longWaveBullet && isShootingLongWave) {
					//					bullet.tag=oCharacterModel.CharacterType+ "Attack";
//					//Debug.Log("oCharacterModel.Direction.X ; " +oCharacterModel.Direction.X +  "oCharacterModel.Direction.Y " + oCharacterModel.Direction.Y);
					if (oCharacterModel.Direction.X == -1 && oCharacterModel.Direction.Y == 0) {
						longWaveBullet.transform.eulerAngles = new  Vector3 (0, 0, 0);
//						//Debug.Log("longWaveBullet.transform.rotation 0: " + longWaveBullet.transform.rotation.eulerAngles);
					}
					if (oCharacterModel.Direction.X == 1 && oCharacterModel.Direction.Y == 0) {
						longWaveBullet.transform.eulerAngles = new Vector3 (0, 0, 180);
//						//Debug.Log("longWaveBullet.transform.rotation 180: " + longWaveBullet.transform.rotation.eulerAngles);
					}
					if (oCharacterModel.Direction.Y == -1 && oCharacterModel.Direction.X == 0) {
						longWaveBullet.transform.eulerAngles = new Vector3 (0, 0, 90);
//						//Debug.Log("longWaveBullet.transform.rotation 90: " + longWaveBullet.transform.rotation.eulerAngles);
					}
					if (oCharacterModel.Direction.Y == 1 && oCharacterModel.Direction.X == 0) {
						longWaveBullet.transform.eulerAngles = new Vector3 (0, 0, 270);
//						//Debug.Log("longWaveBullet.transform.rotation 270: " + longWaveBullet.transform.rotation.eulerAngles);
					}
					longWaveBullet.gameObject.layer = LayerMask.NameToLayer (oCharacterModel.CharacterType + "Attack");
					longWaveBullet.transform.position = transform.position;
					longWaveBullet.transform.position = new Vector2 (transform.position.x + (ctSpriteRenderer.bounds.size.x / 2) * oCharacterModel.Direction.X, transform.position.y + (ctSpriteRenderer.bounds.size.y / 2) * oCharacterModel.Direction.Y);
					//					//Debug.Log(" bullet pos : " + bullet.transform.position +  "name :"  +bullet.name);
					longWaveBullet.gameObject.SetActive (true);
				
				
					Vector2 vel = new Vector2 (0, 0);
					if (oCharacterModel.Direction.Y != 0 && oCharacterModel.Direction.X == 0) {
						vel = new Vector2 (item * oCharacterModel.oCurrentSkill.Acceleration, 1 * oCharacterModel.Direction.Y * oCharacterModel.oCurrentSkill.Acceleration);
					} else if (oCharacterModel.Direction.Y == 0 && oCharacterModel.Direction.X != 0) {
						vel = new Vector2 (1 * oCharacterModel.Direction.X * oCharacterModel.oCurrentSkill.Acceleration, item * oCharacterModel.oCurrentSkill.Acceleration);
					}
					longWaveBullet.oBulletOwner = oCharacterModel;
					longWaveBullet.oBulletOnwerGO = gameObject;
					longWaveBullet.damage = oCharacterModel.KiLevel;
					longWaveBullet.Direction = new VectorXY (){ X = oCharacterModel.Direction.X, Y = oCharacterModel.Direction.Y };
					longWaveBullet.explosionType = oCharacterModel.oCurrentSkill.ExplosionType;
				}
			}
			ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), false);
		} else {
			StopLongWave ();
		}

	}


	public void StopLongWave ()
	{
		if (isShootingLongWave) {
			
		
			if (longWaveBullet) {
				longWaveBullet.gameObject.SetActive (false);
			}
			ctAnimator.SetBool ("EnergyAttack2", false);
			oCharacterModel.States = States.Idle;
			isShootingLongWave = false;
			oCharacterModel.LastState = States.Idle;
			Debug.Log ("Stopping float WAVE");
		}

//		isPreperingEnergyWave=false;
	}




	#region Cooldown

	void Update ()
	{
		updateTimer += Time.deltaTime;
		if (updateTimer > 1f) {
			updateTimer = 0;
			if (oCharacterModel.TransformState == TransformState.Transformed) {
				oCharacterModel.KiLevel -= (energyUseWhenTransformed / 100) * oCharacterModel.MaxKiLevel;
			}

			if (oCharacterModel.oCurrentSkill.IsContinousKiReduce && isShootingLongWave) {
				oCharacterModel.KiLevel -= ((oCharacterModel.oCurrentSkill.KiNeededInPercentage / 100) * oCharacterModel.MaxKiLevel);
//				oCharacterModel.ReduceKiPoints (oCharacterModel.oCurrentSkill.KiNeededInPercentage / 5);
				if (tag == "Player") {
					CameraShaker.Instance.ShakeOnce (0.4f, 2.25f, 0.5f, 0.31f);
				}
				if (!oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
					StopLongWave ();
				}
			}
		}

		timer += Time.deltaTime;
		if (oCharacterModel.CharacterType == CharacterType.Player) {
		}
		oCharacterModel.oCurrentSkill.LastCooldownVal += Time.deltaTime;
		if (oCharacterModel.oCurrentSkill.LastCooldownVal < oCharacterModel.oCurrentSkill.Cooldown) {
			oCharacterModel.oCurrentSkill.IsCooldown = true;
		} else if (oCharacterModel.oCurrentSkill.LastCooldownVal >= oCharacterModel.oCurrentSkill.Cooldown) {
			oCharacterModel.oCurrentSkill.IsCooldown = false;
		}
	}

	#endregion

	#region AttackTypes



	#region EnergyWave

	public void CancelWave ()
	{
		if (isShootingWave) {
			if (effect) {
				effect.SetActive (false);
				effect.transform.parent = null;
			}
			Debug.Log ("CancelWave " + oCharacterModel.LastState);
			ctAnimator.SetBool ("PrepereEnergy", false);
			oCharacterModel.States = States.Idle;
			oCharacterModel.LastState = States.Idle;
			isShootingWave = false;

			isPreperingEnergyWave = false;
		}
	}

	public void PrepereEnergyAttack ()
	{
		if (oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
			if (oCharacterModel.States == States.Idle && !oCharacterModel.oCurrentSkill.IsCooldown) {
				isPreperingEnergyWave = true;
				timer = 0;
				isShootingWave = true;
				ctAnimator.SetBool ("PrepereEnergy", true);
				oCharacterModel.States = States.StandingSteel;
				oCharacterModel.LastState = oCharacterModel.States;
				effect = EffectsPoolManager.instance.GetEffect (EffectType.EnergyGather);
				if (effect) {
					effect.transform.position = transform.position;
					effect.SetActive (true);
					effect.transform.SetParent (transform);
				}
			}
		} else {
			
		}
	}


	public IEnumerator ShootWave (float[] arr)
	{

		if (effect) {
			effect.SetActive (false);
			effect.transform.parent = null;
		}
		Debug.Log ("oCharacterModel.oCurrentSkill.KiNeededInPercentage ;" + oCharacterModel.oCurrentSkill.KiNeededInPercentage);
//		Debug.Log("oCharacterModel.IsEnoughKi(oCharacterModel.oCurrentSkill.KiNeededInPercentage) : " + oCharacterModel.IsEnoughKi(oCharacterModel.oCurrentSkill.KiNeededInPercentage));
		if ((oCharacterModel.States == States.Idle || oCharacterModel.States == States.StandingSteel) && !oCharacterModel.oCurrentSkill.IsCooldown && isPreperingEnergyWave && timer >= 0.5f && oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
			ctAnimator.SetBool ("PrepereEnergy", false);
			//Debug.Log("Is COoldown ? : "+ oCharacterModel.oCurrentSkill.IsCooldown + "arr : " +arr.Length);
			oCharacterModel.ReduceKiPoints (oCharacterModel.oCurrentSkill.KiNeededInPercentage);
			//			//Debug.Log("IN!!!!!");
			oCharacterModel.oCurrentSkill.LastCooldownVal = 0;

			foreach (float item in arr) {

				Bullet bullet = EffectsPoolManager.instance.GetBullet (oCharacterModel.oCurrentSkill.BulletType);
				//				//Debug.Log("Bu;llet got  : " +bullet.name +  " itemFlaot : " +item);
				if (bullet) {
					//					bullet.tag=oCharacterModel.CharacterType+ "Attack";
					bullet.gameObject.layer = LayerMask.NameToLayer (oCharacterModel.CharacterType + "Attack");
					bullet.transform.position = transform.position;
					bullet.transform.position = new Vector2 (transform.position.x + (ctSpriteRenderer.bounds.size.x / 2) * oCharacterModel.Direction.X, transform.position.y + (ctSpriteRenderer.bounds.size.y / 2) * oCharacterModel.Direction.Y);
					//					//Debug.Log(" bullet pos : " + bullet.transform.position +  "name :"  +bullet.name);
					bullet.gameObject.SetActive (true);
					oCharacterModel.States = States.Attacking;
					oCharacterModel.LastState = oCharacterModel.States;
					Vector2 vel = new Vector2 (0, 0);
					if (oCharacterModel.Direction.Y != 0 && oCharacterModel.Direction.X == 0) {
						vel = new Vector2 (item * oCharacterModel.oCurrentSkill.Acceleration, 1 * oCharacterModel.Direction.Y * oCharacterModel.oCurrentSkill.Acceleration);
					} else if (oCharacterModel.Direction.Y == 0 && oCharacterModel.Direction.X != 0) {
						vel = new Vector2 (1 * oCharacterModel.Direction.X * oCharacterModel.oCurrentSkill.Acceleration, item * oCharacterModel.oCurrentSkill.Acceleration);
					}
					bullet.oBulletOwner = oCharacterModel;
					bullet.oBulletOnwerGO = gameObject;
					bullet.damage = oCharacterModel.KiLevel;
//					bullet.Direction = oCharacterModel.Direction;
					bullet.Direction = new VectorXY (){ X = oCharacterModel.Direction.X, Y = oCharacterModel.Direction.Y };
//					if(oCharacterModel.oCurrentSkill.ExplosionType!=ExplosionType){
					bullet.explosionType = oCharacterModel.oCurrentSkill.ExplosionType;
//					}
					ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), true);
					bullet.ctRigidbody2D.velocity = vel;
				}
				yield return new WaitForSeconds (0.01f);

			}
			yield return new WaitForSeconds (0.2f);

			ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), false);
			oCharacterModel.States = States.Idle;
			isShootingWave = false;
			isPreperingEnergyWave = false;
		} else {
			CancelWave ();
		}
//		oCharacterModel.ReduceKiPoints(oCharacterModel.oCurrentSkill.KiNeededInPercentage);

	}

	#endregion

	#region Shooting Energy

	public IEnumerator Shoot (float[] arr)
	{
//		ctAnimator.SetBool("PrepereEnergy",false);
	
		if (oCharacterModel.Direction.X == 0 && oCharacterModel.Direction.Y == 0) {
			yield break;
		}
		if (oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
			if ((oCharacterModel.States == States.Following || oCharacterModel.States == States.Idle || oCharacterModel.States == States.StandingSteel) && !oCharacterModel.oCurrentSkill.IsCooldown) {
				oCharacterModel.ReduceKiPoints (oCharacterModel.oCurrentSkill.KiNeededInPercentage);
				ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), true);
				oCharacterModel.States = States.Attacking;

				yield return new WaitForSeconds (0.15f);

				oCharacterModel.oCurrentSkill.LastCooldownVal = 0;

				foreach (float item in arr) {

					Bullet bullet = EffectsPoolManager.instance.GetBullet (oCharacterModel.oCurrentSkill.BulletType);
//				//Debug.Log("Bu;llet got  : " +bullet.name +  " itemFlaot : " +item);
					if (bullet) {
//					bullet.tag=oCharacterModel.CharacterType+ "Attack";
						bullet.gameObject.layer = LayerMask.NameToLayer (oCharacterModel.CharacterType + "Attack");
						bullet.transform.position = transform.position;
						if (oCharacterModel.oCurrentSkill.BulletType == BulletType.EnergyShield || oCharacterModel.oCurrentSkill.BulletType == BulletType.NatureShield) {
							bullet.transform.SetParent (transform);
						}
//					//Debug.Log(" bullet pos : " + bullet.transform.position +  "name :"  +bullet.name);
						bullet.gameObject.SetActive (true);
				

						if (oCharacterModel.oCurrentSkill.BulletType != BulletType.EnergyShield && oCharacterModel.oCurrentSkill.BulletType != BulletType.NatureShield) {
							if (oCharacterModel.Direction.X == 1 && oCharacterModel.Direction.Y == 0) {
								bullet.transform.position = HitBoxRight.transform.position;
							} else if (oCharacterModel.Direction.X == -1 && oCharacterModel.Direction.Y == 0) {
								bullet.transform.position = HitBoxLeft.transform.position;
							} else if (oCharacterModel.Direction.X == 0 && oCharacterModel.Direction.Y == 1) {
								bullet.transform.position = HitBoxUp.transform.position;
							} else if (oCharacterModel.Direction.X == 0 && oCharacterModel.Direction.Y == -1) {
								bullet.transform.position = HitBoxDown.transform.position;
							}
						}

						Vector2 vel = new Vector2 (0, 0);
						if (oCharacterModel.Direction.Y != 0 && oCharacterModel.Direction.X == 0) {
							vel = new Vector2 (item * oCharacterModel.oCurrentSkill.Acceleration, 1 * oCharacterModel.Direction.Y * oCharacterModel.oCurrentSkill.Acceleration);
						} else if (oCharacterModel.Direction.Y == 0 && oCharacterModel.Direction.X != 0) {
							vel = new Vector2 (1 * oCharacterModel.Direction.X * oCharacterModel.oCurrentSkill.Acceleration, item * oCharacterModel.oCurrentSkill.Acceleration);
						}
						bullet.damage = oCharacterModel.KiLevel;
						bullet.Direction = new VectorXY (){ X = oCharacterModel.Direction.X, Y = oCharacterModel.Direction.Y };
						bullet.oBulletOwner = oCharacterModel;
						bullet.oBulletOnwerGO = gameObject;
						if (oCharacterModel.oCurrentSkill.ExplosionType != null) {
							bullet.explosionType = oCharacterModel.oCurrentSkill.ExplosionType;
						}
					
						bullet.ctRigidbody2D.velocity = vel;
					}
					yield return new WaitForSeconds (oCharacterModel.oCurrentSkill.ShootRateForMultiBullets);
			

				}
				yield return new WaitForSeconds (0.4f);
				ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), false);
				oCharacterModel.States = States.Idle;
			}
		} else {
			
		}
	}

	#region AIM Shoot

	public IEnumerator ShootAIM (float[] arr)
	{
		//		ctAnimator.SetBool("PrepereEnergy",false);
		//		//Debug.Log("Is COoldown ? : "+ oCharacterModel.oCurrentSkill.IsCooldown + "arr : " +arr.Length);
		if (oCharacterModel.Direction.X == 0 && oCharacterModel.Direction.Y == 0) {
			yield break;
		}
		if (oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
			if (oCharacterModel.States == States.Following || (oCharacterModel.States == States.Idle || oCharacterModel.States == States.StandingSteel) && !oCharacterModel.oCurrentSkill.IsCooldown) {
				oCharacterModel.ReduceKiPoints (oCharacterModel.oCurrentSkill.KiNeededInPercentage);
				//			//Debug.Log("IN!!!!!");
				oCharacterModel.oCurrentSkill.LastCooldownVal = 0;


				foreach (float item in arr) {

					Bullet bullet = EffectsPoolManager.instance.GetBullet (oCharacterModel.oCurrentSkill.BulletType);
					//				//Debug.Log("Bu;llet got  : " +bullet.name +  " itemFlaot : " +item);
					if (bullet) {
						//					bullet.tag=oCharacterModel.CharacterType+ "Attack";
						bullet.gameObject.layer = LayerMask.NameToLayer (oCharacterModel.CharacterType + "Attack");
						//bullet.transform.position = transform.position;
						//					//Debug.Log(" bullet pos : " + bullet.transform.position +  "name :"  +bullet.name);
						bullet.gameObject.SetActive (true);
						oCharacterModel.States = States.Attacking;
						Vector2 vel = new Vector2 (0, 0);

						if (oCharacterModel.Direction.X == 1 && oCharacterModel.Direction.Y == 0) {
							bullet.transform.position = HitBoxRight.transform.position;
						} else if (oCharacterModel.Direction.X == -1 && oCharacterModel.Direction.Y == 0) {
							bullet.transform.position = HitBoxLeft.transform.position;
						} else if (oCharacterModel.Direction.X == 0 && oCharacterModel.Direction.Y == 1) {
							bullet.transform.position = HitBoxUp.transform.position;
						} else if (oCharacterModel.Direction.X == 0 && oCharacterModel.Direction.Y == -1) {
							bullet.transform.position = HitBoxDown.transform.position;
						}

//					if(oCharacterModel.Direction.Y!=0 && oCharacterModel.Direction.X==0){
//						vel= new Vector2(oCharacterModel.oCurrentSkill.Acceleration* oCharacterModel.TargetDirection.X, oCharacterModel.TargetDirection.Y* oCharacterModel.oCurrentSkill.Acceleration);
//					}
//					else if(oCharacterModel.Direction.Y==0 && oCharacterModel.Direction.X!=0){
						vel = new Vector2 (oCharacterModel.TargetDirection.X * oCharacterModel.oCurrentSkill.Acceleration, oCharacterModel.oCurrentSkill.Acceleration * oCharacterModel.TargetDirection.Y);
//					}
						bullet.damage = oCharacterModel.KiLevel;
						bullet.oBulletOwner = oCharacterModel;
						bullet.oBulletOnwerGO = gameObject;
						bullet.Direction = new VectorXY () {
							X = Mathf.RoundToInt (oCharacterModel.TargetDirection.X),
							Y = Mathf.RoundToInt (oCharacterModel.TargetDirection.Y)
						};
//					//Debug.Log("Bullet dir : "+ bullet.Direction);
						if (oCharacterModel.oCurrentSkill.ExplosionType != null) {
							bullet.explosionType = oCharacterModel.oCurrentSkill.ExplosionType;
						}
						ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), true);
						bullet.ctRigidbody2D.velocity = vel;
					}
					yield return new WaitForSeconds (oCharacterModel.oCurrentSkill.ShootRateForMultiBullets);


				}
				yield return new WaitForSeconds (0.39f);
				ctAnimator.SetBool (oCharacterModel.oCurrentSkill.AttakAnim.ToString (), false);
				oCharacterModel.States = States.Idle;
			}
		} else {
			
		}
	}

	#endregion

	#endregion

	#region Transformation

	public  void UseTransformation ()
	{
		if (oCharacterModel.TransformState == TransformState.Normal && oCharacterModel.IsEnoughKi (oCharacterModel.oCurrentSkill.KiNeededInPercentage)) {
			TransformationOn ();

		}
//		}
		if (oCharacterModel.TransformState == TransformState.Transformed) {
			TransformationOff ();
		}
	}

	IEnumerator SpriteColorBlink ()
	{
		for (int i = 0; i < 18; i++) {
			yield return new WaitForSeconds (0.05f);
			ctSpriteRenderer.color = Color.black;
			yield return new WaitForSeconds (0.05f);
			ctSpriteRenderer.color = Color.white;
		}
	}

	void CancelTransformation ()
	{
//		Debug.Log ("Canceling transformation : LastState: " + oCharacterModel.LastState);
		if (oCharacterModel.LastState == States.Transforming) {
//			Debug.Log("CANCEL");
			ctSpriteSkin.newSprite = normalSprite;
			ctSpriteRenderer.color = Color.white;
			ctAnimator.SetBool ("Transform", false);
			oCharacterModel.States = States.Idle;
			oCharacterModel.LastState = States.Idle;
			if (effectAuraPowerBeyond) {
				effectAuraPowerBeyond.SetActive (false);
			}
			if (effectExplosionPowerBeyond) {
				effectExplosionPowerBeyond.SetActive (false);
			}
		}
	}

	public void TransformationOn ()
	{
		if (oCharacterModel.States == States.Idle && !oCharacterModel.oCurrentSkill.IsCooldown) {
			oCharacterModel.ReduceKiPoints (oCharacterModel.oCurrentSkill.KiNeededInPercentage);
			StartCoroutine (StartTransformation ());
			oCharacterModel.States = States.Transforming;
			oCharacterModel.LastState = oCharacterModel.States;
			ctAnimator.SetBool ("Transform", true);
		}
		//		return default(T);
	}

	IEnumerator StartTransformation ()
	{
		effectAuraPowerBeyond = EffectsPoolManager.instance.GetEffect (EffectType.PowerBeyondAura);
		effectExplosionPowerBeyond = EffectsPoolManager.instance.GetEffect (EffectType.PowerBeyondExplosion);
		effectExplosionPowerBeyond.transform.position = transform.position;
		effectAuraPowerBeyond.transform.position = transform.position;
		effectAuraPowerBeyond.SetActive (true);

		yield return new WaitForSeconds (1.5f);
		if (oCharacterModel.States == States.Transforming) {
			ctSpriteRenderer.color = Color.black;
			effectExplosionPowerBeyond.SetActive (true);
		}
		yield return new WaitForSeconds (0.4f);
		if (oCharacterModel.States == States.Transforming) {
			ctSpriteRenderer.color = Color.white;
			ctAnimator.SetBool ("Transform", false);
			oCharacterModel.States = States.Idle;
			effectAuraPowerBeyond.SetActive (false);
			effectExplosionPowerBeyond.SetActive (false);
//		ctAnimator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animators/HeroTransform");
			oCharacterModel.KiLevelMultipler = 2;
			oCharacterModel.KiLevel = oCharacterModel.MaxKiLevel;
			oCharacterModel.TransformState = TransformState.Transformed; 
			ctSpriteSkin.newSprite = transformationSprite;
			ctSpriteSkin.newSpritesheet = transformationSpritesheet;
			oCharacterModel.oCurrentSkill.LastCooldownVal = 0;
			energyUseWhenTransformed = oCharacterModel.oCurrentSkill.KiNeededInPercentage;
			oCharacterModel.OnTransform ();
		}
	}

	public void  TransformationOff ()
	{
		if (oCharacterModel.TransformState == TransformState.Transformed) {
//		ctAnimator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("Animators/HeroNormal");
			oCharacterModel.KiLevelMultipler = 1;
			oCharacterModel.TransformState = TransformState.Normal; 
			oCharacterModel.KiLevel = (float)oCharacterModel.KiLvlPercentage;
	
			ctSpriteSkin.newSprite = normalSprite;
			ctSpriteSkin.newSpritesheet = normalSpritesheet;
			oCharacterModel.OnTransform ();
			//Debug.Log("GHRAAAAAAAAAAAAAA!!!!!!!!!!");
		}
		//		return default(T);
	}

	#endregion

	#endregion

}
