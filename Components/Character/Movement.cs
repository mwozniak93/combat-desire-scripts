﻿using UnityEngine;
using System.Collections;
using CnControls;

public class Movement : MonoBehaviour
{
	[HideInInspector]
	public float speedX, speedY, lastSpeedX, lastSpeedY;
	CharacterModel oCharacter;
	Animator ctAnimator;

	public delegate void Move (float val);

	public event Move eMove;
	//	GameManager oGameManager;
	Vector2 Direction;
	[HideInInspector]
	public Rigidbody2D ctRigdbody2D;
	float xYDifference;

	void Start ()
	{
//		oGameManager=GameManager.instance;
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		ctAnimator = GetComponent<Animator> ();
		ctRigdbody2D = GetComponent<Rigidbody2D> ();
		oCharacter.DirectionChange += UpdateDirection;
	}

	void OnDestroy ()
	{
		oCharacter.DirectionChange -= UpdateDirection;

	}

	void CheckDirection ()
	{
		int angle;
		int face = 4;
//		if(oCharacter.Oponent== CharacterType.Enemy){


		if (Mathf.Abs (oCharacter.SpeedX) + Mathf.Abs (oCharacter.SpeedY) > 0.01f) {
	
			angle = Mathf.RoundToInt (Mathf.Atan2 (oCharacter.SpeedX, oCharacter.SpeedY) / Mathf.PI / 2 * face);
			//script below is to make the result match your format
			if (angle < 0)
				angle += face;
			angle += 1;

			if (angle == 1) {
				Direction = new Vector2 (0, 1);
			}
			if (angle == 2) {
				Direction = new Vector2 (1, 0);
			}
			if (angle == 3) {
				Direction = new Vector2 (0, -1);
			}
			if (angle == 4) {
				Direction = new Vector2 (-1, 0);
			}

		} else {

		}

		oCharacter.Direction.X = Direction.x;
		oCharacter.Direction.Y = Direction.y;
		if (oCharacter.Oponent == CharacterType.Enemy) {
		}
		
		
	}

	
	void FixedUpdate ()
	{
		ctRigdbody2D.velocity = new Vector2 (oCharacter.SpeedX * oCharacter.Speed, oCharacter.SpeedY * oCharacter.Speed);
		if ((oCharacter.States == States.Idle || oCharacter.States == States.Walking || oCharacter.States == States.Following) && (oCharacter.SpeedX != 0 || oCharacter.SpeedY != 0)) {
			ctAnimator.SetBool ("Walking", true);
			setPlayerDirectionAnim (oCharacter.SpeedX, oCharacter.SpeedY);
			if (oCharacter.SpeedX != 0 && oCharacter.SpeedY != 0) {
				lastSpeedX = oCharacter.SpeedX;
				lastSpeedY = oCharacter.SpeedY;
			}
		} else if (oCharacter.States == States.SpeedAttack) {
			oCharacter.SpeedX = oCharacter.Direction.X;
			oCharacter.SpeedY = oCharacter.Direction.Y;
		} else {
			oCharacter.SpeedX = 0;
			oCharacter.SpeedY = 0;
			ctRigdbody2D.velocity = Vector2.zero;
			ctAnimator.SetBool ("Walking", false);
		}
		CheckDirection ();
		if (oCharacter.Oponent == CharacterType.Player) {
//			////Debug.Log("ELSE "  + oCharacter.States);
//			////Debug.Log("velocirt : " + ctRigdbody2D.velocity);
//		////Debug.Log("2 UpdateDirection  X : " + oCharacter.Direction.X + " Y : " +  oCharacter.Direction.Y);
		}
	}

	public void setPlayerDirectionAnim (float horizInput, float vertInput)
	{
		ctAnimator.SetFloat ("SpeedX", horizInput);
		ctAnimator.SetFloat ("SpeedY", vertInput);
		ctAnimator.SetFloat ("LastSpeedX", horizInput);
		ctAnimator.SetFloat ("LastSpeedY", vertInput);
	}


	public void UpdateDirection (float horizInput, float vertInput)
	{
		ctAnimator.SetFloat ("SpeedX", horizInput);
		ctAnimator.SetFloat ("SpeedY", vertInput);
		ctAnimator.SetFloat ("LastSpeedX", horizInput);
		ctAnimator.SetFloat ("LastSpeedY", vertInput);
		Direction = new Vector2 (horizInput, vertInput);

	}
}
