﻿using UnityEngine;
using System.Collections;

public class Guard : MonoBehaviour
{
	CharacterModel oCharacterModel;
	Animator ctAnimator;
	States previousState;
	// Use this for initialization
	void Start ()
	{
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		ctAnimator = GetComponent<Animator> ();
//		oCharacterModel.ePowerupChange+=GuardOn
	}

	public void GuardOn ()
	{
	
		previousState = oCharacterModel.States;

		if (oCharacterModel.States == States.Idle) {
			oCharacterModel.States = States.Guarding;
			StartCoroutine (SetGuardAnim ());
		}
	}

	IEnumerator SetGuardAnim ()
	{
		yield return new WaitForSeconds (0.1f);
		if (oCharacterModel.States == States.Guarding) {
			ctAnimator.SetBool ("IsGuarding", true);
		}
	}

	public void GuardOff ()
	{
		if (oCharacterModel.States == States.Guarding || oCharacterModel.States == States.Hurting) {
			oCharacterModel.States = States.Idle;
			ctAnimator.SetBool ("IsGuarding", false);
		}

	}
		
}
