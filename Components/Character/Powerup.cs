﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EZCameraShake;


public class Powerup : MonoBehaviour
{

	CharacterModel oCharacterModel;
	Animator ctAnimator;
	GameObject powerupAura;
	GameObject aura;
	GameObject bonusPowerUpAura;
	GameObject bonusAura;
	float updateTimer = 0;
	float kiPercentage;
	float AuraStartValue = 0.8f;
	ParticleSystem[] ctAuraParticles;
	ParticleSystem[] ctPowerUpParticles;
	ParticleSystem[] ctBonusPowerupAuraParticles;
	ParticleSystem[] ctBonusAuraParticles;
	// Use this for initialization
	void Start ()
	{
		powerupAura = transform.Find ("PowerupAura").gameObject;
		aura = transform.Find ("Aura").gameObject;
		bonusPowerUpAura = transform.Find ("BonusPowerupAura").gameObject;
		bonusAura = transform.Find ("BonusAura").gameObject;


		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		ctAnimator = GetComponent<Animator> ();
		oCharacterModel.eKiChange += GetKiPercentage;
		oCharacterModel.ePowerupChange += PowerUpOff;
		oCharacterModel.eAuraChange += ChangePowerUpAura;
		ctAuraParticles = aura.GetComponentsInChildren<ParticleSystem> ();
		ctPowerUpParticles = powerupAura.GetComponentsInChildren<ParticleSystem> ();
		ctBonusPowerupAuraParticles = bonusPowerUpAura.GetComponentsInChildren<ParticleSystem> ();
		ctBonusAuraParticles = bonusAura.GetComponentsInChildren<ParticleSystem> ();

		oCharacterModel.eTransformation += ChangeAuraColor;
		ChangeAuraColor ();

	}

	void OnDestroy ()
	{
		oCharacterModel.eKiChange -= GetKiPercentage;
		oCharacterModel.ePowerupChange -= PowerUpOff;
		oCharacterModel.eAuraChange -= ChangePowerUpAura;
	}

	void ChangeAuraColor ()
	{

		Color newColorPowerup = new Color ();
		Color newColorAura = new Color ();
		if (DataManager.instance.oPlayer.SelectedShard.Id != 0) {
			foreach (var item in ctBonusPowerupAuraParticles) {
				item.startColor = new Color32 (
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.r,
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.g,
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.b,
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.a
				);
				;
			}
			foreach (var item in ctBonusAuraParticles) {
				item.startColor = new Color32 (
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.r,
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.g,
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.b,
					(byte)DataManager.instance.oPlayer.SelectedShard.Color.a
				);
				;
			}
		}
		
		if (oCharacterModel.TransformState == TransformState.Normal) {
			newColorPowerup = new Color32 (79, 182, 255, 140);
			newColorAura = new Color32 (79, 182, 255, 140);
		} else if (oCharacterModel.TransformState == TransformState.Transformed) {
			newColorPowerup = new Color32 (99, 186, 0, 140);
			newColorAura = new Color32 (99, 186, 0, 140);
		}
		foreach (var item in ctAuraParticles) {
			item.startColor = newColorAura;
		}
		foreach (var item in ctPowerUpParticles) {
			item.startColor = newColorPowerup;
		}

	}

	public void PowerupOn ()
	{
//		oCharacterModel.States=States.Idle ;
//		//Debug.Log("oCharacterModel.oState ;"  +oCharacterModel.States);
		if (oCharacterModel.States == States.Idle) {
//		//Debug.Log("Powerupon");
			oCharacterModel.States = States.Charging;
			updateTimer = 0;
			ctAnimator.SetBool ("IsCharging", true);
			AudioManager.Instance.Play (AudioType.PowerUp);
			if (DataManager.instance.oPlayer.SelectedShard.Id == 0) {
				powerupAura.SetActive (true);
			}
			if (DataManager.instance.oPlayer.SelectedShard.Id != 0) {
				bonusPowerUpAura.SetActive (true);
			}

		} else if (oCharacterModel.States == States.Charging || oCharacterModel.States == States.Hurting || oCharacterModel.States == States.Guarding || oCharacterModel.States == States.SuperPunchPrepere) {
			PowerUpOff ();
		}
	}

	public void PowerUpOff ()
	{
		if (oCharacterModel.States != States.Guarding && oCharacterModel.States != States.SuperPunchPrepere && oCharacterModel.States != States.Attacking && oCharacterModel.States != States.StandingSteel && oCharacterModel.States != States.TKFocus && oCharacterModel.States != States.TKShoot) {	
			oCharacterModel.States = States.Idle;
		}
		ctAnimator.SetBool ("IsCharging", false);
		AudioManager.Instance.StopPlaying ();
//		ctAnimator.SetBool("IsGuarding",false);
		powerupAura.SetActive (false);
		if (DataManager.instance.oPlayer.SelectedShard.Id != 0) {
			bonusPowerUpAura.SetActive (false);
		}
	}

	public void AuraOn ()
	{
		if (!aura.activeInHierarchy) {
			if (DataManager.instance.oPlayer.SelectedShard.Id == 0) {
				aura.SetActive (true);
			}
		}
		if (DataManager.instance.oPlayer.SelectedShard.Id != 0) {
			if (!bonusAura.activeInHierarchy) {
				bonusAura.SetActive (true);

			}
		}
	}

	public void AuraOff ()
	{
		if (aura.activeInHierarchy) {
			aura.SetActive (false);
		}
		if (DataManager.instance.oPlayer.SelectedShard.Id != 0) {
			if (bonusAura.activeInHierarchy) {
				bonusAura.SetActive (false);
			}
		}
	}


	public void ChangePowerUpAura (bool val)
	{
//		if(aura.activeInHierarchy){
		powerupAura.SetActive (val);
		if (DataManager.instance.oPlayer.SelectedShard.Id != 0) {
//			if (bonusAura.activeInHierarchy) {
			bonusPowerUpAura.SetActive (val);
//			}
		}
//		}
	}

	void GetKiPercentage (float kiLevel, float maxKiLevel)
	{
		kiPercentage = (float)kiLevel / (float)maxKiLevel;
		if (kiPercentage >= AuraStartValue) {
			AuraOn ();

		} else if (kiPercentage < AuraStartValue) {
			AuraOff ();
		}

	}

	void Update ()
	{
		updateTimer += Time.deltaTime;
		if (oCharacterModel.States == States.Charging) {
			CameraShaker.Instance.ShakeOnce (0.4f, 0.25f, 0.5f, 0.31f);
			if (updateTimer > 0.45f) {
				updateTimer = 0;
				oCharacterModel.ChargeKi (0.04f * oCharacterModel.MaxKiLevel);
			}
		}
	}
}
