﻿using UnityEngine;
using System.Collections;

public class SensePowerLevel : MonoBehaviour
{
	//	GameObject sensePowerCanvas;
	// Use this for initialization
	CharacterModel oCharacter;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
//		sensePowerCanvas = GameObject.Find ("CanvasSensePower");
	}

	public void SensePower (bool val)
	{
		if (val) {
			oCharacter.States = States.StandingSteel;
		} else {
			oCharacter.States = States.Idle;
		}
		DataManager.instance.oPlayer.OnSensePower (val);
	}
}
