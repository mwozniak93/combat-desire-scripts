﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;


public class ShardAffect:MonoBehaviour
{
	CharacterModel oCharacter;
	ShardModel selectedShard;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		selectedShard = DataManager.instance.oPlayer.SelectedShard;

		if (selectedShard.Id != 0) {
			if (selectedShard.SpeedBonus != 0) {
				oCharacter.Speed = oCharacter.Speed * DataManager.instance.oPlayer.SelectedShard.SpeedBonus;
			}
			if (selectedShard.MaxKiLevelBaseBonus != 0) {
				oCharacter.MaxKiLevelBase = oCharacter.MaxKiLevelBase * (float)DataManager.instance.oPlayer.SelectedShard.MaxKiLevelBaseBonus;
			}
			if (selectedShard.HealthRecoveryAmount != 0) {
				StartCoroutine (ShaderAction ());
			}
		}
	}

	IEnumerator ShaderAction ()
	{
		yield return new WaitForSeconds (60f);

		oCharacter.AddHp (oCharacter.MaxHealth * selectedShard.HealthRecoveryAmount / 100);
		StartCoroutine (ShaderAction ());
	}

}

