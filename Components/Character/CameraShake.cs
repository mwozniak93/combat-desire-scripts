﻿using System;
using UnityEngine;
using EZCameraShake;

public class CameraShake:MonoBehaviour
{
	 
	CharacterModel oCharacter;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		oCharacter.eDamageTaken += Shake;

	}

	void Shake (float dmgVal)
	{
		CameraShaker.Instance.ShakeOnce (1, 3.5f, 0.5f, 0.31f);

	}

}

