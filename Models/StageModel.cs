﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;


[System.Serializable]
public class StageModel
{

	public List <WaveBonus> WavesBonuses;
	public List<ShardRandomizer> ShardsToRandomize;

	public void InitWavesBonusses ()
	{
		int countNeeded = 1;
		int previousCountNeeded = 0;
		int waveCountMultipler = 0;
		int tempCounter = 0;
		for (int i = 1; i <= 11; i++) {
			int temp = previousCountNeeded;
			previousCountNeeded = countNeeded;
			countNeeded = temp + countNeeded;
			tempCounter = (i % 2 == 0) ? tempCounter + 1 : tempCounter;
			waveCountMultipler += 3 + tempCounter;
			if (i == 1) {
				waveCountMultipler = 1;
			}
			WavesBonuses.Add (new WaveBonus () {
				Id = i,
				WaveCountNeeded = waveCountMultipler,
				CoinsBonus = 220 * (countNeeded + tempCounter),
				PowerBonus = 80 * (countNeeded + tempCounter)
			});
		}
	
	}

	public bool IsAnyWaveBonusToGet ()
	{
		bool result = false;
		foreach (var item in WavesBonuses) {
			if (item.IsUnlcoked && !item.IsCompleted) {
				result = true;
			}
		}
		return result;
	
	}

	public void MultiplyEnemies (int count)
	{
		foreach (var item in Waves) {
			List<EnemyType> enemyTypes = new List<EnemyType> ();
			List<EnemyType> enemyTypes2 = new List<EnemyType> ();
			foreach (var oEnemy in item.EnemyTypes) {
				
				EnemyType oEnemyType = oEnemy;
				EnemyType oEnemyType2 = oEnemy;
				enemyTypes.Add (oEnemyType);
				enemyTypes2.Add (oEnemyType2);
			}
			for (int i = 0; i <= count; i++) {
				
				item.EnemyTypes = new List<EnemyType> ();
				var newList = enemyTypes2.Concat (enemyTypes);
				foreach (var item2 in newList) {
					item.EnemyTypes.Add (item2);
				}
				//Debug.Log (" item.EnemyTypes. : " + item.EnemyTypes.Count + "enemyTypes counmt : " + enemyTypes.Count);
			}
		}
	
	}

	public void UnlockWaveBonus ()
	{
		foreach (var item in WavesBonuses) {
			if (MissionWaves.CurrentAmount > item.WaveCountNeeded) {
				item.IsUnlcoked = true;
			}
		}
	}


	public ShardRandomizer RandomizeShard ()
	{
		int randomNumber = UnityEngine.Random.Range (0, 100);
		foreach (var item in ShardsToRandomize) {
			if (randomNumber < item.ChanceToGetRandomizedEnd && randomNumber >= item.ChanceToGetRandomizedStart) {
				return item;
			}
		
		}
		return null;
	}

	public int WaveModRequiredForBoss;
	//	public BonusModel ShardBonus;
	[SerializeField]
	float coinsMultipler;

	public float CoinsMultipler {
		get{ return coinsMultipler; }
		set{ coinsMultipler = value; }
	}

	//	[SerializeField]
	//	float kiLvlExp;
	//	public float KiLvlExp{
	//		get{return kiLvlExp;}
	//		set{kiLvlExp =value;}
	//	}

	[SerializeField]
	float waveMultipler;

	public float WaveMultipler {
		get{ return waveMultipler; }
		set{ waveMultipler = value; }
	}

	[SerializeField]
	float startWaveMultipler;

	public float StartWaveMultipler {
		get{ return startWaveMultipler; }
		set{ startWaveMultipler = value; }
	}

	[SerializeField]
	Wave currentWave;

	public Wave CurrentWave {
		get{ return currentWave; }
		set{ currentWave = value; }
	}

	[SerializeField]
	float expMultipler;

	public float ExpMultipler {
		get{ return expMultipler; }
		set{ expMultipler = value; }
	}

	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set{ id = value; }
	}

	public EnemyType EnemyBoss;

	//	[SerializeField]
	//	float kiLevelFactor;
	//	public float KiLevelFactor{
	//		get{return kiLevelFactor;}
	//		set{kiLevelFactor =value;}
	//	}
	[SerializeField]
	bool isLocked;

	public bool IsLocked {
		get{ return isLocked; }
		set{ isLocked = value; }
	}

	[SerializeField]
	bool isFinished;

	public bool IsFinished {
		get{ return isFinished; }
		set{ isFinished = value; }
	}

	[SerializeField]
	string mapPath;

	public string MapPath {
		get{ return mapPath; }
		set{ mapPath = value; }
	}

	[SerializeField]
	Mission missionWaves;

	public Mission MissionWaves {
		get{ return missionWaves; }
		set{ missionWaves = value; }
	}

	//
	//	[SerializeField]
	//	Mission missionCrystals;
	//	public Mission MissionCrystals{
	//		get{return missionCrystals;}
	//		set{missionCrystals =value;}
	//	}
	//

	[SerializeField]
	float powerLvlMultipler;

	public float PowerLvlMultipler {
		get{ return powerLvlMultipler; }
		set{ powerLvlMultipler = value; }
	}

	public void Unlock ()
	{
		IsLocked = false;
	}

	public void Finish ()
	{
		IsFinished = true;
	}

	public List<Wave> Waves;

	public delegate void StageComplete ();

	[field:NonSerialized]
	public event StageComplete eStageComplete;

	public void OnStageCompleted ()
	{
		if (eStageComplete != null) {
			eStageComplete ();
		}
	}

	public int MaxSkeletonWarriorCount, MaxBlueGoblinsCount, MaxGreenGoblinsCount, MaxSkeletonWizardCount, MaxSlimeBlueCount, MaxSkeletonCount, MaxRedGoblinCount, MaxYellowSlimeCount, MaxYellowGoblinsCount, MaxGhostNormalCount;

	[field:NonSerialized]
	public Vector2[] SpawnPoints;

	public Vector2 GetRandomSpawnPos ()
	{
		return SpawnPoints [UnityEngine.Random.Range (0, SpawnPoints.Length)];
	}


	public delegate void CoinsChange (double amount);

	[field:NonSerialized]
	public event CoinsChange eCoinsChange;

	[SerializeField]
	float coins = 0;

	public float Coins {
		get {
			//			//Debug.Log("coins : " +coins);
			return coins;
		}
		set {
			coins = value;
			if (eCoinsChange != null) {
				eCoinsChange (value);
			}
			;
		}

	}

	[SerializeField]
	int starRatings;

	public int StarRatings {
		get {
			int rating = 0;
			float completePercentage = MissionWaves.CurrentHighscoreAmount / MissionWaves.RequiredAmount;
			//Debug.Log ("completePercentage : " + completePercentage);
			if (completePercentage >= 0.25f) {
				rating = 1;
			}
			if (completePercentage >= 0.60f) {
				rating = 2;
			}
			if (completePercentage >= 0.95f) {
				rating = 3;
			}
			return rating;
		}
 
	}

	[SerializeField]
	float kiLvlGained = 0;

	public float KiLvlGained {
		get {
			//			//Debug.Log("coins : " +coins);
			return kiLvlGained;
		}
		set {
			kiLvlGained = value;
			//			if(eCoinsChange!=null){
			//				eCoinsChange(value);
			//			}					;}
	
		}
	}
}
