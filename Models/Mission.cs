using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class Mission
{
	public delegate void MissionUpdate (Mission mission);

	[field:NonSerialized]
	public event MissionUpdate eMissionUpdate;

	public delegate void MissionComplete (Mission mission);

	[field:NonSerialized]
	public event MissionComplete eMissionComplete;


	[SerializeField]
	float currentHighscorerequiredAmount;

	public float CurrentHighscoreAmount {
		get{ return currentHighscorerequiredAmount; }
		set{ currentHighscorerequiredAmount = value; }
	}

	public void OnMissionUpdate ()
	{
		if (eMissionUpdate != null) {
			eMissionUpdate (this);
		}
	}

	public void IncreaseRequiredAmountForNextStage ()
	{
		RequiredAmount += RequiredAmount / 4;

	}

	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set{ id = value; }
	}

	[SerializeField]
	float bonusKiLevel;

	public float BonusKiLevel {
		get{ return bonusKiLevel; }
		set{ bonusKiLevel = value; }
	}

	[SerializeField]
	float bonusCoins;

	public float BonusCoins {
		get{ return bonusCoins; }
		set{ bonusCoins = value; }
	}
	//	[SerializeField]
	//	int waveNumber;
	//
	//	public int WaveNumber {
	//		get{ return waveNumber; }
	//		set{ waveNumber = value; }
	//	}
	////
	//
	//	public delegate void WaveNext(int waveNumber);
	//	[field:NonSerialized]
	//	public  event WaveNext eWaveNumberChange;
	//
	////	public void Complete(){
	////		if(eMissionComplete!=null){
	////			eMissionComplete();
	////		}
	////	}
	////
	//	public void OnWaveChange(int waveNumber){
	//		if (eWaveNumberChange != null) {
	//			eWaveNumberChange (waveNumber);
	//		}
	//	}

	[SerializeField]
	int currentAmount;

	public int CurrentAmount {
		get{ return currentAmount; }
		set {
			currentAmount = value;

		}

	}

	public void OnMissionComplete (Mission mission)
	{
		if (eMissionUpdate != null) {

			eMissionUpdate (this);

//			if (IsCompleted ()) {
//				//Debug.Log ("Is Mission Completed? : " + IsCompleted ());
			if (eMissionComplete != null) {

				eMissionComplete (mission);
			}

//			}
		}

	}

	[SerializeField]
	float requiredAmount;

	public float RequiredAmount {
		get{ return requiredAmount; }
		set{ requiredAmount = value; }
	}
	//	[SerializeField]
	float finishedAmount;

	public float FinishedAmount {
		get{ return finishedAmount; }
		set{ finishedAmount = value; }
	}

	[SerializeField]
	string story;

	public string Story {
		get{ return story; }
		set{ story = value; }
	}

	//		public void OnMissionComplete(float currentAmount){
	//		if(currentAmount>=RequiredAmount)
	//			if(eMissionComplete!=null){
	//			eMissionComplete();
	//			}
	//		}

	public bool IsCompleted ()
	{
		if (CurrentAmount >= RequiredAmount) {
			return true;
		} else {
			return false;
		}
	}
}

