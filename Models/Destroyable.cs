﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System;
//
//
//[System.Serializable]
//public class Destroyable  {
//	public  delegate void KiChange(float kiLevel, float maxKiLevel);
//	[field:NonSerialized]
//	public virtual event KiChange eKiChange;
//	public delegate void HpChange(float health, float maxHealth);
//	[field:NonSerialized]
//	public virtual event HpChange eHpChange;
//	public delegate void DamageTaken(float dmg);
//	[field:NonSerialized]
//	public virtual  event DamageTaken eDamageTaken;
//
//
//
//
//	[SerializeField]
//	float kiLevelMultipler;
//	public float KiLevelMultipler{
//		get{return kiLevelMultipler;}
//		set{kiLevelMultipler =value;}
//	}
//
//
//
//	[SerializeField]
//	float kiLevel;
//	public float KiLevel {
//		get{return kiLevel;}
//		set{
//
//			if(value<=MaxKiLevel && value>=0){
//				kiLevel=value;
//			}
//			if(eKiChange!=null){
//
//				eKiChange(KiLevel,MaxKiLevel);
//			}
//		}
//	}
//	float kiLevelPercentage;
//	public float KiLvlPercentage {
//		get
//		{
//			return KiLevel/MaxKiLevel;
//		}
//	}
//
//
//	[SerializeField]
//	float maxKiLevelBase;
//	public float MaxKiLevelBase{
//		get{return maxKiLevelBase;}
//		set{
//			maxKiLevelBase =value;
//			if(eKiChange!=null){
//				eKiChange(KiLevel,MaxKiLevel);
//			}
//		}
//	}
//
//	float maxKiLevel;
//	public float MaxKiLevel {
//		get{
//			return MaxKiLevelBase * KiLevelMultipler;}
//		set{
//
//			maxKiLevel = value;
//			if(eKiChange!=null){
//				eKiChange(KiLevel,MaxKiLevel);
//			}
//		}
//	}
//
//
//
//
//	bool isDead;
//	public bool IsDead{
//		get{return isDead;}
//		set{isDead =value;}
//	}
//
//	[SerializeField]
//	float hpMultipler=1;
//	public float HpMultipler{
//		get{return hpMultipler;}
//		set{hpMultipler =value;}
//	}
//	//	[SerializeField]
//	private float maxHealth;
//	public float MaxHealth
//	{
//		get
//		{
//			//			//Debug.Log(" get maxHp? : " + maxHealth);
//			return MaxKiLevelBase*10*HpMultipler;}
//
//		set{
//			//			//Debug.Log(" set maxHp? : " + maxHealth);
//			maxHealth=value;}
//	}
//
//
//	[SerializeField]
//	float health;
//	public float Health
//	{
//		get{return health;}
//		set{
//			if (value>MaxHealth){
//				value=MaxHealth;
//			}
//			health=value;
//			if(health<0){
//				health=0;
//			}
//			if(eHpChange!=null){
//				eHpChange(Health,MaxHealth);
//			}
//		}
//	}
//	public void TakeDamage(float val){
//		//		//Debug.Log("Damage taking  : " +val + "character type : " +CharacterType);
//		if(!IsDead){
//			val = UnityEngine.Random.Range(val-(0.1f*val),val+(0.1f*val));
////			val = val - (KiLevel * 0.1f);
//			if(val>=0){
//				Health-=val;
//			}else{
//				Health-=0;
//			}
//			if(eDamageTaken!=null){
//				eDamageTaken(val);
//			}
//
//		}
//
//	}
//}
//
