﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Wave
{
	
	public delegate void WaveCompleted ();

	[field:NonSerialized]
	public event WaveCompleted eWaveComplete;



	public void OnWaveComplete ()
	{
		if (EnemiesLeft <= 0) {
			if (eWaveComplete != null) {
				eWaveComplete ();
			}
		}

	}


	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set {

			//Debug.Log ("ID  : " + value);
			id = value;
		}
	}

	[SerializeField]
	float lvlMultipler;

	public float LvlMultipler {
		get{ return lvlMultipler; }
		set{ lvlMultipler = value; }
	}

	[SerializeField]
	float enemiesLeft;

	public float EnemiesLeft {
		get{ return enemiesLeft; }
		set{ enemiesLeft = value; }
	}

	public List<EnemyType> EnemyTypes;


	public void InitEnemy (CharacterModel character, StageModel currentStage)
	{
		//Debug.Log ("Exp now 0 :  " + character.Exp + " startExp 0 : " + character.StartExp + " currentStage.ExpMultipler  0: " + currentStage.ExpMultipler);
//		character.MaxKiLevelBase = character.StartMaxBaseKiLvl;
		character.MaxHealth = character.StartMaxHealth;
//		//Debug.Log ("currenstage WaveMultipler : " + currentStage.WaveMultipler + " character.MaxKiLevelBase ; " +character.MaxKiLevelBase);
		character.MaxKiLevelBase = character.StartMaxBaseKiLvl;
		character.MaxKiLevelBase = character.MaxKiLevelBase * ((float)currentStage.PowerLvlMultipler);
		character.KiLevel = character.MaxKiLevelBase;
//		character.MaxHealth =character.MaxHealth* (currentStage.WaveMultipler);
		character.Health = character.MaxHealth;
		character.Exp = character.StartExp;
		//Debug.Log ("Exp now :  " + character.Exp + " startExp : " + character.StartExp + " currentStage.ExpMultipler : " + currentStage.ExpMultipler);
		character.Exp = character.Exp * currentStage.ExpMultipler;
		if (DataManager.instance.oPlayer.SelectedShard.ExpBonus != 0) {
			character.Exp = character.Exp + (character.Exp * DataManager.instance.oPlayer.SelectedShard.ExpBonus / 100);
		}
		character.IsDead = false;
	}



}

