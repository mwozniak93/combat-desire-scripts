﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[System.Serializable]
public class CharacterModel
{
	//	public delegate void KiChange(float kiLevel, float maxKiLevel);
	//	[field:NonSerialized]
	//	public  event KiChange eKiChange;
	//	public delegate void HpChange(float health, float maxHealth);
	//	[field:NonSerialized]
	//	public  event HpChange eHpChange;
	//	public delegate void DamageTaken(float dmg);
	//	[field:NonSerialized]
	//	public  event DamageTaken eDamageTaken;
	//	AudioManager.Instance.Play (AudioType.Hit);
	public  delegate void Punch ();

	[field:NonSerialized]
	public event Punch ePunch;

	public  delegate void KiChange (float kiLevel,float maxKiLevel);

	[field:NonSerialized]
	public event KiChange eKiChange;

	public delegate void HpChange (float health,float maxHealth);

	[field:NonSerialized]
	public event HpChange eHpChange;

	public delegate void DamageTaken (float dmg);

	[field:NonSerialized]
	public event DamageTaken eDamageTaken;

	public delegate void DialogShow (string msg);

	[field:NonSerialized]
	public event DialogShow eDialogShow;

	public delegate void TransformationCancel ();

	[field:NonSerialized]
	public event TransformationCancel eTransformationCancel;

	public delegate void eDirectionChange (float x,float y);

	[field:NonSerialized]
	public event eDirectionChange DirectionChange;

	public delegate void PowerupChange ();

	[field:NonSerialized]
	public event PowerupChange ePowerupChange;

	public delegate void AuraChange (bool val);

	[field:NonSerialized]
	public event AuraChange eAuraChange;

	public delegate void EnergyGatherOff ();

	[field:NonSerialized]
	public event EnergyGatherOff eEnergyGatherOff;


	public delegate void LongWaveOff ();

	[field:NonSerialized]
	public event LongWaveOff eLongWaveOff;

	public delegate void CancelTransformation ();

	[field:NonSerialized]
	public event CancelTransformation eCancelTransformation;

	public delegate void CancelSuperPunch ();

	[field:NonSerialized]
	public event CancelSuperPunch eCancelSuperPunch;

	public delegate void Transformation ();

	[field:NonSerialized]
	public event Transformation eTransformation;

	public delegate void Die ();

	[field:NonSerialized]
	public event Die eDie;

	public delegate void ExpAdd (float expVal);

	[field:NonSerialized]
	public event ExpAdd eExpAdd;

	public delegate void TkOff ();

	[field:NonSerialized]
	public event TkOff eTkOff;

	public delegate void TkObjectMet (bool isOn);

	[field:NonSerialized]
	public event TkObjectMet eTkObjectMet;

	public delegate void HpAdd (float newHp);

	[field:NonSerialized]
	public event HpAdd eHpAdd;

	public void OnPunch ()
	{
		if (ePunch != null) {
			ePunch ();
		}
	
	}

	//	[SerializeField]
	//	float coins;
	//	public float PushForce{
	//		get{return pushForce;}
	//		set{pushForce =value;}
	//	}

	public bool IsPassiveSearchedSkillPresent (SkillPassiveType skillType)
	{
		foreach (var item in PlayerSkillsList) {
			if (item.SkillPassiveType == skillType && item.SkillUsageType == SkillUsageType.Passive) {
				return true;
			}
		}
		return false;
	
	}
	//	val-(0.1f*val),val+(0.1f*val));
	public string Damage {
		get {
			float minVal = (KiLevel - (0.1f * KiLevel));
			float maxVal = (KiLevel + (0.1f * KiLevel));
			string minValStr = minVal.ConvertNumber ();
			string maxValStr = maxVal.ConvertNumber ();
			return  minValStr + " - " + maxValStr;
		
		}

	}

	public string MaxDamage {
		get {
			float minVal = (MaxKiLevel - (0.1f * KiLevel));
			float maxVal = (MaxKiLevel + (0.1f * KiLevel));
			string minValStr = minVal.ConvertNumber ();
			string maxValStr = maxVal.ConvertNumber ();
			return  minValStr + " - " + maxValStr;

		}

	}

	public List<ItemModel> Items;

	[SerializeField]
	VectorXY startPos;

	public VectorXY StartPos {
		get{ return startPos; }
		set{ startPos = value; }
	}

	[SerializeField]
	float pushForce;

	public float PushForce {
		get{ return pushForce; }
		set{ pushForce = value; }
	}
	//	[SerializeField]
	float defendBtnPressTime;

	public float DefendBtnPressTime {
		get{ return defendBtnPressTime; }
		set{ defendBtnPressTime = value; }
	}

	[SerializeField]
	float speedX;

	public float SpeedX {
		get{ return speedX; }
		set{ speedX = value; }
	}



	[SerializeField]
	float speedY;

	public float SpeedY {
		get{ return speedY; }
		set{ speedY = value; }
	}


	public List<SkillModel> PlayerSkillsList;

	[SerializeField]
	CharacterType oponent;

	public CharacterType Oponent {
		get{ return oponent; }
		set{ oponent = value; }
	}

	[SerializeField]
	States states;

	public States States {
		get{ return states; }
		set{ states = value; }
	}

	[SerializeField]
	States lastStates;

	public States LastState {
		get{ return lastStates; }
		set{ lastStates = value; }
	}

	[SerializeField]
	TransformState transformState;

	public TransformState TransformState {
		get{ return transformState; }
		set{ transformState = value; }
	}


	[SerializeField]
	float speed;

	public float Speed {
		get{ return speed; }
		set{ speed = value; }
	}



	[SerializeField]
	int attackAnimationsCount;

	public int AttackAnimationsCount {
		get{ return attackAnimationsCount; }
		set{ attackAnimationsCount = value; }
	}


	[SerializeField]
	float punchBtnPressTime;

	public float PunchBtnPressTime {
		get{ return punchBtnPressTime; }
		set{ punchBtnPressTime = value; }
	}

	[SerializeField]
	VectorXY direction;

	public VectorXY Direction {
		get{ return direction; }
		set{ direction = value; }
	}

	[SerializeField]
	float pushRate;

	public float PushRate {
		get{ return pushRate; }
		set{ pushRate = value; }
	}


	[SerializeField]
	bool shouldReduceKi;

	public bool ShouldReduceKi {
		get{ return shouldReduceKi; }
		set{ shouldReduceKi = value; }
	}

	[SerializeField]
	SkillModel currentSkill;

	public SkillModel oCurrentSkill {
		get{ return currentSkill; }
		set{ currentSkill = value; }
	}

	//	public float DirX{get;set;}
	//	public float DirY {get;set;}


	//	[SerializeField]

	//	[SerializeField]


	//	[SerializeField]
	bool isGuarding;

	public bool IsGuarding {
		get{ return isGuarding; }
		set{ isGuarding = value; }
	}

	[SerializeField]
	CharacterType characterType;

	public CharacterType CharacterType {
		get{ return characterType; }
		set{ characterType = value; }
	}

	[SerializeField]
	VectorXY targetDirection;

	public VectorXY TargetDirection {
		get{ return targetDirection; }
		set{ targetDirection = value; }
	}

	[SerializeField]
	float distanceToPlayer;

	public float DistanceToPlayer {
		get{ return distanceToPlayer; }
		set{ distanceToPlayer = value; }
	}

	[SerializeField]
	float startMaxHealth;

	public float StartMaxHealth {
		get{ return startMaxHealth; }
		set{ startMaxHealth = value; }
	}

	[SerializeField]
	float level;

	public float Level {
		get{ return level; }
		set {
			level = value;

			NextlevelExp = 100 * (Mathf.Pow (Level, LevelFactor));
//			Expierence=0;
			LevelFactor += 0.3f;
		}
	}

	[SerializeField]
	float levelFactor;

	public float LevelFactor {
		get{ return levelFactor; }
		set{ levelFactor = value; }
	}

	[SerializeField]
	float nextlevelExp;

	public float NextlevelExp {
		get{ return nextlevelExp; }
		set{ nextlevelExp = value; }
	}


	public void AddKiPoints ()
	{
//		//Debug.Log("MaxKiLevel : " +MaxKiLevel);
//		if(KiLevel<=MaxKiLevel){
		KiLevel += MaxKiLevel / 25;
//		}
	
	}

	[SerializeField]
	float exp;

	public float Exp {
		get{ return exp; }
		set{ exp = value; }
	}

	public float StartExp;

	[SerializeField]
	Mission mission;

	public Mission Mission {
		get{ return mission; }
		set{ mission = value; }
	}

	//	public void PowerupNotify(){
	//		if(ePowerupChange!=null){
	//		ePowerupChange();
	//		}
	//	}


	public void OnDamageEnd ()
	{
		if (ePowerupChange != null) {
			ePowerupChange ();
		}
		if (eEnergyGatherOff != null) {
			eEnergyGatherOff ();
		}
		if (eCancelTransformation != null) {
			eCancelTransformation ();
		}
		if (eTkOff != null) {
			eTkOff ();
		}
		if (eLongWaveOff != null) {
			eLongWaveOff ();
		}
	}



	public void OnDead (CharacterModel oEnemy)
	{
		//Debug.Log("Taking Damage from : " + oEnemy.StartMaxBaseKiLvl);
		if (Health <= 0 && !IsDead) {
			if (eDie != null) {
				//Debug.Log("Omdead : hp : " + Health  + " maxhp : : " + MaxHealth);
				eDie ();
			}
			States = States.Idle;
//			//Debug.Log("OnDEAD !!!!!!!!!!!!");
			IsDead = true;
//			if(oEnemy.eTkObjectMet!=null){
//			oEnemy.eTkObjectMet(false);
//							}
			oEnemy.AddExp (Exp);

		
		}
	}

	public void SetPowerUpAura (bool val)
	{
		if (eAuraChange != null) {
			eAuraChange (val);
		}
	}

	public void OnTkObjectMet (bool isOn)
	{
		if (eTkObjectMet != null) {
			eTkObjectMet (isOn);
//			//Debug.Log("MEET! "  );
		}
	}

	public void OnDamageStart ()
	{
		if (DirectionChange != null) {
			
//			//Debug.Log("UpdateDirection  X : " + Direction.X + " Y : " + Direction.Y);
			DirectionChange (Direction.X, Direction.Y);
		}
	}

	public void ReduceKiPoints (float val)
	{
//		if(KiLevel>=0){
		//Debug.Log("ShouldReduceKi; " + ShouldReduceKi);
		if (ShouldReduceKi) {
//			//Debug.Log(" beofreKiLevel : " +KiLevel);
//		KiLevel-=MaxKiLevel*(val/100);
			float value = (val / 100) * (float)MaxKiLevelBase / Level;
			KiLevel -= (float)value;
//			//Debug.Log("KiLevel  after: " +KiLevel);
		}
//		}
//		base.OnKiChange();
	
//		//Debug.Log("KI LVL : " +KiLevel);
	
		
	}

	public void InitEnemyCharacter ()
	{
		this.StartMaxHealth = this.MaxHealth;
		this.StartMaxBaseKiLvl = this.MaxKiLevelBase;
		this.StartExp = this.Exp;
//		this.SetActive (false);
	
	}
	//	public void OnMissionComplete(float currentAmount){
	//		if(currentAmount>=Miss)
	//		if(eMissionComplete!=null){
	//		eMissionComplete();
	//		}
	//	}
	//
	public void OnDialogShow (string msg)
	{
		if (eDialogShow != null) {
			eDialogShow (msg);
		}
	}

	public bool IsEnoughKi (float kiPercentRequired)
	{
		bool result = false;
//		//Debug.Log("kiPercentRequired :  " + kiPercentRequired + "KiLvlPercentage ; " + KiLvlPercentage); 
		if (KiLvlPercentage >= kiPercentRequired) {
			result = true;
		} else {
			if (eDialogShow != null) {
				eDialogShow ("Not Enough Ki!");
			}
			result = false;
		}
//		//Debug.Log("RESULT : " +result);
		return result;
	}
	//	public void ReduceKiPointsInPercent(float val){
	//		if(ShouldReduceKi){
	//			KiLevel-=MaxKiLevel*(val/100);
	//		}
	//	}
	//
	public void ChargeKi (float val)
	{
		KiLevel += val;
	}


	public SkillModel ChangeCurrentSkill (int Id)
	{
		foreach (var item in PlayerSkillsList) {
			if (item.Id == Id) {
				return item;
			}
		}
		return null;
	}

	public float StartSpeed;
	//	public void Restart(){
	//
	//	}
	//
	public void InitCharacter ()
	{
//		this.KiLevelBase = this.MaxKiLevel;
		//Debug.Log("KiMaxKiLevelBasee : "+ MaxKiLevelBase);
		//Debug.Log("Ki level before : "+ MaxKiLevel);
		this.MaxKiLevel = this.MaxKiLevelBase;
		this.States = States.Idle;
		this.Health = this.MaxHealth;
		this.KiLevel = this.MaxKiLevel / 2;
		this.StartMaxBaseKiLvl = this.MaxKiLevelBase;
		this.StartMaxHealth = this.MaxHealth;
		this.StartExp = this.Exp;
		this.StartSpeed = this.Speed;
		//Debug.Log ("what is start exp ?: " + StartExp); 
		//Debug.Log (" what is EXP : " + Exp);
		this.IsDead = false;
		foreach (var item in this.PlayerSkillsList) {
			item.LastCooldownVal = item.Cooldown;
		}
		if (this.PlayerSkillsList.Count > 0) {
			this.oCurrentSkill = this.PlayerSkillsList [0];
		}

		//Debug.Log("Ki level After: "+ MaxHealth);
	}

	public void OnTransform ()
	{
		if (eTransformation != null) {
			eTransformation ();
		}
	}

	public void AddHp (float healthPoints)
	{
		Health += (float)healthPoints;
		if (eHpAdd != null) {
			eHpAdd (healthPoints);
		}
	}

	public void AddExp (float expPoints)
	{
		//expPoints = Mathf.Round (expPoints);
//		//Debug.Log("Before exp add : " +MaxKiLevelBase +  " name : " + CharacterType + " adding Exp to kilvl " + expPoints + " NExtLvl +  " + NextlevelExp );
		expPoints = (expPoints * DataManager.instance.oPlayer.CurrentStage.ExpMultipler);
		MaxKiLevelBase += (float)expPoints;
		MaxKiLevel = MaxKiLevelBase;
		DataManager.instance.oPlayer.CurrentStage.KiLvlGained += expPoints;
		if (MaxKiLevelBase >= NextlevelExp) {
			Level += 0.2f;
		}
		if (eExpAdd != null) {
			eExpAdd (expPoints);
		}
	}




	[SerializeField]
	float kiLevelMultipler;

	public float KiLevelMultipler {
		get{ return kiLevelMultipler; }
		set{ kiLevelMultipler = value; }
	}



	[SerializeField]
	float kiLevel;

	public float KiLevel {
		get{ return kiLevel; }
		set {
//			//Debug.Log ("Settinf... " + CharacterType);
			if (value <= MaxKiLevel && value >= 0) {
				kiLevel = value;
			}
			if (value >= MaxKiLevel) {
				kiLevel = MaxKiLevel;
			}
			if (TransformState == TransformState.Transformed) {
				if (value * 100 / MaxKiLevelBase < 20) {
					if (eTransformationCancel != null) {
					
						eTransformationCancel ();
					}
				}
			}

			if (eKiChange != null) {

				eKiChange (KiLevel, MaxKiLevel);
			}
		}
	}

	float kiLevelPercentage;

	public float KiLvlPercentage {
		get {
			return KiLevel * 100 / MaxKiLevelBase;
		}
	}


	[SerializeField]
	float maxKiLevelBase;

	public float MaxKiLevelBase {
		get{ return maxKiLevelBase; }
		set {
			maxKiLevelBase = value;
			if (eKiChange != null) {
				eKiChange (KiLevel, MaxKiLevel);
			}
		}
	}

	float maxKiLevel;

	public float MaxKiLevel {
		get {
			return MaxKiLevelBase * (float)KiLevelMultipler;
		}
		set {

			maxKiLevel = value;
			if (eKiChange != null) {
				eKiChange (KiLevel, MaxKiLevel);
			}
		}
	}



	[SerializeField]
	bool isDead;

	public bool IsDead {
		get{ return isDead; }
		set{ isDead = value; }
	}

	[SerializeField]
	float hpMultipler = 1;

	public float HpMultipler {
		get{ return hpMultipler; }
		set{ hpMultipler = value; }
	}
	//	[SerializeField]
	private float maxHealth;

	public float MaxHealth {
		get {
			//			//Debug.Log(" get maxHp? : " + maxHealth);
			return MaxKiLevelBase * 10 * (float)HpMultipler;
		}

		set {
			//			//Debug.Log(" set maxHp? : " + maxHealth);
			maxHealth = value;
		}
	}


	[SerializeField]
	float health;

	public float Health {
		get{ return health; }
		set {
			if (value > MaxHealth) {
				value = MaxHealth;
			}
			health = value;
			if (health < 0) {
				health = 0;
			}
			if (eHpChange != null) {
				eHpChange (Health, MaxHealth);
			}
		}
	}

	public void TakeDamage (float val)
	{
//		//Debug.Log("Damage taking  : " +val + "character type : " +CharacterType + "Is dead : " + IsDead);
		if (!IsDead) {
//			//Debug.Log("Is dead : " + IsDead);
			val = (float)UnityEngine.Random.Range (val - (0.1f * val), val + (0.1f * val));
			//			val = val - (KiLevel * 0.1f);
			if (val >= 0) {
				Health -= (float)val;
			} else {
				Health -= 0;
			}
			if (eDamageTaken != null) {
				eDamageTaken (val);
			}

		}

	}

	public void RestoreHealth (PlayerModel oPlayer)
	{
		if (oPlayer.Coins > 100) {
			AddHp (MaxHealth / 4);
			oPlayer.Coins -= 100;
		}
	}

	public void RestoreKi (PlayerModel oPlayer)
	{
		if (oPlayer.Coins > 100) {
			KiLevel += (MaxKiLevelBase / 4);
			oPlayer.Coins -= 100;
			//Trzeba dodać w HUD ilosc aktualnych wsyztskich coinsow
		}
	}

	public float StartMaxBaseKiLvl;
	//	public CharacterModel(){
	//		//Debug.Log("MAX KI : " + maxKiLevel );
	//		PlayerSkillsList = new List<SkillModel>();
	//		Direction= new VectorXY();
	//		TargetDirection = new VectorXY();
	//		MaxKiLevel = maxKiLevel;
	//
	//	}
}
