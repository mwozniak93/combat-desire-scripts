﻿using UnityEngine;
using System.Collections;
using System;


[System.Serializable]
public class SkillModel
{
	[SerializeField]
	SkillPassiveType skillPassiveType;

	public SkillPassiveType SkillPassiveType {
		get{ return skillPassiveType; }
		set{ skillPassiveType = value; }
	}

	public string Description;

	[SerializeField]
	SkillUsageType skillUsageType;

	public SkillUsageType SkillUsageType {
		get{ return skillUsageType; }
		set{ skillUsageType = value; }
	}

	[SerializeField]
	float cooldown;

	public float Cooldown {
		get{ return cooldown; }
		set{ cooldown = value; }
	}

	[SerializeField]
	bool isContinousKiReduce;

	public bool IsContinousKiReduce {
		get{ return isContinousKiReduce; }
		set{ isContinousKiReduce = value; }
	}

	[SerializeField]
	float price;

	public float Price {
		get{ return price; }
		set{ price = value; }
	}

	[SerializeField]
	float kiLvlRequired;

	public float KiLvlRequired {
		get{ return kiLvlRequired; }
		set{ kiLvlRequired = value; }
	}

	[SerializeField]
	bool isBought;

	public bool IsBought {
		get{ return isBought; }
		set{ isBought = value; }
	}

	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set{ id = value; }
	}

	[SerializeField]
	float damageMultipler;

	public float DamageMultipler {
		get{ return damageMultipler; }
		set{ damageMultipler = value; }
	}

	[SerializeField]
	float kiNeededInPercentage;

	public float KiNeededInPercentage {
		get{ return kiNeededInPercentage; }
		set{ kiNeededInPercentage = value; }
	}

	[SerializeField]
	string title;

	public string Title {
		get{ return title; }
		set{ title = value; }
	}

	[SerializeField]
	SkillType skillType;

	public SkillType oSkillType {
		get{ return skillType; }
		set{ skillType = value; }
	}

	[SerializeField]
	float level;

	public float Level {
		get{ return level; }
		set{ level = value; }
	}

	public SkillModel ()
	{
		this.Level = 1;
	}

	public float[] BulletDirections;

	//	[SerializeField]
	//	CustomColor auraColor;
	//	public CustomColor AuraColor{
	//		get{return auraColor;}
	//		set{auraColor =value;}
	//	}
	//	[SerializeField]
	//	string bulletPath;
	//	public string BulletPath{
	//		get{return bulletPath;}
	//		set{bulletPath =value;}
	//	}
	//	[SerializeField]
	//	string explosionPath;
	//	public string ExplosionPath{
	//		get{return explosionPath;}
	//		set{explosionPath =value;}
	//	}
	[SerializeField]
	float acceleration;

	public float Acceleration {
		get{ return acceleration; }
		set{ acceleration = value; }
	}

	[SerializeField]
	bool isCooldown;

	public bool IsCooldown {
		get{ return isCooldown; }
		set{ isCooldown = value; }
	}

	[SerializeField]
	float shootRateForMultiBullets;

	public float ShootRateForMultiBullets {
		get{ return shootRateForMultiBullets; }
		set{ shootRateForMultiBullets = value; }
	}

	[SerializeField]
	float upgradePrice;

	public float UpgradePrice {
		get{ return upgradePrice; }
		set{ upgradePrice = value; }
	}

	[SerializeField]
	AttackAnim attakAnim;

	public AttackAnim AttakAnim {
		get{ return attakAnim; }
		set{ attakAnim = value; }
	}

	[SerializeField]
	BulletType bulletType;

	public BulletType BulletType {
		get{ return bulletType; }
		set{ bulletType = value; }
	}

	[SerializeField]
	EffectType effectType;

	public EffectType EffectType {
		get{ return effectType; }
		set{ effectType = value; }
	}

	[SerializeField]
	ExplosionType explosionType;

	public ExplosionType ExplosionType {
		get{ return explosionType; }
		set{ explosionType = value; }
	}

	public delegate void SkillChange (SkillModel oSkill);

	[field:NonSerialized]
	public event SkillChange eSkillChange;

	public delegate void CooldownChange (float cooldown,float maxCooldown);

	[field:NonSerialized]
	public event CooldownChange eCooldownChange;

	float lastCooldownVal;

	public float LastCooldownVal {
		get {
			return lastCooldownVal;
		}
		set {
			lastCooldownVal = value;

			if (eCooldownChange != null) {
//				////Debug.Log(" lastCooldownVal : " +lastCooldownVal);
				eCooldownChange (value, Cooldown);
			}
		}
	}

	public void OnSkillChange (SkillModel oSkill)
	{
		if (eSkillChange != null) {
			////Debug.Log("ONSKILLCHANGE");
			eSkillChange (oSkill);
		}
	}


	public bool Buy (PlayerModel oPlayer)
	{
		////Debug.Log("oPlayer.Coins ; "  +oPlayer.Coins + " oPlayer.Character.MaxKiLevelBase : " + oPlayer.Character.MaxKiLevelBase + " Price ; " + Price + " Required Ki LV L : " + KiLvlRequired);
		if (!IsBought && oPlayer.Coins >= Price && oPlayer.Character.MaxKiLevelBase >= KiLvlRequired) {
			oPlayer.Coins -= Price;
			IsBought = true;
			Price = UpgradePrice;
			oPlayer.Character.PlayerSkillsList.Add (this);
			SaveData.Save ();
		
		}

		return IsBought;
	}

	public bool Upgrade (PlayerModel oPlayer)
	{
		//			////Debug.Log("oPlayer.Coins ; "  +oPlayer.Coins + " oPlayer.Character.MaxKiLevelBase : " + oPlayer.Character.MaxKiLevelBase + " Price ; " + Price + " Required Ki LV L : " + KiLvlRequired);oSkillType != SkillType.Transformation &&
		if (IsBought && oPlayer.Coins >= Price && oPlayer.Character.MaxKiLevelBase >= KiLvlRequired && Level <= 3 && SkillUsageType == SkillUsageType.Active) {
			oPlayer.Coins -= Price;
			Price += Price / 4;
			Level += 1;
			KiNeededInPercentage -= KiNeededInPercentage * 0.2f;
			KiLvlRequired += KiLvlRequired / 10;
			if (Cooldown >= 1) {
				Cooldown -= (Cooldown / 5);
			}
			DamageMultipler += 0.3f;
			SaveData.Save ();
			return true;
//				IsBought = true;
//				oPlayer.Character.PlayerSkillsList.Add(this);
		

		}
		return false;
	}
}
