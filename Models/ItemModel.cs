﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ItemModel
{
	
	[SerializeField]
	float throwPower;

	public float ThrowPower {
		get{ return throwPower; }
		set{ throwPower = value; }
	}
	//	//	[SerializeField]
	//
	////	CharacterModel owner;
	////	public CharacterModel Owner{
	////		get{return owner;}
	////		set{owner =value;}
	////	}
	////
	[SerializeField]
	VectorXY throwDirection;

	public VectorXY ThrowDirection {
		get{ return throwDirection; }
		set{ throwDirection = value; }
	}

	[SerializeField]
	float boostValue;

	public float BoostValue {
		get{ return boostValue; }
		set{ boostValue = value; }
	}

	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set{ id = value; }
	}

	[SerializeField]
	ItemBoostType itemBoostType;

	public ItemBoostType ItemBoostType {
		get{ return itemBoostType; }
		set{ itemBoostType = value; }
	}

	[SerializeField]
	ItemType itemType;

	public ItemType ItemType {
		get{ return itemType; }
		set{ itemType = value; }
	}


}
