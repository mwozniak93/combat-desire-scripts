
using System;
using System.Collections.Generic;
using System.Collections;

//using System;
using UnityEngine;

[System.Serializable]
public class VectorXY
{

	[SerializeField]
	float x;

	public float X {
		get{ return x; }
		set{ x = value; }
	}

	[SerializeField]
	float y;

	public float Y {
		get{ return y; }
		set{ y = value; }
	}


}

