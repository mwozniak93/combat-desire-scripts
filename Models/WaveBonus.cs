﻿using System;
using UnityEngine;

[System.Serializable]
public class WaveBonus
{
	public int Id;
	public int WaveCountNeeded;
	public double CoinsBonus;
	public float PowerBonus;
	public bool IsUnlcoked;
	public bool IsCompleted;

}


