﻿using System;
using System.Collections.Generic;
using System.Collections;

//using System;
using UnityEngine;

[System.Serializable]
public class CustomColor
{

	[SerializeField]
	float r;

	public float R {
		get{ return r; }
		set{ r = value; }
	}

	[SerializeField]
	float g;

	public float G {
		get{ return g; }
		set{ g = value; }
	}

	[SerializeField]
	float b;

	public float B {
		get{ return b; }
		set{ b = value; }
	}

}

