﻿using System;
using UnityEngine;
using System.Linq;
using System.Text.RegularExpressions;


[System.Serializable]
public class ShardModel
{
	public ItemType ShardType;
	[SerializeField]
	float expectedAmount;

	public float ExpectedAmount {
		get{ return expectedAmount; }
		set {
			expectedAmount = value;
		}
	}

	[SerializeField]
	float currentAmount;

	public float CurrentAmount {
		get{ return currentAmount; }
		set {
			currentAmount = value;
		}
	}


	 
	[SerializeField]
	string title;

	public string Title {

		get{ return Regex.Replace (ShardType.ToString (), "([a-z])([A-Z])", "$1 $2"); }
		set {
			title = value;
		}
	}


	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set {
			id = value;
		}
	}

	[SerializeField]
	MyColor color;

	public MyColor Color {
		get{ return color; }
		set {
			color = value;
		}
	}


	public float HealthRecoveryAmount;
	public float ExpBonus;
	public float SpeedBonus;
	public float MaxKiLevelBaseBonus;

	public bool IsEnoughAmount ()
	{
		if (CurrentAmount >= ExpectedAmount) {
			return true;
		} else {
			return false;
		}

	}


}



[System.Serializable]
public class MyColor
{
	[SerializeField]
	public float r, g, b, a;
}

[System.Serializable]
public class ShardRandomizer
{
	public float ChanceToGetRandomizedStart, ChanceToGetRandomizedEnd;
	public ItemType ShardType;
}

