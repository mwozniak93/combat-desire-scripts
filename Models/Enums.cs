﻿
public enum CharacterType
{
Enemy,
	Player
}
public enum States
{
Walking,
	Idle,
	Attacking,
	Hurting,
	Guarding,
	Charging,
Following,
	StandingNear,
	StandingSteel,
	MovingBack,
	Dead,
	SpeedAttack,
	SuperPunchPrepere,
	Vanishing,
	Transforming,
	TKFocus,
	TKShoot
}
public enum BulletType
{
EnergyShoot1 = 1,
	Knife = 2,
	EnergyBlast = 3,
	ElectricWave = 4,
	ExplosionWave1 = 5,
	SuperBlast = 6,
	PlainEnergy = 7,
	StrikeOfLight = 8,
	FinalCannon = 9,
	ExplosionFury = 10,
	EnergyShield = 11,
	SuperShine = 12,
	Bone = 13,
	ExplosionWaveRed = 14,
	NatureShield = 15,
	EnergyShoot2 = 16
}
public enum AttackAnim
{
EnergyAttack1,
EnergyAttack2,
	IsCharging
}
public enum SkillType
{
Transformation,
	OneShot,
	Wave,
	Aim,
	LongWave
}
public enum EffectType
{
Vanish,
	SuperPunch,
	EnergyGather,
PowerBeyondAura,
PowerBeyondExplosion,
	BoxTk,
	KiLevelGain
}
public enum ExplosionType
{
EnergyShoot,
EnergyBlast,
SmokeSmall,
SmokeBig,
	Hit
}
public enum TransformState
{
Normal,
	Transformed
}
public enum ItemBoostType
{
Health,
	Coins,
	KiLevel,
	Mission
}
public enum ItemType
{
Chicken,
	Coin,
	Hamburger,
	Pizza,
	Mission,
	ShardOfSky,
	ShardOfTiger,
	ShardOfRose,
	ShardOfLife,
ShardOfFire,
	ShardOfBlueberry,
	ShardOfLight,
	ShardOfDarkness,
	None

}
public enum EnemyType
{
	GoblinBlue,
	GoblinGreen,
	Skeleton,
	SkeletonWizard,
	SkeletonWarrior,
SlimeBlue,
	GoblinRed,
	SlimeYellow,
	SlimeYellowBoss,
	GoblinYellow,
	GoblinBlueBoss,
GhostNormal,
SkeletonWarriorRedBoss
}
public enum UiState
{
	None,
Pause,
	Game
}
public enum SkillUsageType
{
	Active,
Passive
}
public enum SkillPassiveType
{
	None,
Teleportation,
SuperPunch,
	SensePower
}
public enum TooltipType
{
	MainMenu,
	Hud,
	StagesMenu
}