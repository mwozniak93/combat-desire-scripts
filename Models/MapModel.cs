﻿using System;
using UnityEngine;
using System.Collections.Generic;

[Serializable]
public class MapModel
{

	[SerializeField]
	string mapName;

	public string MapName {
		get{ return mapName; }
		set{ mapName = value; }
	}

	[SerializeField]
	int id;

	public int Id {
		get{ return id; }
		set{ id = value; }
	}

	[SerializeField]
	public List<StageModel> Stages;

	[SerializeField]
	bool isLocked;

	public bool IsLocked {
		get{ return isLocked; }
		set{ isLocked = value; }
	}

	public void Unlock ()
	{
		IsLocked = false;
	}
}

