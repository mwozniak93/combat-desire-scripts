﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;


[System.Serializable]
public class PlayerModel
{


	public delegate void SensePower (bool val);

	[field:NonSerialized]
	public event SensePower eSensePower;


	public void OnSensePower (bool val)
	{
		if (eSensePower != null) {
			eSensePower (val);
		}

	}

	[SerializeField]
	int id;

	public int Id { get { return id; } set { id = value; } }

	[SerializeField]
	CharacterModel character;

	public CharacterModel Character {
		get {
			return character;
		}
		set {
			character = value;		
		}
	}

	[SerializeField]
	MapModel currentMap;

	public MapModel CurrentMap {
		get{ return currentMap; }
		set{ currentMap = value; }
	}

	[SerializeField]
	StageModel currentStage;

	public StageModel CurrentStage {
		get{ return currentStage; }
		set{ currentStage = value; }
	}

	[SerializeField]
	ShardModel selectedShard;

	public ShardModel SelectedShard {
		get{ return selectedShard; }
		set{ selectedShard = value; }
	}

	public List <MapModel> Maps;

	public delegate void CoinsChange (double amount);

	[field:NonSerialized]
	public event CoinsChange eCoinsChange;

	[SerializeField]
	double coins;

	public double Coins {
		get {
//			////Debug.Log("coins : " +coins);
			return System.Math.Round (coins, 0);
		}
		set {
			coins = value;
			if (eCoinsChange != null) {
				eCoinsChange (value);
			}
			;
		}

	}

	public List<SkillModel> Skills;

	[SerializeField]
	VectorXY startPosOnMap;

	public VectorXY StartPosOnMap {
		get{ return startPosOnMap; }
		set{ startPosOnMap = value; }
	}

	public List<ShardModel> Shards;

	public void FinishStageLose (float oldPowerLvl, double coins)
	{
		Character.MaxKiLevelBase = (float)oldPowerLvl;
//		Coins+=coins;
		CurrentStage.MissionWaves.CurrentAmount = 0;
		SaveData.Save ();
	}

	public void FinishStageWin (float newPowerLvl, double coins)
	{
		Character.MaxKiLevelBase = (float)newPowerLvl;
		Character.MaxKiLevel = Character.StartMaxBaseKiLvl;
		Coins += coins;
		CurrentStage.UnlockWaveBonus ();
		if (Application.platform == RuntimePlatform.Android) {
			GoogleApi.Instance.PostScore ((long)newPowerLvl);
		}
//		Debug.Log ("CurrentStage.MissionWaves.CurrentAmount   : " + CurrentStage.MissionWaves.CurrentAmount);

//		Debug.Log ("CurrentStage.MissionWaves.CurrentHighscoreAmount   : " + CurrentStage.MissionWaves.CurrentHighscoreAmount);
//		CurrentStage.Finish();
		if (CurrentStage.MissionWaves.CurrentAmount > CurrentStage.MissionWaves.CurrentHighscoreAmount) {
			

			//Debug.Log ("SAVing  CurrentStage.Mission.CurrentHighscoreAmount : " + CurrentStage.MissionWaves.CurrentHighscoreAmount);
			//Debug.Log ("SAVing  CurrentStage.Mission.CurrentAmount: " + CurrentStage.MissionWaves.CurrentAmount);
			CurrentStage.MissionWaves.CurrentHighscoreAmount = CurrentStage.MissionWaves.CurrentAmount;
		}
//		Debug.Break ();
		if (CurrentStage.MissionWaves.CurrentAmount >= CurrentStage.MissionWaves.RequiredAmount) {
			
			int indexOfCurrentStage = CurrentMap.Stages.IndexOf (CurrentStage);
			if (indexOfCurrentStage >= 0 && indexOfCurrentStage + 1 < CurrentMap.Stages.Count) {
				CurrentMap.Stages [indexOfCurrentStage + 1].Unlock ();



			}

//			if (indexOfCurrentStage >= 3) {
			int indexOfCurrentMap = Maps.IndexOf (CurrentMap);
			if (indexOfCurrentMap + 1 > 0 && indexOfCurrentMap + 1 <= Maps.Count) {
				Maps [indexOfCurrentMap + 1].Unlock ();
				if (indexOfCurrentStage <= Maps [indexOfCurrentMap + 1].Stages.Count) {
					
				
					Maps [indexOfCurrentMap + 1].Stages [indexOfCurrentStage].Unlock ();
				}

//				}
			}
		}
//		CurrentStage.Mission.CurrentHighscoreAmount=CurrentStage.Mission.CurrentAmount;
//		CurrentStage.Mission.CurrentAmount=0;
//		Coins+=coins;
		SaveData.Save ();

	}

	public delegate void ShardCollected (ItemType bonusShard);

	[field:NonSerialized]
	public event ShardCollected eShardCollected;

	public void OnShardCollected (ItemType shardType)
	{

		if (eShardCollected != null) {
			eShardCollected (shardType);
		}
	}

	public void CollectShard (ItemType shardType)
	{
		foreach (var item in Shards) {
			if (item.ShardType == shardType) {
				//Debug.Log ("Collecting shard... " + item.ShardType);
				item.CurrentAmount += 1;
				OnShardCollected (shardType);
			}
		}
	}

	public bool IsAnySkillToBuy ()
	{
		foreach (var item in Skills) {
			if (item.Price <= Coins && !item.IsBought && Character.MaxKiLevelBase >= item.KiLvlRequired) {
				return true;
			}
		}
		return false;
	}

	//
	//	public  void Save(){
	//		ListPlayersSavedData = new List<PlayerModel>();
	//		PlayersSavedData.Add(DataManager.instance.oPlayer);
	//		BinaryFormatter bf = new BinaryFormatter();
	//		FileStream file = File.Create (Application.persistentDataPath + "/PlayerSave.gd");
	//		bf.Serialize(file, PlayersSavedData);
	//		file.Close();
	//	}
	//
	//	public  void Load() {
	//		if(File.Exists(Application.persistentDataPath + "/PlayerSave.gd")) {
	//			BinaryFormatter bf = new BinaryFormatter();
	//			FileStream file = File.Open(Application.persistentDataPath + "/PlayerSave.gd", FileMode.Open);
	//			PlayersSavedData = (List<PlayerModel>)bf.Deserialize(file);
	//
	//			//			////Debug.Log("Load completed ? :"  +PlayersSavedData.Count);
	//			file.Close();
	//		}
	//	public PlayerModel (){
	//		character= new CharacterModel();
	//		Character =  new CharacterModel();
	//		Character = character;
	//		Stages = new List<StageModel>();
	//		Skills =new List<SkillModel>();
	//	}

	//	public

}
