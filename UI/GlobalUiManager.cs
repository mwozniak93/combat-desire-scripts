﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GlobalUiManager : MonoBehaviour
{

	public static GlobalUiManager instance;
	Animator ctGlobalUiAnimator;
	Image ctImage;

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (transform.parent.gameObject);
		}
		DontDestroyOnLoad (transform.parent.gameObject);
	}

	void Start ()
	{
		ctGlobalUiAnimator = GetComponent<Animator> ();
		ctImage = GetComponent<Image> ();
	}

	public IEnumerator FadeIn ()
	{
//		transform.parent.gameObject.SetActive(true);
		ctImage.enabled = true;
		ctGlobalUiAnimator.SetBool ("IsFadeIn", true);
		yield return new WaitForSeconds (1.5f);
		ctImage.enabled = false;
//		transform.parent.gameObject.SetActive(false);
	}

	public IEnumerator FadeOut ()
	{
//		transform.parent.gameObject.SetActive(true);
		ctImage.enabled = true;
		ctGlobalUiAnimator.SetBool ("IsFadeIn", false);
		yield return new WaitForSeconds (0);
//		ctImage.enabled=false;
//		transform.parent.gameObject.SetActive(false);
	}

}
