﻿using UnityEngine;
using System.Collections;

public class MenuStageTrigger : MonoBehaviour
{
	public StageModel oStage;
	//	public static void LoadStage(StageModel stage) { }
	void OnTriggerEnter2D (Collider2D coll)
	{
//		//Debug.Log("Collider :"  +coll.name);
		if (coll.CompareTag ("Player") && !oStage.IsLocked) {
			MenuUi.instance.ConfirmStageLoad ("Are you sure you want to load Level?", oStage);
		}

	}

	void OnTriggerExit2D (Collider2D coll)
	{
		//		//Debug.Log("Collider :"  +coll.name);
		if (coll.CompareTag ("Player")) {
			MenuUi.instance.CLoseAskPanel ();
		}

	}
}
