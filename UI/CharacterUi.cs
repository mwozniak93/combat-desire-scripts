﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CharacterUi : MonoBehaviour
{
	Transform TrmFloatingTexts;
	List<Text> FloatingTexts;
	CharacterModel oCharacter;
	Text DialogBoxText;
	Text txtPowerLvl;

	void Awake ()
	{
		txtPowerLvl = transform.Find ("Canvas").Find ("PowerLevel").Find ("PowerLevelTxt").GetComponent<Text> ();
		txtPowerLvl.transform.parent.gameObject.SetActive (false);

		foreach (var item in transform.Find ("Canvas").GetComponentsInChildren<Deactivate>()) {
			item.time = 1f;
		}
	}
	// Use this for initialization
	void Start ()
	{
		
		Transform tmPowerLevelForDetect1 = transform.Find ("Canvas").Find ("PowerLevel").Find ("Image");
		tmPowerLevelForDetect1.name = "PowerLevelDetectImg1";
		Transform tmPowerLevelForDetect2 = transform.Find ("Canvas").Find ("PowerLevel").Find ("Image");
		tmPowerLevelForDetect2.name = "PowerLevelDetectImg2";
		if (!tmPowerLevelForDetect1.GetComponent<Shadow> ()) {
			tmPowerLevelForDetect1.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Character_Stats_Box");
			tmPowerLevelForDetect2.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("lightning");
		} else {
			tmPowerLevelForDetect2.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Character_Stats_Box");
			tmPowerLevelForDetect1.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("lightning");
		}
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		TrmFloatingTexts = transform.Find ("Canvas").Find ("FloatingTexts");
		DialogBoxText = transform.Find ("Canvas").Find ("DialogBox").Find ("DialogBoxText").GetComponent<Text> ();
		DialogBoxText.transform.parent.gameObject.SetActive (false);
		FloatingTexts = new List<Text> ();

		oCharacter.eDamageTaken += UpdateDamageTaken;
		oCharacter.eExpAdd += UpdateExp;
		oCharacter.eHpAdd += UpdateHealth;
		DataManager.instance.oPlayer.eSensePower += ShowPowerLvl;
		foreach (Text item in gameObject.GetComponentsInChildren<Text>()) {
			item.font = Resources.Load<Font> ("Comic_Book");
		}
		foreach (Transform item in TrmFloatingTexts) {
			FloatingTexts.Add (item.GetComponent<Text> ());
		}
	}


	void ShowPowerLvl (bool val)
	{
		txtPowerLvl.transform.parent.gameObject.SetActive (val);
		txtPowerLvl.text = "" + oCharacter.KiLevel.ConvertNumber (); 
	}

	public void UpdateCoins (double val)
	{
//		//Debug.Log("UpdateCoins : " +val);
		Text floatingText = GetFloatingText ();
		if (floatingText) {
			floatingText.color = new Color32 (0, 135, 62, 255);
			floatingText.text = "$$$$$";
			floatingText.gameObject.SetActive (true);
		}
	}

	void OnEnable ()
	{
		if (DialogBoxText) {
			DialogBoxText.transform.parent.gameObject.SetActive (false);
		}
	}

	void ShowDialogBox (string msg)
	{
		StartCoroutine (ShowDialogBoxCourutine (msg));

	}

	IEnumerator ShowDialogBoxCourutine (string msg)
	{
		if (!DialogBoxText.transform.parent.gameObject.activeInHierarchy) {
			DialogBoxText.text = msg;
			DialogBoxText.transform.parent.gameObject.SetActive (true);
			yield return new WaitForSeconds (2f);
			DialogBoxText.transform.parent.gameObject.SetActive (false);
		}
	}

	void UpdateHealth (float val)
	{
		Text floatingText = GetFloatingText ();
		if (floatingText) {
			floatingText.color = Color.red;
			floatingText.text = "+" + Mathf.Round (val);
			floatingText.gameObject.SetActive (true);
		}
	}

	void UpdateExp (float exp)
	{
		Text floatingText = GetFloatingText ();
		if (floatingText) {
			floatingText.color = new Color32 (233, 155, 0, 255);
			floatingText.text = "+" + exp.ConvertNumber ();
			floatingText.gameObject.SetActive (true);
			GameObject effect = EffectsPoolManager.instance.GetEffect (EffectType.KiLevelGain);
			if (effect) {
				effect.transform.position = transform.position;
				effect.SetActive (true);
			}
		}
	}

	void OnDestroy ()
	{
		oCharacter.eDamageTaken -= UpdateDamageTaken;
		oCharacter.eExpAdd -= UpdateExp;
		DataManager.instance.oPlayer.eSensePower -= ShowPowerLvl;

		oCharacter.eHpAdd -= UpdateHealth;
		DataManager.instance.oPlayer.eCoinsChange -= UpdateCoins;
	}

	void UpdateDamageTaken (float dmg)
	{
		Text floatingText = GetFloatingText ();
		if (floatingText) {
			floatingText.color = Color.red;
			floatingText.gameObject.SetActive (true);
			if (dmg >= 0) {
				dmg = Mathf.Round (dmg);
			} else {
				dmg = 0;
			}
			floatingText.text = "-" + dmg.ConvertNumber ();
		}
	}

	Text GetFloatingText ()
	{
		foreach (Text item in FloatingTexts) {
			if (!item.gameObject.activeInHierarchy) {
				return item;
			}
		}
		return null;
	}


}
