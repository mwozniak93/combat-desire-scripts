﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
public class MenuUi : MonoBehaviour {
	[SerializeField]
	Transform StagesParent, AskPanel, MenuPanel, StageSelectMenuPanel, SkillsMenuPanel, SkillsPanelContent, SkillsDescription, MapPanel, LoadingBox, ShardsMenu, WavesBonusMenu;
	[SerializeField]
	Image SkillImageImage, SelectedShardImg;
	[SerializeField]
	Text KiLevelRequiredTxt, DamageText, SkillNameText, CooldownText, CoinsText, CharacterKiLevelText, CurrentSkillLevelText, SkillPriceText, SkillEnergyNeededText, SkillDescriptionText;
	[SerializeField]
	Button btnBuy, btnSave, btnLoginGoogle, btnShowLeaderboard, btnUpgrade;
	[SerializeField] 
	GameObject GoBought;
	[SerializeField]
	GameObject goHighlightSkills,goNewBonusAchievemntStages;
	public static MenuUi instance;
//	public delegate void Del<T>(T item);
//	public static void Notify(int i) { }

//	Del<int> m1 = new Del<int>(Notify);
	public delegate  void  AskPanelMethod<T>(T param1);

	void Awake(){
		if(instance==null){
			instance=this;
		}else{Destroy(gameObject);
		}

		ShowLeaderboardButton (false);
	}
	void Start(){
		foreach (Text item in Component.FindObjectsOfType<Text>()) {
			if (item.name != "Title") {
				item.font = Resources.Load<Font> ("Comic_Book");
			}
			}
		////Debug.Log("Application.persistentDataPath + " +Application.persistentDataPath   );
//		GenerateStagesUi();
		GenerateSkillsPanelContent();
		AssignDifficulity ();
		GenerateCurrentMap ();
		GenerateCurrentStage ();
		DataManager.instance.oPlayer.Character.eKiChange+=UpdateCharacterKiLevelText;
		DataManager.instance.oPlayer.eCoinsChange+=UpdatePlayerCoinsText;
		DataManager.instance.oPlayer.Coins=DataManager.instance.oPlayer.Coins;
		DataManager.instance.oPlayer.Character.MaxKiLevel=DataManager.instance.oPlayer.Character.MaxKiLevel;

		btnLoginGoogle.onClick.RemoveAllListeners ();
		btnLoginGoogle.onClick.AddListener (() => {
			GoogleApi.Instance.Login();
		
		});

		btnShowLeaderboard.onClick.RemoveAllListeners ();
		btnShowLeaderboard.onClick.AddListener (() => {
			GoogleApi.Instance.ShowRanking();
		});
		GenerateCurrentSkillDescription (DataManager.instance.oPlayer.Character.oCurrentSkill);
		GenerateShardsMenu ();
		if (DataManager.instance.oPlayer.SelectedShard.IsEnoughAmount ()) {
			AssignSelectedShard (DataManager.instance.oPlayer.SelectedShard);
		} else {
			AssignSelectedShard ((from x in DataManager.instance.oPlayer.Shards  where x.Id== 0
			                      select x).FirstOrDefault ());

		}
		HighlightSkills ();
		GenerateWavesBonusPanel ();
//		btnSave.onClick.AddListener(()=>{
//			SaveNow();
//		});
	}
//	public void SaveNow(){
//		SaveData.Save();
//	}
//
	public void GenerateWavesBonusPanel(){
		foreach (Transform item in WavesBonusMenu.transform) {
			item.name = "WaveBonus";
		}
		foreach (WaveBonus item in DataManager.instance.oPlayer.CurrentStage.WavesBonuses) {
			Text coinsBonusTxt = WavesBonusMenu.Find ("WaveBonus").Find("CoinsBonus").GetComponent<Text>();
			Text powerBonusTxt = WavesBonusMenu.Find ("WaveBonus").Find("PowerBonus").GetComponent<Text>();
			Text titleTxt = WavesBonusMenu.Find ("WaveBonus").Find("Title").GetComponent<Text>();
			Button confirmBtn =  WavesBonusMenu.Find ("WaveBonus").Find ("ConfirmBtn").Find ("Confirm").GetComponent<Button>();
			GameObject confirmLockedImg = WavesBonusMenu.Find ("WaveBonus").Find ("ConfirmBtn").Find ("Locked").gameObject;
			GameObject confirmImg = WavesBonusMenu.Find ("WaveBonus").Find ("ConfirmBtn").Find ("Confirm").gameObject;
			GameObject completedImg = WavesBonusMenu.Find ("WaveBonus").Find ("Completed").gameObject;
			WavesBonusMenu.Find ("WaveBonus").name = "WaveBonus" + item.Id;
			confirmBtn.onClick.RemoveAllListeners ();
			titleTxt.text = item.WaveCountNeeded + " Waves";
			powerBonusTxt.text = item.PowerBonus.ToString ();
			coinsBonusTxt.text = item.CoinsBonus.ToString ();
			completedImg.SetActive (false);
			confirmLockedImg.SetActive (false);
			confirmImg.SetActive (false);
			goNewBonusAchievemntStages.SetActive (false);
			WaveBonus itemCopy = item;
			if (DataManager.instance.oPlayer.CurrentStage.IsAnyWaveBonusToGet()) {
				goNewBonusAchievemntStages.SetActive (true);
			}

			if (item.IsUnlcoked) {
				
				confirmImg.SetActive (true);
				confirmLockedImg.SetActive (false);
				confirmBtn.onClick.AddListener (() => {
					//Debug.Log("CLICK");
					DataManager.instance.oPlayer.Coins += itemCopy.CoinsBonus;
					DataManager.instance.oPlayer.Character.MaxKiLevelBase+= itemCopy.PowerBonus;
					itemCopy.IsCompleted =true;
					SaveData.Save();
					GenerateWavesBonusPanel();
				});

			} else {
				confirmImg.SetActive (false);
				confirmLockedImg.SetActive (true);
			}
			if (item.IsCompleted) {
				confirmBtn.gameObject.SetActive (false);
				completedImg.SetActive (true);
			} else {
				completedImg.gameObject.SetActive (false);

			}


		}
	}
	public void OpenWavesBonusPanel(){
		WavesBonusMenu.transform.parent.transform.parent.gameObject.SetActive (true);
	}
	public void CloseWavesBonusPanel(){
		WavesBonusMenu.transform.parent.transform.parent.gameObject.SetActive (false);
	}
	public void HighlightSkills(){
		goHighlightSkills.SetActive (DataManager.instance.oPlayer.IsAnySkillToBuy());	
	}
	public void ShowBtnLoginGoogle(bool val){
		btnLoginGoogle.gameObject.SetActive (val);
	}

	public void ShowLeaderboardButton(bool val){
		btnShowLeaderboard.gameObject.SetActive (val);
	}

	void OnDisable(){
		DataManager.instance.oPlayer.Character.eKiChange-=UpdateCharacterKiLevelText;
		DataManager.instance.oPlayer.eCoinsChange-=UpdatePlayerCoinsText;
	}

	void GenerateShardsMenu(){
		int counter = 0;
		foreach (ShardModel item in DataManager.instance.oPlayer.Shards.ToList().OrderBy(x=>x.Id)) {
			
			ShardsMenu.transform.GetChild(counter).gameObject.name= "Shard" + counter;
//			ShardsMenu.transform.GetChild(counter).gameObject (false);
			counter += 1;
		}
		counter = 0;
		foreach (ShardModel item in DataManager.instance.oPlayer.Shards.ToList().OrderBy(x=>x.Id)) {
			ShardModel shardCopy = item;
			GameObject goMainShard = ShardsMenu.GetChild (counter).gameObject;
			goMainShard.SetActive (true);
			Image imgShard = goMainShard.transform.Find ("Image").GetComponent<Image> ();
			imgShard.color =  new Color32((byte)shardCopy.Color.r,(byte)shardCopy.Color.g,(byte)shardCopy.Color.b,(byte)shardCopy.Color.a);
			Text txtInfoAmount = goMainShard.transform.Find ("Count").GetComponent<Text> ();
			Text txtInfoRecoverHp = goMainShard.transform.Find ("HpRecoveryText").GetComponent<Text> ();

			Text txtInfoBonusPowerLevel = goMainShard.transform.Find ("BonusPowerLevelText").GetComponent<Text> ();
			Text txtInfoBonusSpeed = goMainShard.transform.Find ("BonusSpeedText").GetComponent<Text> ();
			Text txtInfoBonusExp = goMainShard.transform.Find ("BonusExpText").GetComponent<Text> ();
			Text txtInfoShardName = goMainShard.transform.Find ("ShardNameText").GetComponent<Text> ();
			////Debug.Log ("item.Title ; " + item.Title);
			txtInfoShardName.text = item.Title;
			if (item.Id==0 ) {
				txtInfoShardName.text = "Normal Shard";
			}

			txtInfoBonusPowerLevel.text = "POWER: +" +item.MaxKiLevelBaseBonus.ToString() + "%";
			txtInfoBonusExp.text = "EXP: +"+ item.ExpBonus + "%";
			txtInfoBonusSpeed.text = "SPEED: +" +item.SpeedBonus + "%";
			txtInfoRecoverHp.text = "HP RECOVERY: +" +item.HealthRecoveryAmount + "%/60 SEC";
			txtInfoAmount.text = "x" + item.CurrentAmount.ToString();
			Button btnSelectShard = goMainShard.GetComponent<Button> ();
			Button btnSelectShard2 = goMainShard.transform.Find ("UseBtn").GetComponent<Button> ();
			btnSelectShard2.onClick.RemoveAllListeners ();
			btnSelectShard2.onClick.AddListener (() => {
				DataManager.instance.oPlayer.SelectedShard = shardCopy;
				CloseShardsPanel();
				AssignSelectedShard(shardCopy);
				////Debug.Log("SELECTING SHAD");
				SaveData.Save();
			});

			Image imgLock = goMainShard.transform.Find ("Lock").GetComponent<Image> ();
			btnSelectShard.onClick.RemoveAllListeners ();


			counter += 1;
			if (shardCopy.IsEnoughAmount ()) {
				imgLock.gameObject.SetActive (false);
				imgShard.gameObject.SetActive (true);
				btnSelectShard2.gameObject.SetActive (true);
				btnSelectShard.onClick.AddListener (() => {
					DataManager.instance.oPlayer.SelectedShard = shardCopy;
					CloseShardsPanel();
					AssignSelectedShard(shardCopy);
					//Debug.Log("SELECTING SHAD");
					SaveData.Save();
				});
			} else {
				btnSelectShard2.gameObject.SetActive (false);
//				imgShard.gameObject.SetActive (false);
//				imgShard.color =  new Color32((byte)shardCopy.Color.r,(byte)shardCopy.Color.g,(byte)shardCopy.Color.b,(byte)60);
				imgLock.gameObject.SetActive (true);
			}
		}

	}
	void AssignSelectedShard(ShardModel shard){
		if (shard.Id != 0) {
			SelectedShardImg.sprite = Resources.Load<Sprite> ("Shard");
			SelectedShardImg.color = new Color32((byte)shard.Color.r,(byte)shard.Color.g,(byte)shard.Color.b,(byte)shard.Color.a);
		} else {
			SelectedShardImg.sprite = Resources.Load<Sprite> ("Shard");
			SelectedShardImg.color = Color.white;
		}
		}


	void CloseShardsPanel(){
		ShardsMenu.parent.gameObject.SetActive (false);

	}
	public void GenerateCurrentMap(){
		Image imgMapImage = MapPanel.transform.Find ("MapImg").Find("Image").GetComponent<Image> ();
		Text txtMapName = MapPanel.transform.Find ("MapNameTxt").GetComponent<Text> ();
		txtMapName.text = DataManager.instance.oPlayer.CurrentMap.MapName;
		imgMapImage.sprite = Resources.Load<Sprite> ("MapImages/" + DataManager.instance.oPlayer.CurrentMap.Id);
		if (DataManager.instance.oPlayer.CurrentMap.IsLocked) {
			imgMapImage.color = new Color (1, 1, 1, 0.35f);
		} else {
			imgMapImage.color = new Color (1, 1, 1, 1f);
		}
		Image ImgMapLocked = MapPanel.transform.Find ("MapImgLocked").GetComponent<Image> ();
		ImgMapLocked.gameObject.SetActive (DataManager.instance.oPlayer.CurrentMap.IsLocked);
		AssignDifficulity ();
		GenerateCurrentStage ();
			
	}
	public void GenerateCurrentStage(){
		GenerateWavesBonusPanel ();
		Text txtExpMultipler = MapPanel.transform.Find ("ExpMultiplerTxt").GetComponent<Text> ();
		Text txtCoinsMultipler = MapPanel.transform.Find ("CoinsMultiplerTxt").GetComponent<Text> ();
		Text txtWavesRequired = MapPanel.transform.Find ("WavesRequiredTxt").GetComponent<Text> ();
		txtWavesRequired.text =  "BEAT " +DataManager.instance.oPlayer.CurrentStage.MissionWaves.RequiredAmount + " WAVES TO UNLOCK NEXT DIFFICULTY/MAP";
		txtExpMultipler.text = "EXP x" +DataManager.instance.oPlayer.CurrentStage.ExpMultipler;
		txtCoinsMultipler.text = "COINS X " + DataManager.instance.oPlayer.CurrentStage.CoinsMultipler;
		Text txtCurrentHighscore = MapPanel.Find("CurrentHighScoreTxt").GetComponent<Text>(); 
		txtCurrentHighscore.text = "Highscore: " + +DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentHighscoreAmount;
		Text txtMission = MapPanel.Find ("MissionTxt").GetComponent<Text> ();
		txtMission.text = DataManager.instance.oPlayer.CurrentStage.MissionWaves.Story;
		Transform tmStarRatings = MapPanel.Find ("RatingStars");

//		foreach (Transform item in tmStarRatings) {
//			item.gameObject.SetActive (false);
//		}
		//Debug.Log("DataManager.instance.oPlayer.CurrentStage.StarRatings )  : " +  DataManager.instance.oPlayer.CurrentStage.StarRatings);
		for (int i = 1; i <= DataManager.instance.oPlayer.CurrentStage.StarRatings; i++) {
			GameObject goStarRatingComplete = tmStarRatings.Find (i + "Complete").gameObject;
			GameObject goStarRatingToDo = tmStarRatings.Find (i + "Todo").gameObject;
			goStarRatingComplete.SetActive (true);
			goStarRatingToDo.SetActive (false);
		}
		for (int i = 3; i > DataManager.instance.oPlayer.CurrentStage.StarRatings; i--) {
			GameObject goStarRatingComplete = tmStarRatings.Find (i + "Complete").gameObject;
			GameObject goStarRatingToDo = tmStarRatings.Find (i + "Todo").gameObject;
			goStarRatingComplete.SetActive (false);
			goStarRatingToDo.SetActive (true);
		}

	}

	public void ShowLoadingBox(bool val){
		LoadingBox.gameObject.SetActive (val);
	}


	public void AssignDifficulity(){
		//Debug.Log ("DataManager.instance.oPlayer.CurrentMap.Stages ;" + DataManager.instance.oPlayer.CurrentMap.Stages.Count);
		foreach (StageModel oStage in DataManager.instance.oPlayer.CurrentMap.Stages) {
			Transform tmStage = MapPanel.transform.Find ("StagesMenu").Find (oStage.Id.ToString());
			Image imgStageDifficulity = tmStage.GetComponent<Image>(); 
			Text txtStageLvlNumber = tmStage.Find("Txt").GetComponent<Text>(); 
			Image imgStageDifficulityLocked = tmStage.Find("ImgLocked").GetComponent<Image>(); 
			imgStageDifficulity.color = Color.white;
			imgStageDifficulityLocked.gameObject.SetActive (oStage.IsLocked);

			txtStageLvlNumber.text = oStage.Id.ToString ();
			StageModel oStageCopy = oStage;
			Button btnStageDifficulity = tmStage.GetComponent<Button> ();
			btnStageDifficulity.onClick.RemoveAllListeners ();
			btnStageDifficulity.onClick.AddListener (() => {
				
				DataManager.instance.oPlayer.CurrentStage = oStageCopy;
				GenerateCurrentStage();
				for (int i = 0; i < DataManager.instance.oPlayer.CurrentMap.Stages.Count; i++) {
					Transform _tmStage = MapPanel.transform.Find ("StagesMenu").Find (DataManager.instance.oPlayer.CurrentMap.Stages[i].Id.ToString());
					Image _imgStageDifficulity = _tmStage.GetComponent<Image>(); 
					_imgStageDifficulity.color = Color.white;
				}
				imgStageDifficulity.color = Color.yellow;
			});
		}

		Transform tmStageCurrent = MapPanel.transform.Find ("StagesMenu").Find (DataManager.instance.oPlayer.CurrentStage.Id.ToString());
		Image imgStageDifficulityCurrent = tmStageCurrent.GetComponent<Image>(); 
		imgStageDifficulityCurrent.color = Color.yellow;
	}
	
//	void GenerateStagesUi(){
//		foreach (StageModel oStage in DataManager.instance.oPlayer.Maps) {
//			////Debug.Log("Stage  : " +oStage.Id);
//			Transform tmStage = StagesParent.Find("Stage"+oStage.Id);
//			Text lvlText = tmStage.Find("LvlText").GetComponent<Text>();
//
//			GameObject goLocked = tmStage.Find("Locked").gameObject;
//			GameObject goUnlocked = tmStage.Find("Unlocked").gameObject;
//			MenuStageTrigger MenuStageTriggerCollider = tmStage.Find("Collider").GetComponent<MenuStageTrigger>();
//			StageModel thisStage = oStage;
//			goUnlocked.SetActive(false);
//			lvlText.text="Level: " +thisStage.Id.ToString();
//			MenuStageTriggerCollider.oStage =thisStage; 
//				if(oStage.IsLocked){
//				
//				goLocked.SetActive(true);
//				}
//			else if(!oStage.IsLocked){
////				goUnlocked.SetActive(true);	
//				goLocked.SetActive(false);
////				goUnlocked.gameObject.GetComponent<MonoBehaviour>()
//			}
//		}
//	}

	public void OpenMapStagesPanel(){
		ShardsMenu.parent.gameObject.SetActive (false);
		MenuPanel.gameObject.SetActive(false);
		MapPanel.gameObject.SetActive(true);
	}
	public void CloseMapStagesPanel(){
		ShardsMenu.parent.gameObject.SetActive (false);
		MenuPanel.gameObject.SetActive(true);
		MapPanel.gameObject.SetActive(false);
	}

	public void CloseMenuPanel(){
		MenuPanel.gameObject.SetActive(false);
		ShardsMenu.parent.gameObject.SetActive (false);
//		StageSelectMenuPanel.gameObject.SetActive(true);
	}
	public void OpenMenuPanel(){
		ShardsMenu.parent.gameObject.SetActive (false);
		MapPanel.gameObject.SetActive(false);
		MenuPanel.gameObject.SetActive(true);

//		MenuPanel.gameObject.SetActive(false);
//		StageSelectMenuPanel.gameObject.SetActive(false);
	}
	public void CLoseAskPanel(){
		AskPanel.gameObject.SetActive(false);
	}
	public void OpenShardsPanel(){
		//Debug.Log ("Openning shardsmneu : ");
		ShardsMenu.parent.gameObject.SetActive (true);
//		MenuPanel.gameObject.SetActive(false);
//		MapPanel.gameObject.SetActive(false);
	}



	public void OpenSkillPanel(){
		ShardsMenu.parent.gameObject.SetActive (false);
		MenuPanel.gameObject.SetActive(false);
//		StageSelectMenuPanel.gameObject.SetActive(false);
		SkillsMenuPanel.gameObject.SetActive(true);
	}
	public void CloseSkillPanel(){
		ShardsMenu.parent.gameObject.SetActive (false);
		MenuPanel.gameObject.SetActive(true);
//		StageSelectMenuPanel.gameObject.SetActive(false);
		SkillsMenuPanel.gameObject.SetActive(false);
	}
//	public void OpenAskPanel<AskPanelMethod>(AskPanelMethod method,string message ){
	public void ConfirmStageLoad(string message, StageModel stage ){
//		////Debug.Log("Param : " +method.ToString());

		Text AskText = AskPanel.Find("AskText").GetComponent<Text>();
		AskText.text = message;
		Button yesBtn = AskPanel.Find("Yes").GetComponent<Button>();
		Button noBtn = AskPanel.Find("No").GetComponent<Button>();
		noBtn.onClick.RemoveAllListeners();
		yesBtn.onClick.RemoveAllListeners();
		AskPanel.gameObject.SetActive(true);
//		yesBtn.onClick.AddListener(method);
		yesBtn.onClick.AddListener(()=>{
			DataManager.instance.StartCoroutine(DataManager.instance.LoadLevel(stage,false));
//			method;
//			method();
//			AskPanel.gameObject.SetActive(false);
		});
		noBtn.onClick.AddListener(()=>{
			AskPanel.gameObject.SetActive(false);
		});
	}

	public void ConfirmSkillBuy(string message, SkillModel oSkill){
		//		////Debug.Log("Param : " +method.ToString());

		Text AskText = AskPanel.Find("AskText").GetComponent<Text>();
		AskText.text = message;
		Button yesBtn = AskPanel.Find("Yes").GetComponent<Button>();
		Button noBtn = AskPanel.Find("No").GetComponent<Button>();
		Button okBtn = AskPanel.Find("Ok").GetComponent<Button>();
		okBtn.gameObject.SetActive(false);
		okBtn.onClick.RemoveAllListeners();
		noBtn.onClick.RemoveAllListeners();
		yesBtn.onClick.RemoveAllListeners();
		AskPanel.gameObject.SetActive(true);
		//		yesBtn.onClick.AddListener(method);
		okBtn.onClick.AddListener(()=>{AskPanel.gameObject.SetActive(false);
			yesBtn.gameObject.SetActive(true);
			noBtn.gameObject.SetActive(true);
		});
		yesBtn.onClick.AddListener(()=>{
			bool isBuySuccess= oSkill.Buy(DataManager.instance.oPlayer);
			if(isBuySuccess){
				AskPanel.gameObject.SetActive(false);
				GenerateCurrentSkillDescription(oSkill);
				GenerateSkillsPanelContent();
				HighlightSkills();
				}
			else if(!isBuySuccess){
				okBtn.gameObject.SetActive(true);
				AskText.text="You cant buy this skill right now!";
				yesBtn.gameObject.SetActive(false);
				noBtn.gameObject.SetActive(false);
			}
//			DataManager.instance.StartCoroutine(DataManager.instance.LoadLevel(stage));
			//			method;
			//			method();

		});
		noBtn.onClick.AddListener(()=>{
			AskPanel.gameObject.SetActive(false);
			yesBtn.gameObject.SetActive(true);
			noBtn.gameObject.SetActive(true);
		});
	}
	public void ConfirmSkillUpgrade(string message, SkillModel oSkill){
		//		////Debug.Log("Param : " +method.ToString());

		Text AskText = AskPanel.Find("AskText").GetComponent<Text>();
		AskText.text = message;
		Button yesBtn = AskPanel.Find("Yes").GetComponent<Button>();
		Button noBtn = AskPanel.Find("No").GetComponent<Button>();
		Button okBtn = AskPanel.Find("Ok").GetComponent<Button>();
		okBtn.gameObject.SetActive(false);
		okBtn.onClick.RemoveAllListeners();
		noBtn.onClick.RemoveAllListeners();
		yesBtn.onClick.RemoveAllListeners();
		AskPanel.gameObject.SetActive(true);
		//		yesBtn.onClick.AddListener(method);
		okBtn.onClick.AddListener(()=>{AskPanel.gameObject.SetActive(false);
			yesBtn.gameObject.SetActive(true);
			noBtn.gameObject.SetActive(true);
		});
		yesBtn.onClick.AddListener(()=>{
			bool isUpgradeSuccess= oSkill.Upgrade(DataManager.instance.oPlayer);
			if(isUpgradeSuccess){
				AskPanel.gameObject.SetActive(false);
				GenerateCurrentSkillDescription(oSkill);
				GenerateSkillsPanelContent();
			}
			else if(!isUpgradeSuccess){
				okBtn.gameObject.SetActive(true);
				AskText.text="You cant upgrade this skill right now!";
				yesBtn.gameObject.SetActive(false);
				noBtn.gameObject.SetActive(false);
			}
			//			DataManager.instance.StartCoroutine(DataManager.instance.LoadLevel(stage));
			//			method;
			//			method();

		});
		noBtn.onClick.AddListener(()=>{
			AskPanel.gameObject.SetActive(false);
			yesBtn.gameObject.SetActive(true);
			noBtn.gameObject.SetActive(true);
		});
	}





	public void ShowInfoMessage(string message){
		//		////Debug.Log("Param : " +method.ToString());
		AskPanel.gameObject.SetActive(true);
		Text AskText = AskPanel.Find("AskText").GetComponent<Text>();
		AskText.text = message;
		Button yesBtn = AskPanel.Find("Yes").GetComponent<Button>();
		Button noBtn = AskPanel.Find("No").GetComponent<Button>();
		Button okBtn = AskPanel.Find("Ok").GetComponent<Button>();
		okBtn.onClick.RemoveAllListeners();
//		noBtn.onClick.RemoveAllListeners();
//		yesBtn.onClick.RemoveAllListeners();
		yesBtn.gameObject.SetActive(false);
		noBtn.gameObject.SetActive(false);
		AskPanel.gameObject.SetActive(true);
		okBtn.gameObject.SetActive (true);
		//		yesBtn.onClick.AddListener(method);
		okBtn.onClick.AddListener(()=>{AskPanel.gameObject.SetActive(false);
			yesBtn.gameObject.SetActive(true);
			noBtn.gameObject.SetActive(true);
		});

	
	}


	public void GenerateSkillsPanelContent(){
		int counter = 1 ;
		foreach (Transform item in SkillsPanelContent.transform) {
			item.gameObject.name = "Skill" + counter;
			counter += 1;
		}
		counter = 0;
		foreach (SkillModel oSkill in DataManager.instance.oPlayer.Skills.ToList().OrderBy(x=> x.KiLvlRequired)) {
			GameObject goMainSkill = SkillsPanelContent.transform.GetChild (counter).gameObject;
			Button btnSkill = goMainSkill.GetComponent<Button>();
			GameObject goSkillHighlight = goMainSkill.transform.Find ("Highlight").gameObject;
			////Debug.Log("OSKUIL :"  + oSkill.Title + "counter : " +oSkill.Id);
			Image imgSkill = goMainSkill.transform.Find("Image").GetComponent<Image>();
			imgSkill.sprite = Resources.Load<Sprite>("Sprites/Ui/Skills/Skill"+oSkill.Id);
			GameObject goSkill = goMainSkill.transform.Find("Locked").gameObject;
			bool isNew = (oSkill.Price <= DataManager.instance.oPlayer.Coins && !oSkill.IsBought && DataManager.instance.oPlayer.Character.MaxKiLevelBase >= oSkill.KiLvlRequired);
			goSkillHighlight.SetActive (isNew);

			SkillModel oCurrentSkill = oSkill;
			if(oSkill.IsBought){
				goSkill.SetActive(false);
//				btnBuy.gameObject.SetActive(false);
			}
			else if(!oSkill.IsBought){
				goSkill.SetActive(true);
//				btnBuy.gameObject.SetActive(true);

			}
			btnSkill.onClick.RemoveAllListeners();
			btnSkill.onClick.AddListener(()=>{
				GenerateCurrentSkillDescription(oCurrentSkill);
			});
			counter += 1;
//			counter+=1;
		}
	}


	void GenerateCurrentSkillDescription(SkillModel oSkill){
		//Debug.Log ("generating sescription for : " + oSkill.Title + "  isBought? : " + oSkill.IsBought);
		btnUpgrade.gameObject.SetActive (false);
//		SkillPriceText.transform.parent.gameObject.SetActive (false);
		CurrentSkillLevelText.text = "LEVEL: " + oSkill.Level.ToString();
		CurrentSkillLevelText.transform.parent.gameObject.SetActive (oSkill.SkillUsageType!= SkillUsageType.Passive);

		btnUpgrade.onClick.RemoveAllListeners ();
		if(oSkill.IsBought){
			
			btnBuy.gameObject.SetActive(false);
			GoBought.SetActive(false);
			if (oSkill.Level <= 3  && oSkill.SkillUsageType == SkillUsageType.Active) {
				btnUpgrade.gameObject.SetActive (true);
				btnUpgrade.onClick.AddListener (() => {
					ConfirmSkillUpgrade ("Are you sure you want to upgrade " + oSkill.Title + " for " + oSkill.Price + "?", oSkill);
			
				});
			}
		}
		else if (!oSkill.IsBought){
			SkillPriceText.transform.parent.gameObject.SetActive (true);
			GoBought.SetActive(false);
			btnBuy.gameObject.SetActive(true);
			btnUpgrade.gameObject.SetActive (false);

		}

		btnBuy.onClick.RemoveAllListeners();
		btnBuy.onClick.AddListener(()=>{
			ConfirmSkillBuy("Are you sure you want to buy " + oSkill.Title  + " for " + oSkill.Price + "?",oSkill);
				});

//		if (oSkill.oSkillType!= SkillType.Transformation) {
//			
//		}

		SkillImageImage.sprite = Resources.Load<Sprite>("Sprites/Ui/Skills/Skill"+oSkill.Id);
		KiLevelRequiredTxt.text = System.Math.Round(oSkill.KiLvlRequired,0).ToString();
		CooldownText.text = System.Math.Round(oSkill.Cooldown,0).ToString();
		SkillNameText.text = oSkill.Title;
		DamageText.text =System.Math.Round(oSkill.DamageMultipler * 100,0).ToString() + "%";
		SkillPriceText.text = System.Math.Round(oSkill.Price,0).ToString ();
		SkillEnergyNeededText.text = System.Math.Round(oSkill.KiNeededInPercentage,0).ToString () +"%";
		SkillDescriptionText.text = oSkill.Description;

		if (oSkill.Level > 3) {
			SkillPriceText.transform.parent.gameObject.SetActive (false);
		}
		if (oSkill.SkillPassiveType == SkillPassiveType.SensePower || oSkill.oSkillType == SkillType.Transformation) {
			DamageText.transform.parent.gameObject.SetActive (false);
			CooldownText.transform.parent.gameObject.SetActive (false);
//			SkillEnergyNeededText.transform.parent.gameObject.SetActive (false);

		} else {
			CooldownText.transform.parent.gameObject.SetActive (true);
			DamageText.transform.parent.gameObject.SetActive (true);
//			SkillEnergyNeededText.transform.parent.gameObject.SetActive (true);
		}

	}

	public void UpdateCharacterKiLevelText(float val, float maxVal){
		////Debug.Log( " UPDATING CharacterKiLevelText : " + maxVal);
		CharacterKiLevelText.text =(maxVal.ConvertNumber());
	}
	public void UpdatePlayerCoinsText(double  val){
		
		CoinsText.text = (float.Parse(val.ToString()).ConvertNumber().ToString());
	}
}
