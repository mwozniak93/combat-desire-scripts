﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

public class PlayerSkillUi : MonoBehaviour
{
	[SerializeField]
	Transform SkillsPanel, SkillsPanelScrollRect, SkillPanelScrollRectItemsList;
	CharacterModel oCharacterModel;
	//	GameObject specialSkillButton;
	[SerializeField]
	Text txtSelectedSkillName;
	[SerializeField]
	Image selectedSkillImage, selectedSkillImageBg, selectedSkillImagePause, selectedSkillImageBgPause;
	[SerializeField]
	GameObject CurrentSkillDesc;
	//	Transform SkillsPanelContent;
	PlayerSkillController ctPlayerSkillController;
	GameObject playerGo;
	// Use this for initialization
	public static PlayerSkillUi Instance;

	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
		} else {
			Destroy (Instance);
		}
	}


	public void ShowSkillsScrollRectPanel (bool val)
	{
		if (oCharacterModel.States == States.Idle || oCharacterModel.States == States.Walking) {
			//Debug.Log ("SkillsPanelScrollRect.gameObject.activeInHierarchy ; " + SkillsPanelScrollRect.gameObject.activeInHierarchy);
			SkillsPanelScrollRect.gameObject.SetActive (val);
		}
	
	}

	void GenerateCurrentSkillDesc ()
	{
		Text CurrentSkillDescText = CurrentSkillDesc.transform.Find ("Desc").Find ("TextDesc").GetComponent<Text> ();
		Text CurrentSkillCooldownText = CurrentSkillDesc.transform.Find ("Cooldown").Find ("TextCooldown").GetComponent<Text> ();
		Text CurrentSkillPowerNeededText = CurrentSkillDesc.transform.Find ("PowerNeeded").Find ("TextPowerNeeded").GetComponent<Text> ();

		CurrentSkillPowerNeededText.text = DataManager.instance.oPlayer.Character.oCurrentSkill.KiNeededInPercentage + "%";
		CurrentSkillCooldownText.text = DataManager.instance.oPlayer.Character.oCurrentSkill.Cooldown + " SEC";
		CurrentSkillDescText.text = DataManager.instance.oPlayer.Character.oCurrentSkill.Description;
	}

	public void InitHud ()
	{
		playerGo = PlayerSingleton.instance.gameObject;
		oCharacterModel = playerGo.GetComponent<CharacterContainer> ().oCharacter;
		ctPlayerSkillController = playerGo.GetComponent<PlayerSkillController> ();
		GeneratePanel ();
		GeneratePanelScrollRect ();
		foreach (Text item in Component.FindObjectsOfType<Text>()) {
			item.font = Resources.Load<Font> ("Comic_Book");
		}
	}

	public void OpenPanel ()
	{
	}

	public void GeneratePanelScrollRect ()
	{
		float counter = 1;
		foreach (SkillModel item in oCharacterModel.PlayerSkillsList.OrderBy(skill=>skill.KiLvlRequired).ToList()) {
			if (item.SkillUsageType == SkillUsageType.Active) {
				//				//Debug.Log ("skill : " + item.Id + " lvl  :" + item.Title + " oCharacterModel.PlayerSkillsList : " + oCharacterModel.PlayerSkillsList.Count);
				Transform skillItem = SkillPanelScrollRectItemsList.Find ("Skill");
				skillItem.name = "Skill" + item.Id;
				skillItem.gameObject.SetActive (true);  
				Button bgSelectButton = skillItem.GetComponent<Button> (); 
				Image image = skillItem.GetComponent<Image> (); 
				SkillModel oCurrentSkill = item;
				image.sprite = Resources.Load<Sprite> ("Sprites/Ui/Skills/Skill" + item.Id);
				bgSelectButton.onClick.RemoveAllListeners ();
				bgSelectButton.onClick.AddListener (() => {
					if (oCharacterModel.States == States.Idle || oCharacterModel.States == States.Walking) {
						oCurrentSkill.eSkillChange += AssignSelectedSkill;
						oCurrentSkill.OnSkillChange (oCurrentSkill);
						ShowSkillsScrollRectPanel (false);	
					}
				});
				counter += 1;
			}
		}
	}

	public void GeneratePanel ()
	{
		float counter = 1;
		foreach (SkillModel item in oCharacterModel.PlayerSkillsList.OrderBy(skill=>skill.KiLvlRequired).ToList()) {
			if (item.SkillUsageType == SkillUsageType.Active) {
//				//Debug.Log ("skill : " + item.Id + " lvl  :" + item.Title + " oCharacterModel.PlayerSkillsList : " + oCharacterModel.PlayerSkillsList.Count);
				Transform skillItem = SkillsPanel.Find ("Skill");
				skillItem.name = "Skill" + item.Id;
				skillItem.gameObject.SetActive (true);
				Text damageText = skillItem.Find ("Damage").GetComponent<Text> ();  
				Text kiText = skillItem.Find ("Ki").GetComponent<Text> ();  
				Button bgSelectButton = skillItem.Find ("Image").GetComponent<Button> (); 
				Text nameText = skillItem.Find ("Name").GetComponent<Text> ();  
				Image image = skillItem.Find ("Image").GetComponent<Image> (); 
				SkillModel oCurrentSkill = item;
				kiText.text = "Needed Ki: " + (item.KiNeededInPercentage).ToString ();
				damageText.text = "Damage: " + (item.DamageMultipler).ToString ();
				nameText.text = "" + item.Title;
				image.sprite = Resources.Load<Sprite> ("Sprites/Ui/Skills/Skill" + item.Id);
				bgSelectButton.onClick.RemoveAllListeners ();
				bgSelectButton.onClick.AddListener (() => {
//				oCharacterModel.oCurrentSkill.eSkillChange -= AssignSelectedSkill;
					if (oCharacterModel.States == States.Idle || oCharacterModel.States == States.Walking) {
						oCurrentSkill.eSkillChange += AssignSelectedSkill;
						oCurrentSkill.OnSkillChange (oCurrentSkill);
					}
//				AssignSelectedSkill();
					OpenPanel ();
				});
//			skillItem.gameObject.SetActive(true);
//			if(item.IsBought)
				counter += 1;
			}
		}
	}

	public void AssignSelectedSkill (SkillModel oSkill)
	{
		if (oCharacterModel.States == States.Idle || oCharacterModel.States == States.Walking) {
			
		
			if (oCharacterModel.oCurrentSkill != null) {
				if (oSkill.SkillUsageType == SkillUsageType.Active) {
					ctPlayerSkillController.ChangeSkill (oSkill);
					selectedSkillImage.sprite = Resources.Load<Sprite> ("Sprites/Ui/Skills/Skill" + oSkill.Id);
					selectedSkillImageBg.sprite = Resources.Load<Sprite> ("Sprites/Ui/Skills/Skill" + oSkill.Id);
					selectedSkillImagePause.sprite = Resources.Load<Sprite> ("Sprites/Ui/Skills/Skill" + oSkill.Id);
					selectedSkillImageBgPause.sprite = Resources.Load<Sprite> ("Sprites/Ui/Skills/Skill" + oSkill.Id);
					txtSelectedSkillName.text = oSkill.Title;
				}
				GenerateCurrentSkillDesc ();
			}
		}
	}

}
