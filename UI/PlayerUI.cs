﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.Text.RegularExpressions;


public class PlayerUI : MonoBehaviour
{
	public static PlayerUI instance;
	//	[SerializeField]
	Text SpeedText, KiLevelText, HealthText, CoinsText, MissionText, MissionInfoText;
	[SerializeField]
	Text CoinsPauseText, CrystalsPauseText, KiLvlPauseText, GameEndTxt, NeedMorePowerLvlTxt, CrystalsMissionText, CurrentCrystalsAmountText, WavesProgressPauseMenuTxt, health2Txt, KiLevel2Txt, damageTxt, maxDamageTxt;
	public GameObject goSensePower;
	Text WaveText;
	//	[SerializeField]
	Image HealthImage, KiLevelImage, CurrentSkillImage, CoinsImage, MissionImage;
	[SerializeField]
	Button btnRestoreHp, btnRestoreKi, btnExit, btnExitWhenPause;
	[SerializeField]
	Image WavesProgressBarImage;
	//	List<Text> FloatingTexts;
	CharacterModel oPlayer;
	[SerializeField]
	public GameObject specialSkillBtn;
	[SerializeField]
	Transform GameOverPanel, GameCompletedPanel, AskPanel, PausePanel, goNewHighscore, sensePowerCanvas, tmMissionsInfo, tmStartRatings;
	GameObject TelekinessisGoParent;
	[SerializeField]
	Image health2Img, kiLevel2Img;
	public MapCanvasController ctMapCanvasController;

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			instance = null;
		}

	}
	// Use this for initialization
	public void InitHud ()
	{
//		ctMapCanvasController.playerTransform = PlayerSingleton.instance.transform;
		Transform hud = transform;
//		KiLevelText = hud.Find("KiLevel").Find("Health").Find("HealthText").GetComponent<Text>();
		KiLevelText = hud.Find ("KiLevel").Find ("KiBar").Find ("KiLevelText").GetComponent<Text> ();
		MissionInfoText = hud.Find ("MissionsInfo").Find ("MissionInfoText").GetComponent<Text> ();
		TelekinessisGoParent = hud.Find ("Buttons").Find ("Telekinessis").gameObject;
		MissionText = hud.Find ("KiLevel").Find ("Mission").Find ("CrystalsMissionTxt").GetComponent<Text> ();
		CoinsText = hud.Find ("KiLevel").Find ("Coins").Find ("CoinsTxt").GetComponent<Text> ();
		HealthText = hud.Find ("KiLevel").Find ("Health").Find ("HealthText").GetComponent<Text> ();
		HealthImage = hud.Find ("KiLevel").Find ("Health").Find ("Main").GetComponent<Image> ();
		KiLevelImage = hud.Find ("KiLevel").Find ("KiBar").Find ("Main").GetComponent<Image> ();
		CoinsImage = hud.Find ("KiLevel").Find ("Coins").Find ("CoinsImg").GetComponent<Image> ();
		MissionImage = hud.Find ("KiLevel").Find ("Mission").Find ("MissionImg").GetComponent<Image> ();
		CurrentSkillImage = hud.Find ("Skills").Find ("Bg").Find ("Skills").GetComponent<Image> ();
		specialSkillBtn = hud.Find ("Buttons").Find ("B").Find ("SkillBtn").gameObject;
		WaveText = hud.Find ("WaveTxt").GetComponent<Text> ();
		oPlayer = PlayerSingleton.instance.gameObject.GetComponent<CharacterContainer> ().oCharacter;
		MissionInfoText.gameObject.SetActive (false);
//		////Debug.Log("BEFORE oPlayer.eKiChange : " +oPlayer.eKiChange);
		oPlayer.eKiChange += UpdateKiLevel;
//		////Debug.Log("AFTER oPlayer.eKiChange : " +oPlayer.eKiChange);
		oPlayer.eHpChange += UpdateHealth;
		oPlayer.MaxHealth = oPlayer.MaxHealth;
		oPlayer.Health = oPlayer.Health;
		oPlayer.MaxKiLevel = oPlayer.MaxKiLevel;
	
//		oPlayer.eDie+=HideHud;
		oPlayer.eDie += ShowGameCompletedPanel;
		oPlayer.eTkObjectMet += ShowTelekinessisBtn;


		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionUpdate += UpdateWave;
//		foreach (Wave item in DataManager.instance.oPlayer.CurrentStage.Waves) {
//			item.eWaveNumberChange += UpdateWave;
//		}
//		DataManager.instance.oPlayer.CurrentStage.CurrentWave.eWaveNumberChange += UpdateWave;
//		GameStats.instance.eCoinsChange+=UpdateCoins;
		DataManager.instance.oPlayer.eCoinsChange += UpdateCoins;
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionUpdate += UpdateMission;
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionComplete += MissionComplete;

		////Debug.Log("SUBSCRIBING PLAYERUI" + " oPlayer : " + oPlayer.KiLevel + " / : "  + oPlayer.MaxKiLevel);
		//		oPlayer.eDamageTaken+=UpdateDamageTaken;
		oPlayer.oCurrentSkill.eCooldownChange += UpdateSkillCooldown;
//		GameStats.instance.Mission.Story = DataManager.instance.oPlayer.CurrentStage.Mission.Story;
//		GameStats.instance.Mission.CurrentAmount=0;
//		GameStats.instance.Mission.RequiredAmount=DataManager.instance.oPlayer.CurrentStage.Mission.RequiredAmount;

//		GameStats.instance.Mission.RequiredAmount=DataManager.instance.oPlayer.CurrentStage.Mission.Story;
		DataManager.instance.oPlayer.Coins = DataManager.instance.oPlayer.Coins;
		////Debug.Log("COINS : " + DataManager.instance.oPlayer.Coins);
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount = DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount;
//		GameStats.instance.Mission.eMissionComplete+=ShowGameOverPanel;
		DataManager.instance.oPlayer.CurrentStage.eStageComplete += ShowGameCompletedPanel;
		DataManager.instance.oPlayer.eCoinsChange += PlayerSingleton.instance.GetComponent<CharacterUi> ().UpdateCoins;
		GlobalUiManager.instance.StartCoroutine (GlobalUiManager.instance.FadeIn ());
		AssignRestoreBtns ();
		oPlayer.eDialogShow += UpdateNeedPowerLvl;
		DataManager.instance.oPlayer.eSensePower += ShowCanvasPowerSense;
		DataManager.instance.oPlayer.eShardCollected += UpdateBonusShards;
//		DataManager.instance.oPlayer.CurrentStage.MissionCrystals.eMissionComplete+= CrystalMissionComplete;
//		DataManager.instance.oPlayer.CurrentStage.MissionCrystals.eMissionUpdate+= CrystalsMissionUpdate;
//		CrystalsMissionUpdate (DataManager.instance.oPlayer.CurrentStage.MissionCrystals);
//		btnExit.onClick.AddListener (() => {
//			DataManager.instance.StartCoroutine(DataManager.instance.LoadMenuLevel());
//		});
		btnExitWhenPause.onClick.AddListener (() => {
			ConfirmReturn ();
		});
		foreach (Text item in Component.FindObjectsOfType<Text>()) {
			item.font = Resources.Load<Font> ("Comic_Book");
		}
		DataManager.instance.CurrentUiState = UiState.Game;
	}

	void UpdateNeedPowerLvl (string msg)
	{
		if (!NeedMorePowerLvlTxt.transform.parent.gameObject.activeInHierarchy) {
			NeedMorePowerLvlTxt.transform.parent.gameObject.SetActive (true);
			NeedMorePowerLvlTxt.text = "Need more power";
		}

	}

	void ShowCanvasPowerSense (bool val)
	{
		sensePowerCanvas.gameObject.SetActive (val);

	}

	void CrystalMissionComplete (Mission missionCrystals)
	{
		CrystalsMissionText.text = missionCrystals.CurrentAmount.ToString () + "/" + missionCrystals.RequiredAmount;
	}

	void CrystalsMissionUpdate (Mission missionCrystals)
	{
		CrystalsPauseText.text = "Found Crystals: \n" + missionCrystals.CurrentAmount + "/" + missionCrystals.RequiredAmount;
		MissionInfoText.gameObject.SetActive (true);
		MissionInfoText.text = "Found Crystals: \n" + missionCrystals.CurrentAmount + "/" + missionCrystals.RequiredAmount;
		CurrentCrystalsAmountText.text = missionCrystals.CurrentAmount + "/" + missionCrystals.RequiredAmount;
	}

	void MissionComplete (Mission mission)
	{
		goNewHighscore.gameObject.SetActive (false);

//		//Debug.Log("DataManager.instance.oPlayer.CurrentStage.StarRatings )  : " +  DataManager.instance.oPlayer.CurrentStage.StarRatings);
		for (int i = 1; i <= DataManager.instance.oPlayer.CurrentStage.StarRatings; i++) {
			GameObject goStarRatingComplete = tmStartRatings.Find (i + "Complete").gameObject;
			GameObject goStarRatingToDo = tmStartRatings.Find (i + "Todo").gameObject;
			goStarRatingComplete.SetActive (true);
			goStarRatingToDo.SetActive (false);
		}
		for (int i = 3; i > DataManager.instance.oPlayer.CurrentStage.StarRatings; i--) {
			GameObject goStarRatingComplete = tmStartRatings.Find (i + "Complete").gameObject;
			GameObject goStarRatingToDo = tmStartRatings.Find (i + "Todo").gameObject;
			goStarRatingComplete.SetActive (false);
			goStarRatingToDo.SetActive (true);
		}
	}

	void UpdateWave (Mission mission)
	{
		WaveText.text = "WAVE: " + mission.CurrentAmount; 
	}

	void UpdateMission (Mission mission)
	{
		//Debug.Log ("Updating... : RequiredAmount : " +  mission.RequiredAmount  + " mission.curren : " + mission.CurrentAmount +  " mission.isCompletec ? : "+  mission.IsCompleted());
		WavesProgressPauseMenuTxt.text = mission.CurrentAmount + "/" + mission.RequiredAmount;
		WavesProgressBarImage.fillAmount = mission.CurrentAmount / mission.RequiredAmount;
		if (mission.IsCompleted ()) {
			WavesProgressPauseMenuTxt.text = "ALL DONE";
			WavesProgressBarImage.fillAmount = 1;

		}
		if (mission.CurrentAmount > 0) {
		} else if (mission.CurrentAmount <= 0) {			
		} else if (mission.CurrentAmount >= mission.RequiredAmount) {
		}

		MissionText.text = mission.CurrentAmount + "/" + mission.RequiredAmount;
	}


	void UpdateBonusShards (ItemType shard)
	{
		tmMissionsInfo.gameObject.SetActive (true);
		MissionInfoText.gameObject.SetActive (true);
		MissionInfoText.text = "BONUS " + Regex.Replace (shard.ToString (), "([a-z])([A-Z])", "$1 $2") + " COLLECTED!!";	

	}

	void UpdateCoins (double val)
	{
		CoinsText.text = float.Parse (val.ToString ()).ConvertNumber ().ToString ();
		CoinsPauseText.text = float.Parse (val.ToString ()).ConvertNumber ().ToString ();
	}

	void HideHud ()
	{
		//btnPause.SetActive(false);
		gameObject.SetActive (false);
	}

	void OnDestroy ()
	{
//		////Debug.Log("Unsubscribing");
		oPlayer.eHpChange -= UpdateHealth;
//		oPlayer.eDie-=HideHud;
		oPlayer.eDie -= ShowGameCompletedPanel;
		oPlayer.eKiChange -= UpdateKiLevel;
		oPlayer.oCurrentSkill.eCooldownChange -= UpdateSkillCooldown;
		oPlayer.eTkObjectMet -= ShowTelekinessisBtn;
		DataManager.instance.oPlayer.eCoinsChange -= UpdateCoins;
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionUpdate -= UpdateMission;
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionUpdate -= UpdateWave;
		DataManager.instance.oPlayer.CurrentStage.eStageComplete -= ShowGameCompletedPanel;
		oPlayer.eDialogShow -= UpdateNeedPowerLvl;
		DataManager.instance.oPlayer.eSensePower -= ShowCanvasPowerSense;
		DataManager.instance.oPlayer.eShardCollected -= UpdateBonusShards;
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.eMissionComplete -= MissionComplete;
//		DataManager.instance.oPlayer.eCoinsChange-=PlayerSingleton.instance.GetComponent<CharacterUi>().UpdateCoins;
//		DataManager.instance.oPlayer.CurrentStage.MissionCrystals.eMissionComplete-= CrystalMissionComplete;
//		DataManager.instance.oPlayer.CurrentStage.MissionCrystals.eMissionUpdate-= CrystalsMissionUpdate;
	}

	IEnumerator ShowGameCompletedPanelCorutine ()
	{
		yield return new WaitForSeconds (1.5f);
		goNewHighscore.gameObject.SetActive (false);

		Transform CompleteList = GameCompletedPanel.Find ("CompleteList").Find ("Content");
		GameObject goNewHighscore2 = CompleteList.Find ("NewHighscore").gameObject;
		Button btnReturn = GameCompletedPanel.Find ("Return").GetComponent<Button> ();
		//		Button btnReturn =  GameCompletedPanel.Find("Return").GetComponent<Button>();
		Text coinsText = CompleteList.Find ("Coins").Find ("Text").GetComponent<Text> ();
		coinsText.text = "COINS +: " + System.Math.Round (DataManager.instance.oPlayer.CurrentStage.Coins).ToString ();
		Text newKiLevelText = CompleteList.Find ("KiLvlGained").Find ("Text").GetComponent<Text> ();
		Text powerBonusText = CompleteList.Find ("PowerBonus").GetComponent<Text> ();
		Text coinsBonusText = CompleteList.Find ("CoinsBonus").GetComponent<Text> ();
		Text wavesSurvivedText = CompleteList.Find ("WavesSurvived").GetComponent<Text> ();
		wavesSurvivedText.text = "WAVES: " + DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount + "/" + DataManager.instance.oPlayer.CurrentStage.MissionWaves.RequiredAmount;
		newKiLevelText.text = "POWER +: " + System.Math.Round (DataManager.instance.oPlayer.CurrentStage.KiLvlGained).ToString ();
		goNewHighscore2.SetActive (false);
		//		btnReturn.onClick.RemoveAllListeners();
		btnReturn.onClick.RemoveAllListeners ();
		//		btnReturn.onClick.AddListener(()=>{
		//			DataManager.instance.StartCoroutine(DataManager.instance.LoadLevel(DataManager.instance.oPlayer.CurrentStage));
		//		});
		Button btnRestart = GameCompletedPanel.Find ("Restart").GetComponent<Button> ();
		btnRestart.onClick.RemoveAllListeners ();
		btnRestart.onClick.AddListener (() => {
			//Debug.Log("assign to btnRestart : " );
			//			DataManager.instance.oPlayer.FinishStageLose(DataManager.instance.oPlayer.Character.StartMaxBaseKiLvl,DataManager.instance.oPlayer.CurrentStage.Coins);
			//			DataManager.instance.StartCoroutine(DataManager.instance.RestartGame());
			DataManager.instance.StartCoroutine (DataManager.instance.LoadLevel (DataManager.instance.oPlayer.CurrentStage, true));
//			DataManager.instance.StartCoroutine(DataManager.instance.LoadLevel(DataManager.instance.oPlayer.CurrentStage,true));
		});
		float bonusCoins = DataManager.instance.oPlayer.CurrentStage.MissionWaves.BonusCoins * (DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount - 1);
		float bonusPower = DataManager.instance.oPlayer.CurrentStage.ExpMultipler * DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount;
		coinsBonusText.text = "BONUS COINS : +" + System.Math.Round (bonusCoins).ToString ();
		powerBonusText.text = "BONUS POWER : +" + System.Math.Round (bonusPower).ToString ();

		if (DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount > DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentHighscoreAmount) {
			goNewHighscore.gameObject.SetActive (true);
			goNewHighscore2.SetActive (true);
		}
		DataManager.instance.oPlayer.FinishStageWin (DataManager.instance.oPlayer.Character.MaxKiLevelBase + bonusPower, bonusCoins);
		MissionComplete (DataManager.instance.oPlayer.CurrentStage.MissionWaves);
		btnReturn.onClick.AddListener (() => {
			//Debug.Log("CLICK  : DataManager.instance.oPlayer.CurrentStage.Coins : " + DataManager.instance.oPlayer.CurrentStage.Coins +  "  DataManager.instance.oPlayer.CurrentStage.bonusCoins  + "   + bonusPower  );

			DataManager.instance.StartCoroutine (DataManager.instance.LoadMenuLevel (false));
		});
		GameCompletedPanel.gameObject.SetActive (true);
		HideHud ();
	}

	void ShowGameCompletedPanel ()
	{


		StartCoroutine (ShowGameCompletedPanelCorutine ());

	}

	void ShowGameOverPanel ()
	{
		Text coinsText = GameOverPanel.Find ("Coins").Find ("Text").GetComponent<Text> ();
		coinsText.text = DataManager.instance.oPlayer.CurrentMap.ToString ();
		Text newKiLevelText = GameOverPanel.Find ("KiLvlGained").Find ("Text").GetComponent<Text> ();
		newKiLevelText.text = 0.ToString ();
		Button btnRestart = GameOverPanel.Find ("Restart").GetComponent<Button> ();
		Button btnReturn = GameOverPanel.Find ("Return").GetComponent<Button> ();
		btnReturn.onClick.RemoveAllListeners ();
		btnRestart.onClick.RemoveAllListeners ();
		btnReturn.onClick.AddListener (() => {
			DataManager.instance.oPlayer.FinishStageLose (DataManager.instance.oPlayer.Character.StartMaxBaseKiLvl, DataManager.instance.oPlayer.CurrentStage.Coins);
			DataManager.instance.StartCoroutine (DataManager.instance.LoadMenuLevel (false));

		});
		btnRestart.onClick.AddListener (() => {
			DataManager.instance.oPlayer.FinishStageLose (DataManager.instance.oPlayer.Character.StartMaxBaseKiLvl, DataManager.instance.oPlayer.CurrentStage.Coins);
			DataManager.instance.StartCoroutine (DataManager.instance.LoadLevel (DataManager.instance.oPlayer.CurrentStage, false));
		});
		GameOverPanel.gameObject.SetActive (true);
		HideHud ();
	}

	public void UpdateSkillCooldown (float val, float maxVal)
	{
		
//		1/States.SelectedSkiillE.coldown*Time.deltaTime
		if (CurrentSkillImage) {
//			////Debug.Log("val : " +val +  " maxval : " +maxVal);
			CurrentSkillImage.fillAmount = val / maxVal;
		}
	}

	void UpdateSpeedText (float val)
	{
		SpeedText.text = "Speed: " + val.ToString ();
	}

	void UpdateHealth (float health, float maxHealth)
	{
		if (HealthImage) {
			HealthImage.fillAmount = (float)health / (float)maxHealth;
		}
		if (health2Img) {
			health2Img.fillAmount = (float)health / (float)maxHealth;
		}

//		//Debug.Log ( "1 maxHealth ;" + maxHealth);
		decimal.Parse (maxHealth.ToString (), NumberStyles.Float);
		health = Convert.ToInt64 (health);
		//maxHealth=Mathf.RoundToInt(maxHealth);
//		//Debug.Log ( "2 maxHealth ;" + maxHealth);
		if (HealthText) {
			HealthText.text = "" + health.ConvertNumber () + "/" + maxHealth.ConvertNumber ();
			health2Txt.text = "Hp: " + health.ConvertNumber () + "/" + maxHealth.ConvertNumber ();
		}
	}

	void UpdateKiLevel (float kiLevel, float maxKiLevel)
	{
//		//Debug.Log("Updatink KI LVL!!!!!!!!!!" +  "maxKiLevel " +  maxKiLevel + "  kilevel ; { " +  kiLevel);
//		if(KiLevelImage){
//		//Debug.Log("kiLevel/maxKiLevel : " + ((float)kiLevel/(float)maxKiLevel));
		KiLevelImage.fillAmount = ((float)kiLevel / (float)maxKiLevel);
		kiLevel2Img.fillAmount = ((float)kiLevel / (float)maxKiLevel);
//		}
		//kiLevel=Mathf.Round(kiLevel);
		//maxKiLevel=Mathf.Round(maxKiLevel);
		if (KiLevelText) {
			KiLvlPauseText.text = "" + Convert.ToInt32 (kiLevel).ToString () + "/" + Convert.ToInt32 (maxKiLevel).ToString ();
			KiLevelText.text = "" + (kiLevel.ConvertNumber ()) + "/" + (maxKiLevel.ConvertNumber ());
			KiLevel2Txt.text = "" + kiLevel.ConvertNumber () + "/" + (maxKiLevel.ConvertNumber ());
		}
	}

	public void OpenPausePanel ()
	{


		if (DataManager.instance.CurrentUiState == UiState.Game) {
			DataManager.instance.CurrentUiState = UiState.Pause;
			Time.timeScale = 0;

			damageTxt.text = "Damage: " + DataManager.instance.oPlayer.Character.Damage;
			maxDamageTxt.text = "Max Damage: " + DataManager.instance.oPlayer.Character.MaxDamage;
			PausePanel.gameObject.SetActive (true);
		} else if (DataManager.instance.CurrentUiState == UiState.Pause) {
			DataManager.instance.CurrentUiState = UiState.Game;
			Time.timeScale = 1;
			PausePanel.gameObject.SetActive (false);
		}



	
	}

	public void ConfirmReturn ()
	{
		//		////Debug.Log("Param : " +method.ToString());
		AskPanel.gameObject.SetActive (true);
		Text AskText = AskPanel.Find ("AskText").GetComponent<Text> ();
		AskText.text = "Are you sure you want to return?";
		Button yesBtn = AskPanel.Find ("Yes").GetComponent<Button> ();
		Button noBtn = AskPanel.Find ("No").GetComponent<Button> ();
		//		Button okBtn = AskPanel.Find("Ok").GetComponent<Button>();
		yesBtn.onClick.RemoveAllListeners ();
		noBtn.onClick.RemoveAllListeners ();
		//		yesBtn.onClick.RemoveAllListeners();
		AskPanel.gameObject.SetActive (true);
		//		yesBtn.onClick.AddListener(method);
		noBtn.onClick.AddListener (() => {
			AskPanel.gameObject.SetActive (false);
		});
		yesBtn.onClick.AddListener (() => {
			Time.timeScale = 1;

			DataManager.instance.StartCoroutine (DataManager.instance.LoadMenuLevel (false));
//				AskPanel.gameObject.SetActive(false);
//				yesBtn.gameObject.SetActive(true);
//				noBtn.gameObject.SetActive(true);

		});


	}




	//	public void LoadMainMenu
	public void ShowTelekinessisBtn (bool isOn)
	{
		TelekinessisGoParent.gameObject.SetActive (isOn);
	}

	public void ConfirmRestore (string message, ItemBoostType WhatToRestore)
	{
		//		////Debug.Log("Param : " +method.ToString());
		AskPanel.gameObject.SetActive (true);
		Text AskText = AskPanel.Find ("AskText").GetComponent<Text> ();
		AskText.text = message;
		Button yesBtn = AskPanel.Find ("Yes").GetComponent<Button> ();
		Button noBtn = AskPanel.Find ("No").GetComponent<Button> ();
//		Button okBtn = AskPanel.Find("Ok").GetComponent<Button>();
		yesBtn.onClick.RemoveAllListeners ();
		noBtn.onClick.RemoveAllListeners ();
		//		yesBtn.onClick.RemoveAllListeners();
		AskPanel.gameObject.SetActive (true);
		//		yesBtn.onClick.AddListener(method);
		noBtn.onClick.AddListener (() => {
			AskPanel.gameObject.SetActive (false);
		});
		yesBtn.onClick.AddListener (() => {
			////Debug.Log("1");
			if (DataManager.instance.oPlayer.Coins > 100) {
				////Debug.Log("2");
				AskPanel.gameObject.SetActive (false);
				if (WhatToRestore == ItemBoostType.Health) {
					oPlayer.RestoreHealth (DataManager.instance.oPlayer);
					////Debug.Log("3");
				} else if (WhatToRestore == ItemBoostType.KiLevel) {
					oPlayer.RestoreKi (DataManager.instance.oPlayer);
				}
				yesBtn.gameObject.SetActive (true);
				noBtn.gameObject.SetActive (true);
			} else {
				AskText.text = "You need at least 100 Coins!";
			}

		});


	}

	void AssignRestoreBtns ()
	{
		btnRestoreHp.onClick.RemoveAllListeners ();
		btnRestoreKi.onClick.RemoveAllListeners ();
		btnRestoreHp.onClick.AddListener (() => {
			ConfirmRestore ("Are you sure you want to restore 25% of Health for 100 coins?", ItemBoostType.Health);
		});
		btnRestoreKi.onClick.AddListener (() => {
			ConfirmRestore ("Are you sure you want to restore 25% of Power for 100 coins?", ItemBoostType.KiLevel);
		});
	}

}
