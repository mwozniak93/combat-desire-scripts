﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterContainer : MonoBehaviour
{
	public CharacterModel oCharacter;
	SpriteRenderer ctSpriteRenderer;
	Color ctCtartColor;
	bool loaded = false;

	void Awake ()
	{
		oCharacter.StartMaxHealth = oCharacter.MaxHealth;
		oCharacter.StartMaxBaseKiLvl = oCharacter.MaxKiLevelBase;
		oCharacter.StartExp = oCharacter.Exp;
//		gameObject.SetActive (false);

	}

	void Start ()
	{
		ctSpriteRenderer = GetComponent<SpriteRenderer> ();
		ctCtartColor = ctSpriteRenderer.color;
		loaded = true;
	}

	void OnEnable ()
	{
//		oCharacter.MaxKiLevelBase = oCharacter.MaxKiLevel;
		if (ctSpriteRenderer) {
			ctSpriteRenderer.color = ctCtartColor;
		}
		oCharacter.MaxKiLevel = oCharacter.MaxKiLevelBase;
		oCharacter.KiLevel = oCharacter.MaxKiLevel;
		oCharacter.Health = oCharacter.MaxHealth;
		oCharacter.IsDead = false;
		oCharacter.States = States.Idle;
//		if (loaded) {
//			gameObject.SetActive (false);
//		}
	}


}
