﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;


public class GoogleApi : MonoBehaviour {
	public static GoogleApi Instance;
	// Use this for initialization
	void Awake () {
		if (Instance==null) {
			Instance = this;
		} else {
			Destroy (this);
		}
		DontDestroyOnLoad (gameObject);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
			.EnableSavedGames()

			.Build();

		PlayGamesPlatform.InitializeInstance(config);

		StartCoroutine (checkInternetConnection ((isConnected) => {
			if (isConnected) {
				if (!Social.localUser.authenticated) {
					Social.localUser.Authenticate((bool success) => {
						if (success) {
							MenuUi.instance.ShowLeaderboardButton(true);
						}
					});
				}
				else {
					MenuUi.instance.ShowLeaderboardButton(true);
				}
			}
		
		}));

	}

	IEnumerator checkInternetConnection(Action<bool> action){
		WWW www = new WWW("http://google.com");
		yield return www;
		if (www.error != null) {
			action (false);
		} else {
			action (true);
		}
	} 


	public  void Login(){
		MenuUi.instance.ShowBtnLoginGoogle (false);
		StartCoroutine(checkInternetConnection((isConnected)=>{
			if (isConnected) {
				if (Social.localUser.authenticated) {
					MenuUi.instance.ShowLeaderboardButton(true);
				}else{

					Social.localUser.Authenticate((bool success) => {
						if (success) {
							//Debug.Log("You've successfully logged in"  + Social.localUser.userName);
							MenuUi.instance.ShowLeaderboardButton(true);
						} else {
							//Debug.Log("Login failed for some reason");
							MenuUi.instance.ShowInfoMessage ("Login failed");
							MenuUi.instance.ShowBtnLoginGoogle (true);
						}
//						MenuUi.instance.ShowLoadingBox(false);

					});
				}
			}
			else{
				MenuUi.instance.ShowInfoMessage ("NO INTERNET CONNECTION");

			}
		}));

//		//Debug.Log ("Application.internetReachability ; " + Application.internetReachability);
//		if (!Application.internetReachability) {
//			return false;
//		}
//		if (Social.localUser.authenticated) {
//			return true;
//		}
//		MenuUi.instance.ShowLoadingBox(true);
//
//		//Debug.Log ("Returning result : " + result);
//		return result;


	}
	public void ShowRanking(){
//		if (!Login()) {
//			MenuUi.instance.ShowInfoMessage ("YOU MUST LOGIN TO YOUR GOOGLE ACCOUNT IN ORDER TO SEE LEADERBOARD");
//			return;
//		}
		MenuUi.instance.ShowLoadingBox(true);
//		Social.ShowLeaderboardUI ();
		PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkIhMnDsKkfEAIQAA",(UIStatus val)=>{
			//Debug.Log("VAL : "  + val);
			MenuUi.instance.ShowLoadingBox(false);
		});
	}

	public void PostScore(long score){
		
		StartCoroutine (checkInternetConnection ((isConnected) => {
			if (isConnected) {
				//Debug.Log("IS Connected and POSTAING SCORE");
				Social.LoadScores ("CgkIhMnDsKkfEAIQAA", ((UnityEngine.SocialPlatforms.IScore[] obj) => {
					//Debug.Log ("Leadrboard after POST val : " + obj [0].value);
					Social.ReportScore (score, "CgkIhMnDsKkfEAIQAA", (bool success) => {
						// handle success or failure
						//Debug.Log("Succesfully POSTED score ? :"  +success);
					});
				
				}));

			}
		}));

}
}
