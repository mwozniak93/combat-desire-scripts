﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class WaveEnemiesCounter
{
	public EnemyType EnemyType;
	public int Count;
}

public class EffectsPoolManager : MonoBehaviour
{
	public static EffectsPoolManager instance;
	[HideInInspector]
	public GameObject[] PunchExplosions;
	public List<GameObject> Explosions;
	[HideInInspector]
	public List<Bullet> Bullets;
	public List<GameObject> Effects;
	public List<CharacterContainer> Enemies;
	public List<ItemContainer> Items;
	GameObject EffectsParent;
	[SerializeField]
	List<WaveEnemiesCounter> EnemiesPerStage = new List<WaveEnemiesCounter> ();
	//	float testTimer=0;
	// Use this for initialization
	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (instance);
		}
	}

	int CountEnemyTypesInList (Wave wave, EnemyType enemyType)
	{
		int counter = 0;
		foreach (var item in wave.EnemyTypes) {
			if (item == enemyType) {
				counter += 1;
			}
		}
		return counter;

	}

	void CountEnemiesInStage ()
	{

		foreach (EnemyType oEnemyType in Enum.GetValues(typeof(EnemyType))) {
			EnemiesPerStage.Add (new WaveEnemiesCounter (){ Count = 0, EnemyType = oEnemyType });
		}

		foreach (var oWave in DataManager.instance.oPlayer.CurrentStage.Waves) {
			foreach (EnemyType oEnemyType in Enum.GetValues(typeof(EnemyType))) {
				int specificEnemyOccurances = CountEnemyTypesInList (oWave, oEnemyType);
				AddEnemyType (oEnemyType, specificEnemyOccurances + 1);
			}
		}

	}


	bool AddEnemyType (EnemyType enemyType, int newCount)
	{
		foreach (var item in EnemiesPerStage) {
			if (item.EnemyType == enemyType && newCount > item.Count) {
				item.Count = newCount;
				return true;
			}
		}
		return false;
	}

	public void InitPrefabs (CharacterModel character)
	{
		CountEnemiesInStage ();

		Bullets = new List<Bullet> ();
		Explosions = new List<GameObject> ();
		Items = new List<ItemContainer> ();
		EffectsParent = new GameObject ("EffectsParent");
		AddNewItems (2, "Effects/Items/Hamburger", ItemType.Hamburger);
		AddNewItems (2, "Effects/Items/Coin", ItemType.Coin);
		AddNewItems (2, "Effects/Items/Mission", ItemType.Mission);

		foreach (var item in DataManager.instance.oPlayer.Shards) {
			if (item.Id != 0) {
				AddNewItems (1, "Shards/" + item.ShardType.ToString (), item.ShardType);
			}
		}
		AddNewEffects (4, "Effects/Effects/Vanish", EffectType.Vanish);
		AddNewEffects (4, "Effects/Effects/EnergyGather", EffectType.EnergyGather);
		AddNewEffects (2, "Effects/Effects/PowerBeyondAura", EffectType.PowerBeyondAura);
		AddNewEffects (2, "Effects/Effects/PowerBeyondExplosion", EffectType.PowerBeyondExplosion);
		AddNewEffects (2, "Effects/Effects/KiLevelGain", EffectType.KiLevelGain);
		AddNewEffects (2, "Effects/Effects/SuperPunch", EffectType.SuperPunch);
		AddNewExplosions (5, "Effects/Explosions/SmokeSmall", ExplosionType.SmokeSmall);
		AddNewExplosions (5, "Effects/Explosions/SmokeBig", ExplosionType.SmokeBig);
		AddNewExplosions (3, "Effects/Explosions/Hit", ExplosionType.Hit);
		AddNewBullets (8, "Effects/Bullets/Bone", BulletType.Bone);
		AddNewBullets (8, "Effects/Bullets/EnergyShoot2", BulletType.EnergyShoot2);
		AddNewBullets (7, "Effects/Bullets/NatureShield", BulletType.NatureShield);
		AddNewBullets (7, "Effects/Bullets/ExplosionWaveRed", BulletType.ExplosionWaveRed);

		foreach (var item in EnemiesPerStage) {
			StartCoroutine (AddNewEnemies (item.Count, "Enemies/" + item.EnemyType.ToString (), item.EnemyType));
		}
		StartCoroutine (AddNewEnemies (2, "Enemies/" + EnemyType.SlimeYellowBoss.ToString (), EnemyType.SlimeYellowBoss));
		StartCoroutine (AddNewEnemies (2, "Enemies/" + EnemyType.GoblinBlueBoss.ToString (), EnemyType.GoblinBlueBoss));
		StartCoroutine (AddNewEnemies (2, "Enemies/" + EnemyType.SkeletonWarriorRedBoss.ToString (), EnemyType.SkeletonWarriorRedBoss));
		foreach (var item in character.PlayerSkillsList) {
			
			if (item.oSkillType != SkillType.Transformation) {
				
//				//Debug.Log ("Adding new bullets for player : " + item.BulletType);
				AddNewBullets (4, "Effects/Bullets/" + item.BulletType.ToString (), item.BulletType);
			}
		}
	}

	GameObject[] InitNewArrayEffects (int count, string prefabPath)
	{
		GameObject[] arr = new GameObject[count];
		for (int i = 0; i < count; i++) {
			arr [i] = (GameObject)Instantiate (Resources.Load (prefabPath));
			arr [i].SetActive (false);
		}
		return arr;
	}

	public CharacterContainer GetEnemy (EnemyType itemType)
	{
		foreach (CharacterContainer item in Enemies) {
			////Debug.Log("Enemies ? : " +item.gameObject.name);
			if (!item.gameObject.activeInHierarchy && item.gameObject.name == itemType.ToString () && item.oCharacter.IsDead) {
				//				////Debug.Log("Bullet got after : " + System.Math.Round((decimal)testTimer,24));
				item.gameObject.SetActive (true);
				item.transform.parent.gameObject.SetActive (true);
				return item;
			}
		}
		return null;
	}

	IEnumerator AddNewEnemies (int count, string prefabPath, EnemyType itemType)
	{

		for (int i = 0; i < count; i++) {
			GameObject item = (GameObject)Instantiate (Resources.Load (prefabPath));
			GameObject goChild = item.transform.Find (itemType.ToString ()).gameObject;
			//Debug.Log ("item ; " + item.activeInHierarchy);
			//Debug.Log ("goChild  ; " + goChild.activeInHierarchy);
//			goChild.SetActive (true);
			item.SetActive (true);

			//			Bullet bull=bullet.GetComponent<Bullet>();
			CharacterContainer itemContainer = goChild.GetComponent<CharacterContainer> ();
			itemContainer.oCharacter.IsDead = true;
			Enemies.Add (itemContainer);
			item.name = itemType.ToString ();
			goChild.name = itemType.ToString ();
//			item.GetComponentInChildren<Spawn> ().Init ();
			goChild.SetActive (false);
//			
			item.SetActive (false);
			item.transform.SetParent (EffectsParent.transform);
			yield return new WaitForSeconds (0);
//			yield return new WaitForEndOfFrame ();
//			goChild.SetActive (false);
		}
	}



	void AddNewItems (int count, string prefabPath, ItemType itemType)
	{

		for (int i = 0; i < count; i++) {
			GameObject item = (GameObject)Instantiate (Resources.Load (prefabPath));
			//			Bullet bull=bullet.GetComponent<Bullet>();
			ItemContainer itemContainer = item.GetComponent<ItemContainer> ();
			Items.Add (itemContainer);
			item.name = itemType.ToString ();
			item.SetActive (false);
			item.transform.SetParent (EffectsParent.transform);
		}
	}

	void AddNewEffects (int count, string prefabPath, EffectType effectType)
	{

		for (int i = 0; i < count; i++) {
			GameObject effect = (GameObject)Instantiate (Resources.Load (prefabPath));
//			Bullet bull=bullet.GetComponent<Bullet>();
			Effects.Add (effect);
			effect.name = effectType.ToString ();
			effect.SetActive (false);
			effect.transform.SetParent (EffectsParent.transform);
		}
	}

	void AddNewExplosions (int count, string prefabPath, ExplosionType exploType)
	{

		for (int i = 0; i < count; i++) {
			GameObject explo = (GameObject)Instantiate (Resources.Load (prefabPath));
			//			Bullet bull=bullet.GetComponent<Bullet>();
			Explosions.Add (explo);
			explo.name = exploType.ToString ();
			explo.SetActive (false);
			explo.transform.SetParent (EffectsParent.transform);
		}
	}

	public GameObject GetExplosion (ExplosionType explosionType)
	{
		foreach (GameObject item in Explosions) {
			//			////Debug.Log("Bullet ? : " +item.gameObject.name);
			if (!item.activeInHierarchy && item.gameObject.name == explosionType.ToString ()) {
				//				////Debug.Log("Bullet got after : " + System.Math.Round((decimal)testTimer,24));
				return item;
			}
		}
		return null;
	}

	void AddNewBullets (int count, string prefabPath, BulletType bulletType)
	{
	
		for (int i = 0; i < count; i++) {
			GameObject bullet = (GameObject)Instantiate (Resources.Load (prefabPath));
			Bullet bull = bullet.GetComponent<Bullet> ();
			Bullets.Add (bull);
			bull.name = bulletType.ToString ();
			bull.gameObject.SetActive (false);
			bull.transform.SetParent (EffectsParent.transform);
		}
	}

	public Bullet GetBullet (BulletType bulletType)
	{
//		testTimer=0.00f;
//		////Debug.Log("Getting bullet type : " +bulletType + "  Bullets  : " +Bullets.Count);
		foreach (Bullet item in Bullets) {
//			////Debug.Log("Bullet ? : " +item.gameObject.name);
			if (!item.gameObject.activeInHierarchy && item.gameObject.name == bulletType.ToString ()) {
//				////Debug.Log("Bullet got after : " + System.Math.Round((decimal)testTimer,24));
				return item;
			}
		}
		return null;
	}

	public GameObject GetEffect (EffectType effectType)
	{
		foreach (GameObject item in Effects) {
			//			////Debug.Log("Bullet ? : " +item.gameObject.name);
			if (!item.activeInHierarchy && item.gameObject.name == effectType.ToString ()) {
				//				////Debug.Log("Bullet got after : " + System.Math.Round((decimal)testTimer,24));
				return item;
			}
		}
		return null;
	}

	public ItemContainer GetItem (ItemType itemType)
	{
		foreach (ItemContainer item in Items) {
			//			////Debug.Log("Bullet ? : " +item.gameObject.name);
			if (!item.gameObject.activeInHierarchy && item.gameObject.name == itemType.ToString ()) {
				//				////Debug.Log("Bullet got after : " + System.Math.Round((decimal)testTimer,24));
				return item;
			}
		}
		return null;
	}

	public GameObject GetPunchExplosion ()
	{
		foreach (var item in PunchExplosions) {
			if (!item.activeInHierarchy) {
				return item;
			}
		}
		return null;
	}

	void Update ()
	{
//		////Debug.Log("TEsttimer : " +testTimer);
//		testTimer+=Time.deltaTime;
	}
}
