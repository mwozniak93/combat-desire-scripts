﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour
{
	public static WaveManager instance;




	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
	}
	// Use this for initialization
	void Start ()
	{
		DataManager.instance.oPlayer.CurrentStage.SpawnPoints = GetSpawnPointsFromMap ();
		StartCoroutine (CreateWave ());
		DataManager.instance.oPlayer.Character.eDie += EndGame;
	}

	void OnDestroy ()
	{
		DataManager.instance.oPlayer.Character.eDie -= EndGame;
	}

	void EndGame ()
	{
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.OnMissionComplete (DataManager.instance.oPlayer.CurrentStage.MissionWaves);
	}


	void GetBonusShard (int waveCounter)
	{
		if (waveCounter % DataManager.instance.oPlayer.CurrentStage.WaveModRequiredForBoss == 0) {

			CharacterContainer newEnemyBoss = EffectsPoolManager.instance.GetEnemy (DataManager.instance.oPlayer.CurrentStage.EnemyBoss);
			if (newEnemyBoss) {
				DataManager.instance.oPlayer.CurrentStage.CurrentWave.InitEnemy (newEnemyBoss.oCharacter, DataManager.instance.oPlayer.CurrentStage);
				newEnemyBoss.transform.position = DataManager.instance.oPlayer.CurrentStage.GetRandomSpawnPos ();
			}
		}

	}

	public IEnumerator CreateWave ()
	{
		if (DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount == 0) {
			yield return new WaitForSeconds (1f);
		}
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount += 1;
		GetBonusShard (DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount);
		DataManager.instance.oPlayer.CurrentStage.MissionWaves.OnMissionUpdate ();

		int indexOfCurrentWave = DataManager.instance.oPlayer.CurrentStage.Waves.IndexOf (DataManager.instance.oPlayer.CurrentStage.CurrentWave);

		if (DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount == 1) {
			DataManager.instance.oPlayer.CurrentStage.PowerLvlMultipler = DataManager.instance.oPlayer.CurrentStage.StartWaveMultipler; 
		} else {
			DataManager.instance.oPlayer.CurrentStage.PowerLvlMultipler += DataManager.instance.oPlayer.CurrentStage.WaveMultipler;
		}

		int randomWave = Random.Range (0, DataManager.instance.oPlayer.CurrentStage.Waves.Count);
		DataManager.instance.oPlayer.CurrentStage.CurrentWave = DataManager.instance.oPlayer.CurrentStage.Waves [randomWave];
		DataManager.instance.oPlayer.CurrentStage.CurrentWave.EnemiesLeft = DataManager.instance.oPlayer.CurrentStage.CurrentWave.EnemyTypes.Count;
		int enemyCounter = 0;
		foreach (var item in  DataManager.instance.oPlayer.CurrentStage.CurrentWave.EnemyTypes) {
			yield return new WaitForSeconds (0.5f);
			enemyCounter += 1;
			EnemyType currentEnemyType = item;
			CharacterContainer newEnemy = EffectsPoolManager.instance.GetEnemy (currentEnemyType);
			newEnemy.oCharacter.eDie -= TryNextWave;
			newEnemy.oCharacter.eDie += TryNextWave;
			DataManager.instance.oPlayer.CurrentStage.CurrentWave.InitEnemy (newEnemy.oCharacter, DataManager.instance.oPlayer.CurrentStage);
			newEnemy.transform.position = DataManager.instance.oPlayer.CurrentStage.GetRandomSpawnPos ();
//			if (DataManager.instance.oPlayer.CurrentStage.MissionWaves.CurrentAmount>1) {
			yield return new WaitForEndOfFrame ();
//			//Debug.Log ("PlayerSingleton.instance.transform.position.magnitude : " + Vector2.Distance(PlayerSingleton.instance.transform.position,newEnemy.transform.position) + " for : " + newEnemy.name );
			if (PlayerSingleton.instance != null) {
				
			
				if (Vector2.Distance (PlayerSingleton.instance.transform.position, newEnemy.transform.position) > 2.3f) {
					newEnemy.gameObject.SetActive (false);	//TODO
				}
			}
		}

	}

	Vector2[] GetSpawnPointsFromMap ()
	{
		GameObject[] goSpawnPoints = GameObject.FindGameObjectsWithTag ("SpawnPh");
		Vector2[] spawnPoints = new Vector2[goSpawnPoints.Length];
		int counter = 0;
		foreach (var item in goSpawnPoints) {
			spawnPoints [counter] = item.transform.position;
			counter += 1;
		}
		return spawnPoints;

	}


	void TryNextWave ()
	{
//		Wave currentWave = DataManager.instance.oPlayer.CurrentStage.CurrentWave;
		DataManager.instance.oPlayer.CurrentStage.CurrentWave.EnemiesLeft -= 1;
		if (DataManager.instance.oPlayer.CurrentStage.CurrentWave.EnemiesLeft <= 0) {
//			//Debug.Log ("CREATING NEW WAVE!!!");
			StartCoroutine (CreateWave ());
//			currentWave.OnWaveComplete ();
		}
	}



}
