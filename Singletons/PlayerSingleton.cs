﻿using UnityEngine;
using System.Collections;

public class PlayerSingleton : MonoBehaviour
{
	public static PlayerSingleton instance;
	// Use this for initialization
	void Awake ()
	{
		//Debug.Log("name SINGLETON!?: " +name);
//		//Debug.Log("name : " +name);
		if (instance == null) {
			instance = this;
		} else {
	
//			//Debug.Log("name ELSE: " +name);
			Destroy (transform.parent.gameObject);
		}
		DontDestroyOnLoad (transform.parent.gameObject);
	}
	

}
