﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AudioType
{
	None,
	Hit,
	KiShoot,
	PowerUp,
	Teleport
}

public class AudioManager : MonoBehaviour
{


	public static AudioManager Instance;
	public List<AudioClip> Clips;
	// Use this for initialization
	public AudioSource ctAutioSource;

	void AddNewClips (int count, string prefabPath, AudioType audioType)
	{

		for (int i = 0; i < count; i++) {
			AudioClip effect = (AudioClip)Instantiate (Resources.Load (prefabPath));
			//			Bullet bull=bullet.GetComponent<Bullet>();
			Clips.Add (effect);
			effect.name = audioType.ToString ();
//			effect.
//			effect.SetActive(false);

//			effect.transform.SetParent(EffectsParent.transform);
		}
	}

	void Awake ()
	{
		ctAutioSource = GetComponent<AudioSource> ();
		if (Instance == null) {
			Instance = this;
		} else {
		
			Instance = null;
		}
		AddNewClips (10, "Sounds/" + AudioType.Hit.ToString (), AudioType.Hit);	
		AddNewClips (10, "Sounds/" + AudioType.Teleport.ToString (), AudioType.Teleport);
		AddNewClips (10, "Sounds/" + AudioType.PowerUp.ToString (), AudioType.PowerUp);
		AddNewClips (10, "Sounds/" + AudioType.KiShoot.ToString (), AudioType.KiShoot);
	}

	public AudioClip GetAudioclip (AudioType audioType)
	{
		foreach (AudioClip item in Clips) {
			//			////Debug.Log("Bullet ? : " +item.gameObject.name);
			if (item.name == audioType.ToString ()) {
				//				////Debug.Log("Bullet got after : " + System.Math.Round((decimal)testTimer,24));
				return item;
			}
		}
		return null;
	}

	public void Play (AudioType audioType)
	{
		ctAutioSource.clip = GetAudioclip (audioType);
		ctAutioSource.volume = 0.41f;
		if (audioType == AudioType.Hit) {
			ctAutioSource.volume = 0.3f;
		}

		if (audioType == AudioType.PowerUp) {
			ctAutioSource.loop = true;
			ctAutioSource.volume = 1f;
			ctAutioSource.clip = GetAudioclip (audioType);
			ctAutioSource.Play ();

		} else {
			ctAutioSource.PlayOneShot (GetAudioclip (audioType));
			ctAutioSource.loop = false;
		}
		if (audioType == AudioType.KiShoot) {
			ctAutioSource.volume = 0.25f;	
		}
//		Debug.Log ("PLAY" + audioType);
	}

	public void StopPlaying ()
	{
		ctAutioSource.Stop ();
	
	}
}
