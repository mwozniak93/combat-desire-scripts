﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Linq;


public class DataManager : MonoBehaviour
{
	public static DataManager instance;
	[SerializeField]
	public PlayerModel oPlayer;
	GameObject PlayerGo;
	public UiState CurrentUiState;
	//	[SerializeField]
	//	CharacterModel oCharacterPlayer;
	CharacterModel oEnemy;
	float testValue;

	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
//		ctGlobalUiAnimator
		foreach (var oMap in oPlayer.Maps) {
			foreach (var oStage in oMap.Stages) {
				oStage.InitWavesBonusses ();
			}
		}
		Application.targetFrameRate = 30;

		QualitySettings.vSyncCount = 0; 

		QualitySettings.antiAliasing = 0;

		SaveData.Load ();
		if (SaveData.PlayersSavedData.Count > 0) {
			LoadPlayerConfig ();
		} else {
			var defaultSkill = (from s in oPlayer.Skills
			                    where s.Title == "Energy Blast"
			                    select s).ToList ().First ();
			var defaultSkill2 = (from s in oPlayer.Skills
			                     where s.Title == "Electric Wave"
			                     select s).ToList ().First ();
			oPlayer.Character.PlayerSkillsList.Add (defaultSkill);
			oPlayer.Character.PlayerSkillsList.Add (defaultSkill2);
			oPlayer.Character.oCurrentSkill = defaultSkill;
			oPlayer.Character.StartSpeed = oPlayer.Character.Speed;
//			oPlayer.SelectedShard = oPlayer.Shards [0];

		}
		oPlayer.CurrentMap = oPlayer.Maps [0];
		oPlayer.CurrentStage = oPlayer.CurrentMap.Stages [0];
		DontDestroyOnLoad (gameObject);
	}

	public void TestAddPowerLevel ()
	{
		oPlayer.Character.MaxKiLevelBase += 1000;

	}

	public void TestAddCoins ()
	{
		oPlayer.Coins += 1000;

	}

	void LoadPlayerConfig ()
	{
		
		oPlayer.Character.oCurrentSkill = SaveData.PlayersSavedData [0].Character.oCurrentSkill;
		oPlayer.SelectedShard = SaveData.PlayersSavedData [0].SelectedShard;
		oPlayer.CurrentStage = SaveData.PlayersSavedData [0].CurrentStage;
		oPlayer.CurrentMap = SaveData.PlayersSavedData [0].CurrentMap;


		foreach (var shardBefore in oPlayer.Shards) {
			foreach (var shardAfter in SaveData.PlayersSavedData[0].Shards) {
				if (shardBefore.Id == shardAfter.Id) {
					shardBefore.CurrentAmount = shardAfter.CurrentAmount;
				}
			}
		}

		foreach (var oSkillBefore in oPlayer.Skills) {
			foreach (var oSkillAfter in SaveData.PlayersSavedData[0].Skills) {
				if (oSkillAfter.Id == oSkillBefore.Id) {
					oSkillBefore.IsBought = oSkillAfter.IsBought;
					oSkillBefore.Price = oSkillAfter.Price;
					oSkillBefore.Level = oSkillAfter.Level;
					oSkillBefore.KiNeededInPercentage = oSkillAfter.KiNeededInPercentage;
					oSkillBefore.DamageMultipler = oSkillAfter.DamageMultipler;
					oSkillBefore.Cooldown = oSkillAfter.Cooldown;

				}
			}
		}
		foreach (MapModel oMapBefore in oPlayer.Maps) {
			foreach (MapModel oMapAfter in SaveData.PlayersSavedData[0].Maps) {
				if (oMapBefore.Id == oMapAfter.Id) {
//					oMapBefore.IsFinished =  oMapAfter.IsFinished;
					oMapBefore.IsLocked = oMapAfter.IsLocked;

					foreach (StageModel oStagelAfter in oMapAfter.Stages) {
						foreach (StageModel oStageBefore in oMapBefore.Stages) {
							if (oStageBefore.Id == oStagelAfter.Id) {
								oStageBefore.IsFinished = oStagelAfter.IsFinished;
								oStageBefore.IsLocked = oStagelAfter.IsLocked;
								oStageBefore.MissionWaves.CurrentAmount = oStagelAfter.MissionWaves.CurrentAmount;
								//Debug.Log ("oStageBefore.Mission.CurrentHighscoreAmount  : " + oStageBefore.MissionWaves.CurrentHighscoreAmount);
								oStageBefore.MissionWaves.CurrentHighscoreAmount = oStagelAfter.MissionWaves.CurrentHighscoreAmount;
								foreach (var oWaveBonusAfter in oStagelAfter.WavesBonuses) {
									foreach (var oWaveBonusBefore in oStageBefore.WavesBonuses) {
										if (oWaveBonusAfter.Id == oWaveBonusBefore.Id) {
											oWaveBonusBefore.IsUnlcoked = oWaveBonusAfter.IsUnlcoked;
											oWaveBonusBefore.IsCompleted = oWaveBonusAfter.IsCompleted;
										}
									}
								}

							}
						}
					}


				}


			}

		}
		oPlayer.Coins = SaveData.PlayersSavedData [0].Coins;
		oPlayer.StartPosOnMap = SaveData.PlayersSavedData [0].StartPosOnMap;
		oPlayer.Character = SaveData.PlayersSavedData [0].Character;
		oPlayer.Character.Speed = oPlayer.Character.StartSpeed;
		oPlayer.Character.MaxHealth = oPlayer.Character.StartMaxHealth;
		oPlayer.Character.StartMaxBaseKiLvl = oPlayer.Character.StartMaxBaseKiLvl;
//		//Debug.Log ("oPlayer..MAPS :" + oPlayer.Maps.Count + " Datamanger : " + DataManager.instance.oPlayer.Maps.Count);

	}


	void Start ()
	{
		GlobalUiManager.instance.StartCoroutine (GlobalUiManager.instance.FadeIn ());

	}

	public void CallLoadMenuLevel ()
	{
		StartCoroutine (LoadLevel (oPlayer.CurrentStage, false));
	}

	public IEnumerator  LoadMenuLevel (bool shouldRestart)
	{
		oPlayer.FinishStageWin (oPlayer.Character.MaxKiLevelBase, 0);
//		SaveData.Save();
		GlobalUiManager.instance.StartCoroutine (GlobalUiManager.instance.FadeOut ());
		yield return new WaitForSeconds (1.5f);

		AsyncOperation async = SceneManager.LoadSceneAsync ("Scene0");
		Destroy (gameObject);
		Destroy (PlayerSingleton.instance.transform.parent.gameObject);
		Destroy (GoogleApi.Instance.gameObject);
	
		yield return async;
		if (shouldRestart) {
			//Debug.Log ("RESTART");
			StartCoroutine (LoadLevel (oPlayer.CurrentStage, true));
			//			Destroy (gameObject);
//					Destroy (PlayerSingleton.instance.transform.parent.gameObject);
		}
		GlobalUiManager.instance.StartCoroutine (GlobalUiManager.instance.FadeIn ());
//		yield return new WaitForSeconds (0.5f);
//		Destroy (GlobalUiManager.instance.transform.parent.gameObject);
	}

	//	public void ChangeCurrentStage
	public void SelectNextMap ()
	{
		int indexOfCurrentMap = oPlayer.Maps.IndexOf (oPlayer.CurrentMap);
		int indexOfCurrentStage = oPlayer.CurrentMap.Stages.IndexOf (oPlayer.CurrentStage);
//		//Debug.Log ("index :" + indexOfCurrentMap);
		if (indexOfCurrentMap < 0 || indexOfCurrentMap >= oPlayer.Maps.Count - 1) {
			oPlayer.CurrentMap = oPlayer.Maps [0];
		} else {
			oPlayer.CurrentMap = oPlayer.Maps [indexOfCurrentMap + 1];
		}
		oPlayer.CurrentStage = oPlayer.CurrentMap.Stages [indexOfCurrentStage];
		MenuUi.instance.GenerateCurrentMap ();
	}

	public void SelectPreviousMap ()
	{
		int indexOfCurrentMap = oPlayer.Maps.IndexOf (oPlayer.CurrentMap);
		int indexOfCurrentStage = oPlayer.CurrentMap.Stages.IndexOf (oPlayer.CurrentStage);
		if (indexOfCurrentMap - 1 < 0 || indexOfCurrentMap - 1 >= oPlayer.Maps.Count - 1) {
			oPlayer.CurrentMap = oPlayer.Maps [oPlayer.Maps.Count - 1];
		} else {
			oPlayer.CurrentMap = oPlayer.Maps [indexOfCurrentMap - 1];
		}
		oPlayer.CurrentStage = oPlayer.CurrentMap.Stages [indexOfCurrentStage];

		MenuUi.instance.GenerateCurrentMap ();
	}




	public IEnumerator  LoadLevel (StageModel oStage, bool shouldRestart)
	{
		if (shouldRestart) {
//						Destroy (gameObject);
			Destroy (PlayerSingleton.instance.transform.parent.gameObject);
		}
		//Debug.Log ("LOading...");
		if (oPlayer.Character.oCurrentSkill != null || oPlayer.Character.PlayerSkillsList.Count != 0) {
			oPlayer.CurrentStage.MultiplyEnemies (1);
			oPlayer.CurrentStage.MissionWaves.CurrentHighscoreAmount = oPlayer.CurrentStage.MissionWaves.CurrentHighscoreAmount;
			oPlayer.CurrentStage.MissionWaves.CurrentAmount = 0;
			if (oPlayer.SelectedShard.Id != 0) {
//				oPlayer.SelectedShard.CurrentAmount -= 1;
			}
			foreach (var item in oPlayer.CurrentStage.Waves) {
				item.LvlMultipler = oPlayer.CurrentStage.StartWaveMultipler;
			}

			if (DataManager.instance.oPlayer.CurrentMap.IsLocked) {
				if (MenuUi.instance != null) {
					MenuUi.instance.ShowInfoMessage ("You must unlock this map first!");
					yield break;
				}
			}

			if (oStage.IsLocked) {
				if (MenuUi.instance != null) {
				
					MenuUi.instance.ShowInfoMessage ("You must unlock this difficulity level first!");
					yield break;
				}
			}

			GlobalUiManager.instance.StartCoroutine (GlobalUiManager.instance.FadeOut ());
			yield return new WaitForSeconds (1.5f);
			oPlayer.CurrentStage = oStage;
			oPlayer.CurrentStage.MissionWaves.CurrentAmount = 0;
			oPlayer.CurrentStage.KiLvlGained = 0;
			oPlayer.CurrentStage.Coins = 0;
//			oPlayer.CurrentStage.Waves = new List<Wave> (){new Wave(){Id=1, LvlMultipler=1 ,EnemyTypes = new List<EnemyType>(){EnemyType.GoblinBlue}}};
//
//			oPlayer.CurrentStage.Waves.FirstOrDefault
			////Debug.Log("oPlayer.CurrentStage ;"  +oPlayer.CurrentStage.MissionWaves.Story);
			oPlayer.Character.InitCharacter ();

			AsyncOperation async = SceneManager.LoadSceneAsync ("Scene1");
			yield return async;
			Instantiate (Resources.Load<GameObject> ("PoolManager"));
//			PlayerUI.instance.ctMapCanvasController.playerTransform = PlayerSingleton.instance.transform;

			InitPlayer ();
			EffectsPoolManager.instance.InitPrefabs (oPlayer.Character);
			PlayerUI.instance.InitHud ();
			PlayerSkillUi.Instance.InitHud ();
			PlayerSkillUi.Instance.AssignSelectedSkill (oPlayer.Character.oCurrentSkill);
			PlayerGo.GetComponent<PlayerSkillController> ().oPlayerModel = oPlayer.Character;
			PlayerGo.GetComponent<PlayerSkillController> ().ChangeSkill (oPlayer.Character.oCurrentSkill);

			//init hud
			GameObject map = (GameObject)Instantiate (Resources.Load ("Maps/Level" + oPlayer.CurrentMap.Id));
//			PlayerSingleton.instance.transform.position = map.transform.Find("Custom").gameObject.fin("PlayerPh").position;
			PlayerSingleton.instance.transform.position = GameObject.FindGameObjectWithTag ("PlayerPh").transform.position;

			foreach (var item in GameObject.FindGameObjectsWithTag("ShardPh")) {
				ShardRandomizer randomizedShard = oPlayer.CurrentStage.RandomizeShard ();
				if (randomizedShard != null) {
					//Debug.Log ("Shard Ranomzied : " + randomizedShard.ShardType + "  in postion : " + item.transform.position);
					GameObject goShard = Instantiate (Resources.Load<GameObject> ("Shards/" + randomizedShard.ShardType.ToString ()));
					goShard.transform.position = item.transform.position;
				}

			}
			oPlayer.Character.oCurrentSkill = oPlayer.Character.oCurrentSkill;
		}	
	}

	public void InitPlayer ()
	{
		PlayerGo = (GameObject)Instantiate (Resources.Load ("HeroParent"));
		PlayerGo = PlayerGo.transform.Find ("Hero").gameObject;
		PlayerGo.GetComponent<CharacterContainer> ().oCharacter = oPlayer.Character;
		GameObject playerGo = PlayerSingleton.instance.gameObject;
		playerGo.SetActive (true);
		GameObject buttons = GameObject.Find ("Buttons");
		////Debug.Log("BITTONS :" + buttons.name);
		GameObject attackBtn = buttons.transform.Find ("A").Find ("AttackBtn").gameObject;
		EventTrigger attackTrigger = attackBtn.GetComponent<EventTrigger> ();
		Punch ctPunch = playerGo.GetComponent<Punch> ();
		EventTrigger.Entry entryPunch = new EventTrigger.Entry ();
		entryPunch.eventID = EventTriggerType.PointerUp;
		entryPunch.callback.AddListener ((eventData) => {
			ctPunch.PunchOn ();
		});

		SuperPunch ctSuperPunch = playerGo.GetComponent<SuperPunch> ();
		EventTrigger.Entry entrySuperPunchPrepere = new EventTrigger.Entry ();
		EventTrigger.Entry entrySuperPunch = new EventTrigger.Entry ();
		entrySuperPunchPrepere.eventID = EventTriggerType.PointerDown;
		entrySuperPunchPrepere.callback.AddListener ((eventData) => {
			ctSuperPunch.SpeedAttackOnDown ();
		});
		entrySuperPunch.eventID = EventTriggerType.PointerUp;
		entrySuperPunch.callback.AddListener ((eventData) => {
			ctSuperPunch.SpeedAttackOnUp ();
		});

		attackTrigger.triggers.Add (entryPunch);
		attackTrigger.triggers.Add (entrySuperPunchPrepere);
		attackTrigger.triggers.Add (entrySuperPunch);


		if (oPlayer.Character.IsPassiveSearchedSkillPresent (SkillPassiveType.Teleportation)) {
			playerGo.GetComponent<Vanish> ().enabled = true;
		} else {
			Destroy (playerGo.GetComponent<Vanish> ());
		}
		GameObject goSenseButton = PlayerUI.instance.goSensePower;
		SensePowerLevel ctPowerSense = playerGo.GetComponent<SensePowerLevel> ();
		// -----------------------SensePower-------------------
		if (oPlayer.Character.IsPassiveSearchedSkillPresent (SkillPassiveType.SensePower)) {
			
			EventTrigger powerSenseTrigger = goSenseButton.GetComponent<EventTrigger> ();

			ctPowerSense.enabled = true;
			EventTrigger.Entry entryPowerSenseOff = new EventTrigger.Entry ();
			entryPowerSenseOff.eventID = EventTriggerType.PointerDown;
			entryPowerSenseOff.callback.AddListener ((eventData) => {
				ctPowerSense.SensePower (true);
			});
			EventTrigger.Entry entryPowerSenseOn = new EventTrigger.Entry ();
			entryPowerSenseOn.eventID = EventTriggerType.PointerUp;
			entryPowerSenseOn.callback.AddListener ((eventData) => {
				ctPowerSense.SensePower (false);
			});
			powerSenseTrigger.triggers.Add (entryPowerSenseOn);
			powerSenseTrigger.triggers.Add (entryPowerSenseOff);
			goSenseButton.SetActive (true);
		} else {
			ctPowerSense.enabled = false;
			goSenseButton.SetActive (false);
		}




		// -------------------DEFEND----------------------
		GameObject defendBtn = buttons.transform.Find ("X").Find ("DefendBtn").gameObject;
		EventTrigger defendTrigger = defendBtn.GetComponent<EventTrigger> ();
		Guard ctGuard = playerGo.GetComponent<Guard> ();
		EventTrigger.Entry entryGuardOff = new EventTrigger.Entry ();
		entryGuardOff.eventID = EventTriggerType.PointerDown;
		entryGuardOff.callback.AddListener ((eventData) => {
			ctGuard.GuardOn ();
		});
		EventTrigger.Entry entryGuardOn = new EventTrigger.Entry ();
		entryGuardOn.eventID = EventTriggerType.PointerUp;
		entryGuardOn.callback.AddListener ((eventData) => {
			ctGuard.GuardOff ();
		});

		if (oPlayer.Character.IsPassiveSearchedSkillPresent (SkillPassiveType.Teleportation)) {
			Vanish ctVanish = playerGo.GetComponent<Vanish> ();
			EventTrigger.Entry entryVanish = new EventTrigger.Entry ();
			entryVanish.eventID = EventTriggerType.PointerClick;
			entryVanish.callback.AddListener ((eventData) => {
				ctVanish.VanishOn ();
			});

			defendTrigger.triggers.Add (entryVanish);
		}
		defendTrigger.triggers.Add (entryGuardOn);
		defendTrigger.triggers.Add (entryGuardOff);
		//--------------------POWERUP-------------------------
		GameObject powerupBtn = buttons.transform.Find ("L").Find ("PowerupBtn").gameObject;
		EventTrigger powerupTrigger = powerupBtn.GetComponent<EventTrigger> ();
		Powerup ctPowerup = playerGo.GetComponent<Powerup> ();
		EventTrigger.Entry entryPowerupOff = new EventTrigger.Entry ();
		entryPowerupOff.eventID = EventTriggerType.PointerDown;
		entryPowerupOff.callback.AddListener ((eventData) => {
			ctPowerup.PowerupOn ();
		});
		EventTrigger.Entry entryPowerupOn = new EventTrigger.Entry ();
		entryPowerupOn.eventID = EventTriggerType.PointerUp;
		entryPowerupOn.callback.AddListener ((eventData) => {
			ctPowerup.PowerUpOff ();
		});

		powerupTrigger.triggers.Add (entryPowerupOn);
		powerupTrigger.triggers.Add (entryPowerupOff);
//		defendTrigger.triggers.Add(entryGuardOff);

		//------------

		// TELEKINESSIS 

		GameObject tkBtn = buttons.transform.Find ("Telekinessis").Find ("TkBtn").gameObject;
		EventTrigger tkTrigger = tkBtn.GetComponent<EventTrigger> ();
		Telekinessis ctTelekinessis = playerGo.GetComponent<Telekinessis> ();
		EventTrigger.Entry entryTelekinessisOn = new EventTrigger.Entry ();
		entryTelekinessisOn.eventID = EventTriggerType.PointerDown;
		entryTelekinessisOn.callback.AddListener ((eventData) => {
			ctTelekinessis.TelekinesisOnDown ();
		});
		EventTrigger.Entry entryTelekinessisOff = new EventTrigger.Entry ();
		entryTelekinessisOff.eventID = EventTriggerType.PointerUp;
		entryTelekinessisOff.callback.AddListener ((eventData) => {
			ctTelekinessis.StartCoroutine (ctTelekinessis.TelekinesisOnUp ());
		});

		tkTrigger.triggers.Add (entryTelekinessisOn);
		tkTrigger.triggers.Add (entryTelekinessisOff);

	}



}
