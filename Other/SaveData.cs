﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;


public class SaveData
{
	//	PlayerModel player;
	public static  List<PlayerModel> PlayersSavedData = new List<PlayerModel> ();

	public static  void Save ()
	{
		PlayersSavedData = new List<PlayerModel> ();
		PlayersSavedData.Add (DataManager.instance.oPlayer);
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/PlayerSave.gd");
		bf.Serialize (file, PlayersSavedData);
		file.Close ();
	}

	public static  void Load ()
	{
		if (File.Exists (Application.persistentDataPath + "/PlayerSave.gd")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/PlayerSave.gd", FileMode.Open);
			PlayersSavedData = (List<PlayerModel>)bf.Deserialize (file);

//			//Debug.Log("Load completed ? :"  +PlayersSavedData.Count);
			file.Close ();
		}
	}
}

