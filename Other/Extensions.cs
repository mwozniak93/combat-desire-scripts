﻿using System;
using UnityEngine;

public  static class  Extensions
{

	public static string ConvertNumber (this float num)
	{
		if (num >= 1000)
			return string.Concat (Math.Round (num / 1000, 1), "k");
		else if (num >= 1000000) {
			return string.Concat (Math.Round (num / 1000000, 1), "m");
				
		} else
			return Math.Round (num, 1).ToString ();
	}
}

