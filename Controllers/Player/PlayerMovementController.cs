﻿using UnityEngine;
using System.Collections;
using CnControls;

[RequireComponent (typeof(Movement))]
public class PlayerMovementController : MonoBehaviour
{
	//	[HideInInspector]
	//	public float speedX, speedY, lastSpeedX,lastSpeedY;
	CharacterModel oCharacter;
	Animator ctAnimator;
	//delegate ()
	//	public delegate void Move(float val);
	//event
	//	public  event Move eMove;
	//	enum Direction {Up= Vector2.left}
	//	GameManager oGameManager;
	public Vector2 Direction;
	Rigidbody2D ctRigdbody2D;
	Movement ctMovement;
	float xYDifference;

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		ctAnimator = GetComponent<Animator> ();
		ctRigdbody2D = GetComponent<Rigidbody2D> ();
		ctMovement = GetComponent<Movement> ();
	}


	void FixedUpdate ()
	{
		if (oCharacter.States != States.Attacking && oCharacter.States != States.Hurting && oCharacter.States != States.Guarding && oCharacter.States != States.Charging && oCharacter.States != States.SpeedAttack && oCharacter.States != States.SuperPunchPrepere) {
			oCharacter.SpeedX = CnInputManager.GetAxis ("Horizontal");
			oCharacter.SpeedY = CnInputManager.GetAxis ("Vertical");
		}

	}
		

}
