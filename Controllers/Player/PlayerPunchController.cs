﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent (typeof(Punch))]
public class PlayerPunchController : MonoBehaviour
{
	[SerializeField]
	EventTrigger PunchBtn;
	Punch ctPunch;
	// Use this for initialization
	void Start ()
	{
//		EventTrigger trigger = specialSkillButton.GetComponent<EventTrigger>();
//		trigger.triggers.Clear();
		ctPunch = GetComponent<Punch> ();
		EventTrigger.Entry entryPunch = new EventTrigger.Entry ();
	
		entryPunch.eventID = EventTriggerType.PointerUp;
		entryPunch.callback.AddListener ((eventData) => {
			ctPunch.PunchOn ();
		});

	}

}
