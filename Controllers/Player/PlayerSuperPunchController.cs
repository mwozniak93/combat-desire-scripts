﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

[RequireComponent (typeof(SuperPunch))]
public class PlayerSuperPunchController : MonoBehaviour
{

	// Use this for initialization
	[SerializeField]
	EventTrigger PunchBtn;
	SuperPunch ctSuperPunch;
	// Use this for initialization
	void Start ()
	{

		ctSuperPunch = GetComponent<SuperPunch> ();
		EventTrigger.Entry entrySuperPunchPrepere = new EventTrigger.Entry ();
		EventTrigger.Entry entrySuperPunch = new EventTrigger.Entry ();
		entrySuperPunchPrepere.eventID = EventTriggerType.PointerDown;
		entrySuperPunchPrepere.callback.AddListener ((eventData) => {
			ctSuperPunch.SpeedAttackOnDown ();
		});

		entrySuperPunch.eventID = EventTriggerType.PointerUp;
		entrySuperPunch.callback.AddListener ((eventData) => {
			ctSuperPunch.SpeedAttackOnUp ();
		});
	}
	

}
