﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using EZCameraShake;

//TODO EnemyAiSkillController =  rowniez ma merode chnakeskill, tyle że chodzi koutyna, ktora w zaleznosci od pwneych warunkow
// przestawia uzwany skilli nie dodaje metody na orzycisk tylko
//do delegeaty, ktora bedzie wykonywana co jakis czas
[RequireComponent (typeof(Skill))]
public class PlayerSkillController : MonoBehaviour
{

	//	public delegate void DUseSkill();
	//	DUseSkill UseSkill;
	[HideInInspector]
	public CharacterModel oPlayerModel;
	GameObject specialSkillButton;
	//	Transformation ctTransformController;
	Skill ctEnergyAttack;
	//	// Use this for initialization
	void Start ()
	{
		oPlayerModel = GetComponent<CharacterContainer> ().oCharacter;
		ctEnergyAttack = GetComponent<Skill> ();
	}

	public void ChangeSkill (SkillModel oSkill)
	{
		oPlayerModel.oCurrentSkill = oSkill;
		EventTrigger trigger = PlayerUI.instance.specialSkillBtn.GetComponent<EventTrigger> ();
		oPlayerModel.oCurrentSkill.eCooldownChange -= PlayerUI.instance.UpdateSkillCooldown;
		oPlayerModel.oCurrentSkill.eCooldownChange += PlayerUI.instance.UpdateSkillCooldown;
		trigger.triggers.Clear ();
		EventTrigger.Entry entry = new EventTrigger.Entry ();
		EventTrigger.Entry entry2 = new EventTrigger.Entry ();

		switch (oPlayerModel.oCurrentSkill.oSkillType) {
		case SkillType.Transformation:
			entry.eventID = EventTriggerType.PointerClick;
//			//Debug.Log("ctTransformController ; " +ctTransformController.name);
			entry.callback.AddListener ((eventData) => {
				ctEnergyAttack.UseTransformation ();
			});
			break;
		
		case SkillType.OneShot:
			entry.eventID = EventTriggerType.PointerClick;
			entry.callback.AddListener ((eventData) => {
				ctEnergyAttack.StartCoroutine (ctEnergyAttack.Shoot (oPlayerModel.oCurrentSkill.BulletDirections));
			});
//			entry.callback.AddListener( (eventData) => { ctEnergyAttack.StartCoroutine(ctEnergyAttack.Shoot(oPlayerModel.oCurrentSkill.BulletDirections))(); } );
			break;
		case SkillType.Aim:
			entry.eventID = EventTriggerType.PointerClick;
			entry.callback.AddListener ((eventData) => {
				ctEnergyAttack.StartCoroutine (ctEnergyAttack.ShootAIM (oPlayerModel.oCurrentSkill.BulletDirections));
			});
			//			entry.callback.AddListener( (eventData) => { ctEnergyAttack.StartCoroutine(ctEnergyAttack.Shoot(oPlayerModel.oCurrentSkill.BulletDirections))(); } );
			break;
		case SkillType.Wave:
//			//Debug.Log("Energy wave:" );
			entry2.eventID = EventTriggerType.PointerDown;
			entry2.callback.AddListener ((eventData) => {
				ctEnergyAttack.PrepereEnergyAttack ();
			});
			entry.eventID = EventTriggerType.PointerUp;
			entry.callback.AddListener ((eventData) => {
				ctEnergyAttack.StartCoroutine (ctEnergyAttack.ShootWave (oPlayerModel.oCurrentSkill.BulletDirections));
			});
			trigger.triggers.Add (entry2);
		//			entry.callback.AddListener( (eventData) => { ctEnergyAttack.StartCoroutine(ctEnergyAttack.Shoot(oPlayerModel.oCurrentSkill.BulletDirections))(); } );
			break;

		case SkillType.LongWave:
			//			//Debug.Log("Energy wave:" );
			entry2.eventID = EventTriggerType.PointerDown;
			entry2.callback.AddListener ((eventData) => {
				ctEnergyAttack.StartCoroutine (ctEnergyAttack.ShootLongWave (oPlayerModel.oCurrentSkill.BulletDirections));
			});
//			entry2.callback.AddListener((eventData) =>{ });
			entry.eventID = EventTriggerType.PointerUp;
			entry.callback.AddListener ((eventData) => {
				ctEnergyAttack.StopLongWave ();
			});
			trigger.triggers.Add (entry2);
			//			entry.callback.AddListener( (eventData) => { ctEnergyAttack.StartCoroutine(ctEnergyAttack.Shoot(oPlayerModel.oCurrentSkill.BulletDirections))(); } );
			break;

		}
		trigger.triggers.Add (entry);
	}

}
