﻿using UnityEngine;
using System.Collections;

public class PlayerTelekinessisController : MonoBehaviour
{
	GameObject TkItem;
	Telekinessis ctTelekinessis;
	CharacterModel oCharacter;

	void Start ()
	{
//		Box = transform.parent.gameObject;
		ctTelekinessis = GetComponentInParent<Telekinessis> ();
		oCharacter = GetComponentInParent<CharacterContainer> ().oCharacter;
	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		//		//Debug.Log("COLL : " + coll.gameObject.name);|| (coll.CompareTag("Bullet") && coll.name!="Hit")
		if (coll.CompareTag ("Enemy") && coll.name == "Box") {
			ctTelekinessis.TkTargetGO = coll.gameObject;
//			ctTelekinessis.TelekinesisOnDown();
			oCharacter.OnTkObjectMet (true);
		}
	}


	void OnTriggerExit2D (Collider2D coll)
	{
//		|| (coll.CompareTag("Bullet") && coll.name!="Hit")
		if (coll.CompareTag ("Enemy") && coll.name == "Box") {
//			Telekinessis ctTelekinessis = coll.GetComponent<Telekinessis>();
			ctTelekinessis.TkTargetGO = null;
//			CharacterModel oCharacter = coll.GetComponent<CharacterContainer>().oCharacter;
			oCharacter.OnTkObjectMet (false);
		}
	}
}
