﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Movement))]
public class AiMoveBackController : MonoBehaviour
{
	CharacterModel oEnemyModel;
	Movement ctMovement;
	[SerializeField]
	bool shouldRandomizeReverseTime;
	[SerializeField]
	Vector2 randomDirTime;
	[SerializeField]
	float reverseDirTime;

	float actualReverseTime;
	// Use this for initialization
	void Start ()
	{
		oEnemyModel = GetComponent<CharacterContainer> ().oCharacter;
		ctMovement = GetComponent<Movement> ();
	
		float random = Random.Range (-10f, 10);
		actualReverseTime += random / 100;
		oEnemyModel.Speed *= -1f;
	}

	void  OnEnable ()
	{
//		StartCoroutine(ReverseDirection());
	
	}

	IEnumerator ReverseDirection ()
	{
		yield return new WaitForSeconds (0.5f);
		if (shouldRandomizeReverseTime) {
			actualReverseTime = Random.Range (randomDirTime.x, randomDirTime.y);
		} else {
			actualReverseTime = reverseDirTime;
		}
		oEnemyModel.Speed *= -1;
		yield return new WaitForSeconds (actualReverseTime);
		StartCoroutine (ReverseDirection ());
	}

}
