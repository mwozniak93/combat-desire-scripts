﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

//TODO EnemyAiSkillController =  rowniez ma merode chnakeskill, tyle że chodzi koutyna, ktora w zaleznosci od pwneych warunkow
// przestawia uzwany skilli nie dodaje metody na orzycisk tylko
//do delegeaty, ktora bedzie wykonywana co jakis czas, a wykonywanie tej delegaty kiedy i jak beize zdefiniowane w kazdym z controleru konkretnego skilla.
// np. transformacja ma sie odbyc tylko jezeli ma hp<10  i to w konreolerze EnemyTransformation (zawsze trzeba sprawdzic czy oCurrentSkill.type == np. Transformation), tak samo w
// energySkill strzelanie np co iles sekund z ucyiem tej Jedenj dostepmnej w EnemySkillController delegaty ze zmienna funkcja przypisaną do niej.
public class AiSkillController : MonoBehaviour
{

	public delegate void DUseSkill ();

	DUseSkill UseSkill;
	CharacterModel oEnemyModel;
	GameObject specialSkillButton;
	//	Transformation ctTransformController;
	Skill ctEnergyAttack;
	//	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (ChangeSkillRandomly ());
	}

	void Start ()
	{
		oEnemyModel = GetComponent<CharacterContainer> ().oCharacter;
	}

	public void ChangeSkill ()
	{
		switch (oEnemyModel.oCurrentSkill.oSkillType) {
		case SkillType.Transformation:
			break;
		
		case SkillType.OneShot:
			break;
		}
	}

	IEnumerator ChangeSkillRandomly ()
	{
		yield return new WaitForSeconds (3f);
		if (oEnemyModel.States != States.Attacking) {
			int ranodmVal = Random.Range (0, oEnemyModel.PlayerSkillsList.Count);
//			//Debug.Log("RandomVal : " + ranodmVal);
			oEnemyModel.oCurrentSkill = oEnemyModel.PlayerSkillsList [ranodmVal];
			oEnemyModel.oCurrentSkill.LastCooldownVal = oEnemyModel.oCurrentSkill.Cooldown;
//		ChangeSkill();
		}
		StartCoroutine (ChangeSkillRandomly ());
	}

}
