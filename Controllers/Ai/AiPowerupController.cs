﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Powerup))]
public class AiPowerupController : MonoBehaviour
{
	Powerup ctPowerupController;
	CharacterModel oEnemy;
	// Use this for initialization
	void Start ()
	{
		oEnemy = GetComponent<CharacterContainer> ().oCharacter;
		ctPowerupController = GetComponent<Powerup> ();

	}

	void OnEnable ()
	{
		StartCoroutine (UsePowerup ());
	}

	IEnumerator UsePowerup ()
	{
		yield return new WaitForSeconds (3f);
		if (oEnemy.KiLvlPercentage < 2f) {
			ctPowerupController.PowerupOn ();
			yield return new WaitForSeconds (2f);
			ctPowerupController.PowerUpOff ();
		}

		StartCoroutine (UsePowerup ());
	}
}
