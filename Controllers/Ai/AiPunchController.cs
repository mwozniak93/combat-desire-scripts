﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Punch))]
public class AiPunchController : MonoBehaviour
{
	Punch ctBasicAttack;
	CharacterModel oEnemyModel;
	[SerializeField]
	float attackRate = 0.5f;
	bool isHitting;
	// Use this for initialization
	void Start ()
	{
		ctBasicAttack = GetComponent<Punch> ();
		oEnemyModel = GetComponent<CharacterContainer> ().oCharacter;

		float random = Random.Range (-10f, 10);
		attackRate += random / 100;
	}

	void OnEnable ()
	{
		StartCoroutine (startPunch ());
	}

	IEnumerator startPunch ()
	{
		yield return new WaitForSeconds (0.5f);
		//		//Debug.Log("oCharacterModel.oState  : " +oCharacterModel.oState );
		if (oEnemyModel.States == States.StandingNear) {
			ctBasicAttack.PunchOn ();
		}
		yield return new WaitForSeconds (attackRate);
		StartCoroutine (startPunch ());
				
	}
}
