﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Skill))]
public class AiShootAIMController : MonoBehaviour
{
	Skill ctEnergyAttack;
	[SerializeField]
	float shootRate = 2;
	CharacterModel oEnemyModel;
	[SerializeField]
	float distanceToPlayer = 3.5f;
	// Use this for initialization
	void Start ()
	{
		ctEnergyAttack = GetComponent<Skill> ();
		oEnemyModel = GetComponent<CharacterContainer> ().oCharacter;

		float random = Random.Range (-10f, 10);
		shootRate += random / 100;
	}

	void OnEnable ()
	{
		StartCoroutine (UseSkill ());

	}

	IEnumerator UseSkill ()
	{
		yield return new WaitForSeconds (shootRate);
//		//Debug.Log("oEnemyModel.DistanceToPlayer : " +oEnemyModel.DistanceToPlayer);
		if ((oEnemyModel.DistanceToPlayer < distanceToPlayer && oEnemyModel.DistanceToPlayer > 0.5f) && oEnemyModel.oCurrentSkill.oSkillType == SkillType.Aim) {
			ctEnergyAttack.StartCoroutine (ctEnergyAttack.ShootAIM (oEnemyModel.oCurrentSkill.BulletDirections));
		}
		StartCoroutine (UseSkill ());
	}
}
