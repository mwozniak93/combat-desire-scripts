﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Skill))]
public class AiOneShootController : MonoBehaviour
{
	Skill ctEnergyAttack;
	[SerializeField]
	float shootRate = 2;
	CharacterModel oEnemyModel;
	[SerializeField]
	float distanceToPlayer = 0.5f;
	// Use this for initialization
	void Start ()
	{
		ctEnergyAttack = GetComponent<Skill> ();
		oEnemyModel = GetComponent<CharacterContainer> ().oCharacter;

		float random = Random.Range (-10f, 10);
		shootRate += random / 100;
	}

	void OnEnable ()
	{
		StartCoroutine (UseSkill ());
	}

	IEnumerator UseSkill ()
	{
		yield return new WaitForSeconds (shootRate);
		if (oEnemyModel.DistanceToPlayer < distanceToPlayer && oEnemyModel.oCurrentSkill.oSkillType == SkillType.OneShot) {
			ctEnergyAttack.StartCoroutine (ctEnergyAttack.Shoot (oEnemyModel.oCurrentSkill.BulletDirections));
		}
		StartCoroutine (UseSkill ());
	}
}
