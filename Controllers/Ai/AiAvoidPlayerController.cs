﻿using UnityEngine;
using System.Collections;

public class AiAvoidPlayerController : MonoBehaviour
{
	CharacterModel oEnemyModel;
	[SerializeField]
	float distanceToAvoidPlayer, timeAfterAvoid, timeAfterStopAvoid;


	// Use this for initialization
	void Start ()
	{
		oEnemyModel = GetComponent<CharacterContainer> ().oCharacter;
		float random = Random.Range (-17f, 17);
		timeAfterAvoid = timeAfterAvoid + random / 100;
		//timeAfterStopAvoid=timeAfterStopAvoid+random/100;
	}

	void OnEnable ()
	{
		StartCoroutine (ChangeSpeedDir ());
	}

	IEnumerator ChangeSpeedDir ()
	{
		
		yield return new WaitForSeconds (timeAfterAvoid);
		if (oEnemyModel.DistanceToPlayer < distanceToAvoidPlayer) {
			oEnemyModel.Speed *= -1;
			yield return new WaitForSeconds (timeAfterStopAvoid);
			oEnemyModel.Speed *= -1;
		}
		StartCoroutine (ChangeSpeedDir ());
	}

}
