﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(Guard))]
public class AiGuardController : MonoBehaviour
{
	Guard ctGuard;
	CharacterModel oCharacterModel;
	CharacterModel oPlayer;
	// Use this for initialization
	void Start ()
	{
		ctGuard = GetComponent<Guard> ();
		oCharacterModel = GetComponent<CharacterContainer> ().oCharacter;
		oPlayer = PlayerSingleton.instance.GetComponent<CharacterContainer> ().oCharacter;
		StartCoroutine (Defend ());
	}

	void Update ()
	{
//		//Debug.Log("oCharacterModel ; " +oCharacterModel.States + "PLQAYER STATE ; " + oPlayer.States);
	}

	IEnumerator Defend ()
	{

		if (oPlayer.States == States.Attacking) {
			ctGuard.GuardOn ();
		} else if (oCharacterModel.States == States.Idle) {
			ctGuard.GuardOff ();
		}
		yield return new WaitForSeconds (1f);
		StartCoroutine (Defend ());
	}
}
