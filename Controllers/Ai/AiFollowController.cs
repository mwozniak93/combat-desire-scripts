﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//public enum States{Attacking,Walking, Idle, Following, StandingNear, Hurting}

[RequireComponent (typeof(Follow))]
public class AiFollowController : MonoBehaviour
{

	//	[HideInInspector]
	//	public float speedX, speedY, lastSpeedX,lastSpeedY;
	CharacterModel oEnemyModel;
	CharacterContainer ctEnemyContainer;
	Animator ctAnimator;
	//	EnemyBasicAttack ctEnemyBasicAttack;
	//	Transformation ctTransformController;
	public delegate void Move (float val);

	public event Move eMove;

	Follow ctFollow;
	//	GameManager oGameManager;
	//	MovementController ctMovementController;
	public Vector2 Direction;
	Rigidbody2D ctRigdbody2D;
	float xYDifference;
	Vector2 dir;
	float DistanceToPlayer;
	List<GameObject> aWayPoints;
	int wayPointCounter = 0;
	Transform player;
	[SerializeField]
	float distanceToDetectPlayer = 1;

	void Start ()
	{
		ctEnemyContainer = GetComponent<CharacterContainer> ();
		oEnemyModel = ctEnemyContainer.oCharacter;
		ctAnimator = GetComponent<Animator> ();
		ctRigdbody2D = GetComponent<Rigidbody2D> ();
		if (PlayerSingleton.instance != null) {
			player = PlayerSingleton.instance.transform;
		}
		ctFollow = GetComponent<Follow> ();
		if (player != null) {
			
		
			ctFollow.target = player.transform;
		}
	}



	void ChasePlayerIfClose ()
	{

		if ((oEnemyModel.DistanceToPlayer < distanceToDetectPlayer && oEnemyModel.DistanceToPlayer >= 0.13f) && (oEnemyModel.States == States.Idle || oEnemyModel.States == States.StandingSteel)) {
			oEnemyModel.States = States.Following;
		}

		if (oEnemyModel.DistanceToPlayer < 0.13f && (oEnemyModel.States == States.Idle || oEnemyModel.States == States.Following)) {

			oEnemyModel.States = States.StandingNear;

		}
		if ((oEnemyModel.DistanceToPlayer < distanceToDetectPlayer && oEnemyModel.DistanceToPlayer >= 0.3f) && (oEnemyModel.States == States.StandingNear)) {

			oEnemyModel.States = States.Following;
		}
		if (oEnemyModel.DistanceToPlayer >= distanceToDetectPlayer && oEnemyModel.States != States.Attacking && oEnemyModel.States != States.Charging && oEnemyModel.States != States.Hurting) {
			
			oEnemyModel.States = States.StandingSteel;
//			//Debug.Log("3");
		}

//		}
	}

	void FixedUpdate ()
	{
//		//Debug.Log("enemy state :" + oEnemyModel.States);
		ChasePlayerIfClose ();
		ctFollow.FollowTarget ();

		if (oEnemyModel.States == States.Idle) {

		}

	}
	


}
