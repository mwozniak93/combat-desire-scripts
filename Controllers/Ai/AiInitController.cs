﻿using UnityEngine;
using System.Collections;

public class AiInitController : MonoBehaviour
{
	CharacterModel oCharacter;
	//	[SerializeField]
	//	Sprite spriteMarker;
	// Use this for initialization

	MapMarker ctMapMarker;

	void Awake ()
	{
		ctMapMarker = transform.parent.GetComponentInChildren<MapMarker> ();
	}

	void DeactivateMarker ()
	{
		ctMapMarker.isActive = false;
	}

	void OnEnable ()
	{
		ctMapMarker.isActive = true;

	}

	void Start ()
	{
		oCharacter = GetComponent<CharacterContainer> ().oCharacter;
		oCharacter.eDie += DeactivateMarker;
		float random = Random.Range (-35f, 35);
		oCharacter.Speed = oCharacter.Speed + (oCharacter.Speed * random / 100);
//		//Debug.Log("Speed AFTER ranomdizing : "  + oCharacter.Speed);
	}

	void OnDestroy ()
	{
		oCharacter.eDie -= DeactivateMarker;
	}
}
